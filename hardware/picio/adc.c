// adc.c

/* David Cote <davidmcote@gmail.com>
 * Fall 2011
 */

#include "adc.h"

u16 adc_read(void){
    u16 result;
    ADCON0bits.GO=1;
    while(ADCON0bits.GO);
    result=ADRESH;
    result<<=8;
    result+=ADRESL;
    return result;
}

void adc_init(void){
    TRISAbits.RA0=1; // High impedance

    ADCON0bits.CHS=0; // Channel 0
    ADCON0bits.GO=0; // ADC Idle
    ADCON0bits.ADON=1; // ADC Enabled

    ADCON1bits.VCFG0=0; // Vdd Reference
    ADCON1bits.VCFG1=0; // Vdd Reference
    ADCON1bits.PCFG=0b1010; // AN4:0 //0b1110; // ANSEL

    ADCON2bits.ADFM=1; // Right justified
    ADCON2bits.ACQT=1; // Acquisition time=2 Tad
    ADCON2bits.ADCS=0; // Clock=Fosc/2
}

u16 adc_read_ch(u8 ch){
    u16 result;
	ADCON0bits.CHS=ch;
    ADCON0bits.GO=1;
    while(ADCON0bits.GO);
    result=ADRESH;
    result<<=8;
    result+=ADRESL;
    return result;
}
