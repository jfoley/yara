
/* David Cote <davidmcote@gmail.com>
 * Fall 2011
 */

/*
A[0] = Gas
A[1] = Break
A[2] = Steering

I[0] = Radio0
I[1] = Radio1
I[2] = Radio2
I[3] = Radio3
I[4] = Lights
I[5] = Radio

I[RC4] = Horn

O[0] = Fan & 0b01
O[1] = Fan & 0b10
O[2] = Motors
 */

#define TICTAC_PROBE     '?'
#define TICTAC_PROBE_ACK '!'

#define TICTAC_DUMP      'X'
#define TICTAC_DUMP_NORESET 'Y'
#define TICTAC_BASE      0x30 // '0'
#define TICTAC_FAN0      TICTAC_BASE + 0b000
#define TICTAC_FAN1      TICTAC_BASE + 0b001
#define TICTAC_FAN2      TICTAC_BASE + 0b010
#define TICTAC_FAN3      TICTAC_BASE + 0b011
#define TICTAC_MOTOR0    TICTAC_BASE + 0b000
#define TICTAC_MOTOR1    TICTAC_BASE + 0b100

/*
     Motor   Fan
'0'    0      0
'1'    0      1
'2'    0      2
'3'    0      3
'4'    1      0
'5'    1      1
'6'    1      2
'7'    1      3
 */





#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "global.h"
#include "uart.h"
#include "adc.h"

#pragma config WDT   = OFF
#pragma config WDTPS = 2048 // ~8 seconds
#pragma config MCLRE = ON
#pragma config FOSC  = INTOSCIO_EC
#pragma config LVP   = OFF

#define BAUD 115200

void btn_isr();


#pragma interrupt hpHandler

void hpHandler(void) {
    // UART interrupt handler
    uart_isr();
    // INT0 RB0 handler
    btn_isr();
}

#pragma code high_priority_interrupt_vector=0x08

void high_priority_interrupt_vector(void) {
    _asm goto hpHandler _endasm;
}

#define NUM_ADC 6
#define NUM_BTN 7

struct {
    i16 adcValues[NUM_ADC];
    u8 btnValues[NUM_BTN];
} hid_info;

void btn_isr() {
    if (INTCONbits.INT0IF)
        hid_info.adcValues[NUM_ADC - 1]++;

    INTCONbits.INT0IF = 0;
}

u8 btn_read(u8 ch) {
    if (ch < (NUM_BTN - 1))
        return (PORTB >> ch) & 1;
    return PORTEbits.RE0;
}

void btn_init(void) {
    TRISB |= 0b111111;
    TRISC &= ~0b111;
    PORTC |= 0b11;
    PORTC &= ~0b100;
    TRISEbits.RE0 = 1;

    hid_info.adcValues[NUM_ADC - 1] = 0;
    INTCONbits.INT0IE = 1;
}

void dump_hid_info(void) {
    int i;
    printf("%d %d ", NUM_ADC, NUM_BTN);
    for (i = 0; i < NUM_ADC; i++)
        printf("%d ", hid_info.adcValues[i]);
    for (i = 0; i < NUM_BTN; i++) {
        printf("%d", hid_info.btnValues[i]);
        hid_info.btnValues[i] = 0;
        if (i != NUM_BTN - 1)
            printf(" ");
    }
    printf("\r\n");
}

void update_hid_info(void) {
    int i;
    for (i = 0; i < NUM_ADC - 1; i++)
        hid_info.adcValues[i] = adc_read_ch(i);
    for (i = 0; i < NUM_BTN; i++) {
        hid_info.btnValues[i] &= 0b01;
        hid_info.btnValues[i] |= btn_read(i) ? 3 : 0;
    }
}

void wdt_reset(void){
    _asm clrwdt _endasm;
}

void wdt_enable(void){
    WDTCONbits.SWDTEN=1;
    wdt_reset();
}


void main(void) {
    // Initialize PIC
    ADCON1bits.PCFG = 0b1110; // A/D ports
    OSCCONbits.IRCF = 0b111; // Internal RC Freq=8 MHz
    CMCON = 7; // Turn off comparitors
    // Initialize Drivers
    uart_init(BAUD, CLOCK_FREQ);
    adc_init();
    btn_init();

    wdt_enable();
   
    while (1) {
        u8 inst;
        do{
            update_hid_info();
        } while (!uart_hasc());

        wdt_reset();

        inst = uart_getc();
        uart_purge();

        if (('0' <= inst) && (inst <= '7'))
            PORTC = inst & 0b111;

        if (inst == TICTAC_PROBE) {
            printf("%c\r\n", TICTAC_PROBE_ACK);
            continue;
        }


        dump_hid_info();

        if (inst == TICTAC_DUMP)
            hid_info.adcValues[NUM_ADC - 1] = 0;

    }

}

