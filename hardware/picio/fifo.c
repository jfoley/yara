// fifo.c

#include "fifo.h"

/* David Cote <davidmcote@gmail.com>
 * 8/12/2011
 *
 * C methods for FIFO queues
 */

/////////////////////////////////////////////////////////////
// Function Declarations
fifo_offset_t fifo_index    (const fifo *f, fifo_offset_t index);

/////////////////////////////////////////////////////////////
// Accessors

// Returns true if FIFO is empty
bool          fifo_empty    (const fifo *f){ return !f->index; }

// Returns true if FIFO is full
bool          fifo_full     (const fifo *f){ return f->index==f->size; }

// Returns the capacity of the FIFO
fifo_offset_t fifo_capacity (const fifo *f){ return f->size; }

// Returns the number of elements in the FIFO
fifo_offset_t fifo_used     (const fifo *f){ return f->index; }

// Returns the number of available slots in the FIFO
fifo_offset_t fifo_available(const fifo *f){ return f->size-f->index; }

// Returns the buffer associated with the FIFO
fifo_data_t  *fifo_buffer   (const fifo *f){ return f->buffer; }

// Returns the next element to be dequeued from a FIFO
//  without actually dequeuing it
// Precondition: Buffer must not be empty
fifo_data_t   fifo_peek     (const fifo *f){ return f->buffer[f->start_index]; }

// Returns the i'th element to be dequeued from a FIFO
//  without actually dequeuing it
// Precondition: Buffer must contain "i" elements
//  able to be dequeued
fifo_data_t   fifo_peekn    (const fifo *f, fifo_offset_t i){ return f->buffer[fifo_index(f, f->start_index+i)]; }

/////////////////////////////////////////////////////////////
// Mutators

// Initializes the members of a FIFO struct
void          fifo_init     (fifo *f, char *buf, fifo_offset_t len){
	f->buffer=buf;
	f->size=len;
	f->start_index=0;
	f->index=0;
}

// Empties a FIFO
void          fifo_purge    (fifo *f)      { f->index=0; }

// Queues data in the FIFO
// Precondition: Buffer must not be empty
void          fifo_put      (fifo *f, fifo_data_t data){
	f->buffer[fifo_index(f, f->start_index+f->index)]=data;
	f->index++;
}

// Returns an element from the FIFO and dequeues it
// Precondition: Buffer must not be empty
fifo_data_t    fifo_get     (fifo *f){
	fifo_data_t tmp=fifo_peek(f);
	f->start_index=fifo_index(f, f->start_index+1);
	f->index--;
	return tmp;
}

/////////////////////////////////////////////////////////////
// Misc

// Shifts a given index up or down until it lies within
//  a FIFO's buffer size
fifo_offset_t  fifo_index   (const fifo *f, fifo_offset_t index){
	while(index>=f->size)
		index-=f->size;
	while(index<0)
		index+=f->size;
	return index;
}


