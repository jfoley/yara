// uart.h
/* David Cote <davidmcote@gmail.com>
 * 8/15/2011
 *
 * Implements buffered, interrupt-driven UART IO on PIC18 microcontrollers
 */

#ifndef __UART_H
#define __UART_H

#include "global.h"

#ifndef UART_USER_FUNCTIONS
#define UART_USER_FUNCTIONS 0
#endif

#define UART_BUFFER_SIZE_IN  0x20
#define UART_BUFFER_SIZE_OUT 0x60

unsigned char uart_hasc(void);
void uart_purge(void);

void uart_init(unsigned long baud_rate, unsigned long clock_freq);
void uart_isr(void);
int  uart_putc(char c);
int  uart_getc(void);
int  uart_getc_echo(void);
unsigned int uart_gets(char *str, unsigned int maxlen);

#if UART_USER_FUNCTIONS
#include <stdio.h>
int _user_putc(char c);
#endif

#endif
