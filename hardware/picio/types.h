// types.h

#ifndef __TYPES_H
#define __TYPES_H

typedef char          i8;
typedef int           i16;
typedef long          i32;
typedef unsigned char u8;
typedef unsigned int  u16;
typedef unsigned long u32;

typedef enum {false, true} boolean;
//typedef const rom far char constchar;

#endif
