// adc.h

/* David Cote <davidmcote@gmail.com>
 * Fall 2011
 */

#ifndef __ADC_H
#define __ADC_H

#include "global.h"

void adc_init(void);
u16 adc_read(void);
u16 adc_read_ch(u8 ch);

#endif
