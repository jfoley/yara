// fifo.h

/* David Cote <davidmcote@gmail.com>
 * 8/12/2011
 *
 * C support for FIFO queues
 */

/* Example usage:
 *
 * // Allocate Memory
 * char *buffer=(char *)malloc(BUFFER_SIZE);
 *
 * // Declare and initialize a FIFO
 * fifo q;
 * fifo_init(&q, buffer, BUFFER_SIZE);
 *
 * // Store data in FIFO
 * while(!fifo_full(&q))
 * 	fifo_put(&q, 'a');
 * 
 * // Retrieve data from FIFO
 * while(!fifo_empty(&q))
 * 	printf("%c", fifo_get(&q));
 * 
 * // Free memory
 * free(buffer);
 */

#ifndef __FIFO_H
#define __FIFO_H

//#include <stdbool.h>
typedef enum { false, true } bool;

// Signed integer type large-enough to hold size of buffers
typedef signed char fifo_offset_t;

// FIFO element type
typedef char fifo_data_t;

/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////

// FIFO struct
typedef struct {
	fifo_data_t	 *buffer;
	fifo_offset_t size;
	fifo_offset_t start_index;
	fifo_offset_t index;
} fifo;

// Declares an initialized FIFO named "name" of size "size"
#define DECLARE_FIFO(name, size) \
	char __##name##_buffer[size]; \
	fifo name={__##name##_buffer, size, 0, 0}

// FIFO Mutators
void          fifo_init     (fifo *f, char *buf, fifo_offset_t len);
void          fifo_put      (fifo *f, char data);
fifo_data_t   fifo_get      (fifo *f);
void          fifo_purge    (fifo *f);

// FIFO Accessors
fifo_data_t   fifo_peek     (const fifo *f);
fifo_data_t   fifo_peekn    (const fifo *f, fifo_offset_t i);
bool          fifo_empty    (const fifo *f);
bool          fifo_full     (const fifo *f);
fifo_offset_t fifo_capacity (const fifo *f);
fifo_offset_t fifo_used     (const fifo *f);
fifo_offset_t fifo_available(const fifo *f);
fifo_data_t  *fifo_buffer   (const fifo *f);

#endif
