// uart.c

/* David Cote <davidmcote@gmail.com>
 * 8/15/2011
 *
 * Implements buffered, interrupt-driven UART IO on PIC18 microcontrollers
 */
#include "uart.h"
#include "fifo.h"

// FIFO queues to buffer UART data
DECLARE_FIFO(uart_out, UART_BUFFER_SIZE_OUT);
DECLARE_FIFO(uart_in,  UART_BUFFER_SIZE_IN);

unsigned char uart_hasc(void){
	return !fifo_empty(&uart_in);
}

void uart_purge(void){
	fifo_purge(&uart_in);
}

/* uart_init
 * Configures the EUSART as a UART.
 * Sets Baud-rate and serial 8N1.
 * Assumes "uart_isr" will be called during high priority interrupts.
 * Enables necessary interrupts.
 */
void uart_init(unsigned long baud_rate, unsigned long clock_freq){
    unsigned long baud_gen=clock_freq/(4*(baud_rate+1));

    // Disable digital output on TX/RX pins
    TRISCbits.RC7 =1;
    TRISCbits.RC6 =1;

    // 8 bit TX/RX
    TXSTAbits.TX9=0;
    RCSTAbits.RX9=0;

    // Configure timing
    TXSTAbits.SYNC=0;    // Asynchronous Mode
    BAUDCONbits.BRG16=1; // 16-bit Baud select
    TXSTAbits.BRGH=1;    // High Speed
    SPBRGH= baud_gen>>8;
    SPBRG = baud_gen;

    // Enable individual TX/RX lines
    RCSTAbits.CREN=1;    // Continuous recieve enable
    TXSTAbits.TXEN=1;    // Transmit enable

    // Set up and enable interrupts
    PIR1bits.TXIF=1; // TXREG is empty
    PIR1bits.RCIF=0; // RCREG is empty
    IPR1bits.RCIP=1; // Recieve interrupt is high priority
    IPR1bits.TXIP=1; // Transmit interrupt is high priority
    PIE1bits.RCIE=1; // Enable recieve interrupt
    PIE1bits.TXIE=0; // Disable transmit interrupt
    INTCONbits.PEIE=1;  // Enable periferal interrupts
    RCONbits.IPEN=1;    // Enable priority interrupts
    INTCONbits.GIEH=1;  // Enable all unmasked interrupts

    // Enable serial port
    RCSTAbits.SPEN=1;

#if UART_USER_FUNCTIONS
    // Set output streams
    stdout=stderr=_H_USER;
#endif
}

/* uart_read
 * Reads a character from the uart_in fifo and returns it.
 * If the uart_in fifo is empty, then this method blocks until it can
 *  get a character from the fifo.
 */
int uart_getc(){
    char c;
    while(fifo_empty(&uart_in)); // Block if buffer is empty
    PIE1bits.RCIE=0; // Disable interrupts
    c=fifo_get(&uart_in);
    PIE1bits.RCIE=1; // Re-enable interrupts
    return c;
}

/* uart_write
 * Stores a character into the uart_out fifo to be transmitted
 * If the fifo is full, this method blocks until the character can be placed
 */
int uart_putc(char c){
    while(fifo_full(&uart_out)) // Block and try to free up the buffer
        PIE1bits.TXIE=1;
    PIE1bits.TXIE=0; // Disable interrupts
    fifo_put(&uart_out, c);
    PIE1bits.TXIE=1; // Re-enable interrupts
	return c;
}

/* uart_isr
 * Called upon every (high-priority) interrupt.
 * RX interrupt condition: Read the byte in RCREG and try to store it in uart_in
 * TX interrupt condition: Grab a byte from the buffer and store it in TXREG
 *   or disable TX interrupt if the buffer is empty
 */
void uart_isr(){
	if(RCSTAbits.OERR){
		RCSTAbits.CREN=0;
		RCSTAbits.CREN=1;
	}
	if(PIE1bits.RCIE && PIR1bits.RCIF)
        if(!fifo_full(&uart_in))
            fifo_put(&uart_in, RCREG);
    if(PIE1bits.TXIE && PIR1bits.TXIF)
        if(!fifo_empty(&uart_out) || (PIE1bits.TXIE=0)) // Yes, "="; not "==".
            TXREG=fifo_get(&uart_out);
}


#if UART_USER_FUNCTIONS
int _user_putc(char c){ return uart_putc(c); }
#endif

int uart_getc_echo(void){
	return uart_putc(uart_getc());
}

unsigned char whitespace(char c){
    static const char whitespace_chars[]={' ', '\r', '\n', '\t', 0};
    unsigned int i=0;
    while(1){
        if(whitespace_chars[i]==c)
            return 1;
        if(whitespace_chars[i]==0)
            return 0;
        i++;
    }
}

unsigned int uart_gets(char *buf, unsigned int maxlen){
    unsigned int i=0;

    // Remove leading whitespace
    while(whitespace(buf[i]=uart_getc_echo()));

    while(1){
        // If we encounter a whitespace character, end the string
        if(whitespace(buf[i]))
            break;

        // If we exceed the buffer size, end the string
        if(++i>=maxlen-1)
            break;

        // Otherwise, read the next character
        buf[i]=uart_getc_echo();//_user_getc();
    }

    // Make the string null-terminated
    buf[i]=0;

    // Return the length of the string (without the null-terminator)
    return i;
}

