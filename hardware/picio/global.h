
/* David Cote <davidmcote@gmail.com>
 * Fall 2011
 */

#ifndef __CADDO_H
#define __CADDO_H

#include <p18f4550.h>
#include "types.h"
#define CLOCK_FREQ 8000000

#define UART_USER_FUNCTIONS 1

#if ALL_USER_FUNCTIONS
	int _user_putc(unsigned char c);
#endif


#endif
