
#ifdef SDCC
	#include <pic16f684.h>
	unsigned int __at 0x2007 CONFIG =
		_WDT_OFF & _PWRTE_ON & _MCLRE_OFF & _CP_OFF & _CPD_OFF & _BOD_OFF & _IESO_OFF & _FCMEN_OFF & _INTOSCIO;
#else
	#error Need Config for other compilers
#endif

#define SWITCH RA0
#define LED    RA1

#define INPUT 1
#define OUTPUT 0

void delay(int amt) {
	while(amt--)
		;
}

void main(void)
{
	// Disable comparators & ADCs
	CMCON0=7; ANSEL=0;

	TRISA0 = INPUT; // input for SWITCH
	TRISA1 = OUTPUT; // output for LED

	LED = 0; // turn off LED to start

	while(1)
	{
		delay(9000);
		
		if(SWITCH) {
			delay(2000);
			if(SWITCH) {
				LED = !LED;
			}
		}
	}
}

