import os
from SCons.Script import *
from path_config import *

def DefineTarget(env, target_name, target_to_run, args = ''):
	if onLinux():
		run_env = Environment(ENV=os.environ)
	else:
		run_env = env
	alias = run_env.Alias(target_name, [target_to_run], target_to_run[0].path + ' ' + args)
	run_env.AlwaysBuild(alias) # always build is a misnomer; means always consider out of date


