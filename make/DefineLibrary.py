import os
from SCons.Script import *

def DefineLibrary(env, lib, src):
	env.Library(lib, src)
	env.Prepend(
		LIBPATH=[Dir('.').abspath],
		LIBS=[lib]
	)

