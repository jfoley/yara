import os.path
import platform
from SCons.Script import *

def onWindows():
	return sys.platform.startswith('win')
def onLinux():
	return sys.platform.startswith('linux')

def getMachineType():
	if onWindows():
		if 'PROGRAMFILES(X86)' in os.environ:
			return 64
		else:
			return 32
	else:
		(bits, execType) = platform.architecture()
		if bits == '64bit':
			return 64
		else:
			return 32
		