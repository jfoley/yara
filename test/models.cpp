#include "Test.h"
#include "backend.h"
#include "Assert.h"
#include <assimp.hpp>
#include <aiScene.h>
#include <aiPostProcess.h>

bool LoadTextures(const aiScene *scene) {
	show(scene->HasAnimations());
	show(scene->HasTextures());
	show(scene->HasCameras());
	show(scene->HasLights());
	show(scene->HasMaterials());
	show(scene->HasMeshes());

	loop(i, scene->mNumMaterials) {
		aiMaterial *mat = scene->mMaterials[i];
		aiString path;
		
		int texIndex = 0;
		aiReturn hasTexture = AI_SUCCESS;
		while(hasTexture == AI_SUCCESS) {
			hasTexture = mat->GetTexture(aiTextureType_DIFFUSE, texIndex, &path);
			show(path.data);
			texIndex++;
		}
	}

	loop(i, scene->mNumTextures) {
		aiTexture* texture = scene->mTextures[i];
		show(texture->achFormatHint);
		show(texture->mWidth);
		show(texture->mHeight);

		u32 size = 0; 
		if(texture->mHeight && texture->mWidth) {
			size = texture->mWidth * texture->mHeight;
		} else {
			size = texture->mWidth + texture->mHeight;
		}

		SDL_RWops *memfile = SDL_RWFromConstMem(texture->pcData, size);
		Assert(memfile);
		SDL_Surface *surface = IMG_LoadTyped_RW(memfile, false, texture->achFormatHint);
		Assert(surface);
		show(surface->w);
		show(surface->h);

		SDL_FreeSurface(surface);
		SDL_FreeRW(memfile);
	}

	loop(i, scene->mNumMeshes) {
		aiMesh *mesh = scene->mMeshes[i];
		show(mesh->HasBones());
		show(mesh->HasFaces());
		show(mesh->HasNormals());
		show(mesh->HasPositions());
	}

	return true;
}


TEST(assimp) {
	SDL_Init(SDL_INIT_EVERYTHING);
	IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF);

	Assimp::Importer importer;

	const aiScene *scene = importer.ReadFile("res/models/xd_tux_0.blend", 
			aiProcess_Triangulate |
			aiProcess_JoinIdenticalVertices |
			aiProcess_RemoveRedundantMaterials |
			aiProcess_SortByPType
			);

	CHECK(scene);
	LoadTextures(scene);

}

