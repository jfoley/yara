#ifndef __TEST_H
#define __TEST_H

#include "macros.h"

struct SubTest {
	/// For each condition tested of a Test, one of these is created
	SubTest(bool r, const char* t, const char* f, int l)
		: result(r), text(t), file(f), line(l) { }
	//SubTest(const SubTest &rhs)
		//: result(rhs.result), text(rhs.text), file(rhs.file), line(rhs.line) { }

	bool result;
	const char* text;
	const char* file;
	int line;
};

/// All tests are subclasses of this
class Test {
	public:
		Test() { }
		virtual ~Test() {}
		virtual void Process() = 0;
		virtual const char* Name() const = 0;
		int Count() const { return subtests.size(); }
		const SubTest& Get(int i) const { return subtests[i]; }
	protected:
		void Register(SubTest s) {
			subtests.push_back(s);
		}
		std::vector<SubTest> subtests;
};

/// hide the ugly global away
namespace TestSystem {
	void Add(Test* test);
};

#define TEST(name) \
	class T##name : public Test { \
		public: \
			T##name() { TestSystem::Add(this); } \
			virtual void Process(); \
			virtual const char* Name() const { return #name; } \
	} T##name##Obj; \
	void T##name::Process() 


#define CHECK(condition) \
	Register(SubTest(condition, #condition, __FILE__, __LINE__))

#define EXPECT_ASSERT(expr) CHECK_ASSERT(expr, true)

#define CHECK_ASSERT(expr, expected) \
	do { \
		bool x = false; \
		try { expr;	} catch (Assertion a) { x = true; } \
		Register(SubTest(x == expected, (expected ? #expr " asserts." : #expr " does assert."), __FILE__, __LINE__)); \
	} while (0)

// Define all tests in cpp files and link them within the SConscript

#endif

