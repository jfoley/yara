#include "Test.h"
#include "math/geom.h"
#include <cstdio>

TEST(sum_of_diff) {
	v2i a(1,2);
	v3i b(1,1,1);

	v3i r = v3i(a)+b;
	CHECK(r[0] == 2);
	CHECK(r[1] == 3);
	CHECK(r[2] == 1);
}

TEST(segment_intersect) {
	CHECK(SegmentsIntersect( v2r(0,0), v2r(1,1), v2r(1,0), v2r(0,1) ));
	CHECK(!SegmentsIntersect( v2r(0,0), v2r(1,1), v2r(1,0), v2r(2,1) ));
}

TEST(triangle_contains) {
	v2r a(0,0);
	v2r b(3,0);
	v2r c(0,3);

#define CHECK_PERMUTATIONS(res,a,b,c,p) \
	CHECK(res == TriangleContains(a, b, c, p)); \
	CHECK(res == TriangleContains(a, c, b, p)); \
	CHECK(res == TriangleContains(b, a, c, p)); \
	CHECK(res == TriangleContains(b, c, a, p)); \
	CHECK(res == TriangleContains(c, b, a, p)); \
	CHECK(res == TriangleContains(c, a, b, p)); \

  CHECK_PERMUTATIONS(true,a,b,c,v2r(1,1));
	CHECK_PERMUTATIONS(true,a, b, c, v2r(.2,2));
	CHECK_PERMUTATIONS(false, a, b, c, v2r(4,4));
	CHECK_PERMUTATIONS(false, a, b, c, v2r(-4,1));
	CHECK_PERMUTATIONS(false, a, b, c, v2r(1,-4));
}

bool operator==(const v2i &a, const v2i &b) {
	return a[0] == b[0] && a[1] == b[1];
}


TEST(vector_normals) {
	v2i px(1,0);
	v2i mx(-1,0);
	v2i py(0,1);
	v2i my(0,-1);


	//     | py
	// mx -o- px
	//  my |


	CHECK(VectorLeft(px) == py);
	CHECK(VectorLeft(py) == mx);
	CHECK(VectorLeft(mx) == my);
	CHECK(VectorLeft(my) == px);
	CHECK(VectorLeft(px) == py);

	CHECK(VectorRight(px) == my);
	CHECK(VectorRight(py) == px);
	CHECK(VectorRight(mx) == py);
	CHECK(VectorRight(my) == mx);
	CHECK(VectorRight(px) == my);
}

