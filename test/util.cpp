#include "Test.h"
#include "util/parse.h"
#include "math/Random.h"
#include <iostream>
#include <cstdio>
#include "types.h"
#include "macros.h"


using namespace util;

TEST(stringf) {
	CHECK(stringf("0x%08x", 15) == "0x0000000f");
}

TEST(changeCase) {
	CHECK(toLower("MiXeD CaSe") == "mixed case");
	CHECK(toUpper("MiXeD CaSe") == "MIXED CASE");
}

TEST(scopeAbuse) {
	string path("window.Width");
	string scope("window");
	string dscope("color");

	CHECK(startsWith("window.Width","window"));
	CHECK(!startsWith("window.Width","color"));
	CHECK( path.substr(scope.size()+1) == "Width");
}

TEST(compactSpaces) {
	CHECK( compactSpaces(" I think this  needs \t\t  less spaces, \n  don't you?  ")
		                 == "I think this needs less spaces, don't you?"
			);
	CHECK( compactSpaces("test\n") == "test");
}

TEST(substrings) {
	CHECK( smartAt("string", -1) == 'g');

	CHECK( startsWith("constantinople", "const"));
	CHECK(!startsWith("apple", "oranges"));
	CHECK(!startsWith("apple", "apples"));
	CHECK( endsWith("util.cpp", "cpp"));
	CHECK( endsWith("util.cpp", ".cpp"));
	CHECK( endsWith("util.cpp", ""));
	CHECK( endsWith("util.cpp", "util.cpp"));
	CHECK(!endsWith("util.cpp", ".h"));

}

TEST(contains) {
	CHECK( contains("google", "ogle"));
	CHECK(!contains("haystack", "needle"));
	CHECK(!contains("megherbi", "an ounce of sense"));
	CHECK( contains("var=value", '='));
	CHECK(!contains("var=value", ':'));
}

TEST(stringSplitting) {
	CHECK( before("asdf,jkl;",',') == "asdf" );
	CHECK( after ("asdf,jkl;",',') == "jkl;" );
	CHECK( before("var=value",'=') == "var" );
	CHECK( after ("var=value",'=') == "value" );
	
	CHECK( beforeLast("a,b,c,d",',') == "a,b,c" );
	CHECK( afterLast ("a,b,c,d",',') == "d" );

	CHECK(removeEnd("seventy-seven", 6) == "seventy");
	CHECK(removeEnd("there/", 1) == "there");

	std::vector<string> numbers = split("1,2,3,4,5,6,7,8,9", ',');
	loop(i, numbers.size()) {
		CHECK(numbers[i] == stringf("%d", i+1));
	}
	CHECK(split("a,b",',').size() == 2);
	CHECK(split("a,",',').size() == 2);
	CHECK(split("a",',').size() == 1);
}

TEST(stringJoins) {
	vector<string> test = split("a;b;c;d;e;f;g", ';');
	//show(test.size());
	//show(int('g'-'a'));
	CHECK(test.size() == ('g'-'a')+1);
	CHECK(join(test, ' ') == "a b c d e f g");
}

TEST(parseDecimal) {
	Random r;
	loop(i, 100) {
		u32 x = r;

		string s = stringf(" %d ",x);
		CHECK(parse<u32>(s).Get() == x);
	}
}

TEST(parseHex) {
	Random r;
	loop(i, 100) {
		u32 x = r;

		string s = stringf("0x%08x", x);
		CHECK(parse<u32>(s).Get() == x);
	}
}

TEST(parseBounds) {
 CHECK(parse<u8>("0x1ef").GetOrElse(0)        == 0xef);
 CHECK(parse<u8>("0xef").GetOrElse(0)         == 0xef);
 CHECK(parse<u16>("0x1beef").GetOrElse(0)     == 0xbeef);
 CHECK(parse<u16>("0xbeef").GetOrElse(0)      == 0xbeef);
 CHECK(parse<u32>("0x1deadbeef").GetOrElse(0) == 0xdeadbeef);
 CHECK(parse<u32>("0xdeadbeef").GetOrElse(0)  == 0xdeadbeef);
}

TEST(parseFloat) {
	Random r;
	loop(i, 100) {
		float x = r;

		//--- use lots of digits so we can test for "equality"
		string s = stringf("%.100f", x);
		CHECK(parse<float>(s).Get() == x);
	}
}

TEST(parseEmpty) {
	CHECK(parse<float>("").GetOrElse(1.0f) == 1.0f);
}

TEST(parseString) {
	string escaped ("  \"I like cats\\n\"  ");
	string expected("I like cats\n");
	string actual  (parse<string>(escaped).GetOrElse(""));
	CHECK(actual == expected);

	CHECK("abcdef" == parse<string>("\"abc\" \"def\"").GetOrElse("nope"));
}

TEST(escapeTest) {
	string input   ("\"\\\\ \\\' \\\" \\n \\t\"");
	string expected("\\ \' \" \n \t");
	string actual  (parse<string>(input).GetOrElse("derp"));
	CHECK(actual == expected);
}

TEST(int_types) {
#define CHECK_TYPE(width) \
	CHECK(sizeof(u##width) == sizeof(i##width)); \
	CHECK(sizeof(u##width) == (width/8));

	CHECK_TYPE(8)
	CHECK_TYPE(16)
	CHECK_TYPE(32)
	CHECK_TYPE(64)
}


