#include "Test.h"
#include "util/Arguments.h"
#include <iostream>
using namespace std;

//--- because writing this is a pain
#define ARG_ARRAY(name) const char * const name[] 

TEST(args) {
	ArgumentParser argsys(false); //--- hide printing for tests

	argsys.DefineFlag("help", "h","Display this help.");

	//cout << "\n" << argsys << "\n";

	ARG_ARRAY(test_good) = {"exe", "--help"};
	ARG_ARRAY(test_bad) = {"expected_failure", "-help"};

	CHECK(argsys.Parse(2, test_good).Valid());
	CHECK(!argsys.Parse(2, test_bad).Valid());
}

TEST(args2) {
	ArgumentParser argsys(false); //--- hide printing for tests

	argsys.DefineFlag("help", "h","Display this help.");
	argsys.DefineFlag("stupid", "s", "Process a stupidly formatted file.");
	argsys.DefineParameter("input file", "The file to process.");

	//cout << "\n" << argsys << "\n";

	ARG_ARRAY(test_missing_param) = {"missing_param", "--help"};
	ARG_ARRAY(test_unknown_param) = {"expected_failure", "-help"};
	ARG_ARRAY(test_good) = {"good", "fileName.conf", "--stupid"};
	ARG_ARRAY(test_short) = {"good", "fileName.conf", "-s"};

	//const char * const 
	CHECK(!argsys.Parse(2, test_missing_param).Valid());
	CHECK(!argsys.Parse(2, test_unknown_param).Valid());
	CHECK(argsys.Parse(3, test_good).Valid());
	CHECK(argsys.Parse(3, test_short).Valid());
}

