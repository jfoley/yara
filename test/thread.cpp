#include "Test.h"

#include "concur/Chrono.h"
#include "concur/Thread.h"
#include "concur/Lockable.h"

#include <iostream>
using namespace std;

class Shared: public Lockable{
	public:
	Shared(): data(0) {}
	int data;
};

class BgTask: public Thread{
	public:
	BgTask(Shared &s, u32 i) : shared(s), id(i) { }
	
	private:
	Shared &shared;
	u32 id;

	virtual void Run(){
		for(int i=0; i<50; i++){
			{
				shared.Lock();
				int tmp=shared.data;
				shared.data=tmp+1;
				Nap(0.001);
			}
			shared.Unlock();
			ForceYield();
		}
	}
	
};

TEST(threads) {
	Shared shared;

	{
		BgTask a(shared, 0), b(shared, 1);
		
		shared.Lock();
		a.Start();
		b.Start();
		shared.Unlock();

		b.Join();
		a.Join();
		// a.~Thread() takes care of a.Join()
	}
	
	CHECK(shared.data==100);
}

