#include "Test.h"
#include "net/Address.h"
#include "net/Socket.h"
#include "util/hexdump.h"

TEST(BasicAddress) {
	Address local;
	//show(local);

	Option<Address> dave = Address::Resolve("pr03.uml.edu");
	if(dave.Valid()) {
		CHECK(dave.Get() == Address(129,63,205,174));
	}

}

TEST(SocketSelf) {
	u16 port = 1234;
	Address local(127,0,0,1,port);
	Socket socket;
	
	CHECK(!socket.Valid());
	CHECK(socket.Open(port));
	CHECK(socket.Valid());
	socket.Close();
	CHECK(!socket.Valid());
	CHECK(socket.Open(port));
	CHECK(socket.Valid());

	const string msg = "This is some ridiculous test data.";
	
	//--- SEND
	CHECK(socket.Send(local, msg.c_str(), msg.size()));
	CHECK(socket.Valid());

	//--- RECEIVE
	{
		Address source;
		string buffer;
		buffer.resize(msg.size());
		Assert(buffer.size() >= msg.size());

		Option<size_t> result = socket.Receive(&source, &buffer[0], size_t(buffer.size()));
		CHECK(result.Valid());
		CHECK(result.Get() == msg.size());
		
		//--- on my machine, the source address is returned as all zeroes for localhost; weird.
		//CHECK(source == local);
		CHECK(buffer == msg);
	}
}
