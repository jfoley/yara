#include "Test.h"
#include "filesystem/FileSystem.h"
#include "filesystem/Path.h"
#include <cstdio>
#include "macros.h"

TEST(list_files) {
	Path root("/");
	vector<Path> files = root.List();
	CHECK(files.size());
	
	int etc = -1;

	loop(i, files.size()) {
		//show(files[i].Name());
		if(files[i].Name() == "etc")
			etc = i;
	}

	CHECK(etc != -1);
	CHECK(files[etc].Directory());

}

TEST(path_funcs) {
	CHECK(Path("/proc/cpuinfo").Exists());
	CHECK(Path("/proc/").Contains("/proc/cpuinfo"));
	CHECK(!Path("/home/").UpDir().Empty());
	CHECK(!Path(".").UpDir().Empty());
}

TEST(unique_dir) {
	string path = UniqueSubDir(".", "src");
	CHECK(path != "src");
}

TEST(dir_creation) {
	string unique = UniqueSubDir(".");
	CHECK(!Path(unique).Exists());
	CHECK(MakeDir(unique));
	CHECK(Path(unique).Exists());
	CHECK(Path(unique).Directory());
	CHECK(RemoveAll(unique));
	CHECK(!Path(unique).Exists());
}


