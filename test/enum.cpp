#include "Test.h"
#include "Day.h"
#include "macros.h"
#include <iostream>

TEST(DayEnum) {
	Day today = Day::Monday;

	//--- can we compare to u32s?
	const u32 test_day_value = Day::Monday;
	CHECK(today == test_day_value);
	CHECK(test_day_value == today);

	//--- can we compare to strings?
	const string test_day_str = "Monday";
	CHECK(test_day_str == today);
	CHECK(today == test_day_str);

	//--- can we compare to days? --- duh
	CHECK(Day::Monday == today);
	CHECK(today == Day::Monday);

	//--- can we use ostreams? yup
	//show(today);
}

