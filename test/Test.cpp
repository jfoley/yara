#include "Test.h"
#include "types.h"
#include "macros.h"
#include "Assert.h"
#include <cstdio>
#include <cstdlib>


// here's a disturbing feature of C++
// if this isn't allocated on the heap;
// it will be cleared after all my Adds happen.
static std::vector<Test*> *list;

void TestSystem::Add(Test* test) {
	if(list == 0) list = new std::vector<Test*>();

	list->push_back(test);
}

int main(int argc, char **argv) {
	if(list == 0) {
		printf("No tests to execute.\n");
		return 0;
	}

	//--- copy over
	std::vector<Test*> testList(*list);
	delete list;

	//--- for sake of never writing this part again
	int n = testList.size();
	//--- for an accumulated success
	bool success = true;

	printf("=====================================================\n");

	loop(i, n) {
		Test* t = testList[i];
		Assert(t);

		printf("(%3d/%3d) %-32s : ", i+1, n, t->Name());
		
		//--- by catching Assert here rather than letting libc++ catch it allows us to run tests after an assertion fails, not busting all of them
		try {
			t->Process();
		} catch (Assertion a) { 
			printf(START_RED "\nUnhandled Assertion: %s\n" END_COLOR, a.what());
			success = false;
			continue;
		}

		u32 good = 0;
		u32 subN = t->Count();

		if(subN == 0) {
			printf(" -- No subtests\n");
			continue;
		}

		loop(j, t->Count()) {
			const SubTest& s = t->Get(j);
			if(!s.result) {
				printf("\nCHECK(" START_RED "%s" END_COLOR ") failed -- see %s:%d", s.text, s.file, s.line);
			} else {
				good++;
			}
		}

		if(good == subN) {
			printf(START_GREEN "SUCCESS" END_COLOR);
		} else {
			printf("\n\n");
		}
		printf("\n");

		success = success && good;
	}

	return success ? EXIT_SUCCESS : EXIT_FAILURE;
}


TEST(Tautology) { CHECK(true); }

