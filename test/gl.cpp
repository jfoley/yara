#include "Test.h"
#include "types.h"
#include "platform/GLContext.h"


TEST(GLfloat) {
	CHECK(sizeof(GLfloat) == sizeof(float));
}

TEST(GLuint) {
	CHECK(sizeof(GLuint) == sizeof(u32));
}

