#include "Example.h"
#include "Test.h"
#include <cstdio>

#include <iostream>
#include <fstream>

#define TEST_DIR "test/"
#define NONEXISTANTFILE "abcdefghijklmnopqrstuv.docx"
#define TEMP_CONF TEST_DIR "temp.conf"

TEST(ConfigTool) {
	Example data;

#define CHECK_DEFAULTS \
	CHECK(data.Window.Width == 640); \
	CHECK(data.Window.Height == 480); \
	CHECK(data.FullScreen == true); \
	CHECK(data.Window.Title == "Config Test"); \
	CHECK(data.Window.A.B.C == 4);

#define CHECK_STUPID \
	CHECK(!data.FullScreen); \
	CHECK(data.Window.Width == 42); \
	CHECK(data.Window.Height == 42); \
	CHECK(data.Window.A.B.C == 42); \
	CHECK(data.Window.Title == "testing");

	CHECK_DEFAULTS

	//--- create file for loading
	{
		std::ofstream file;
		file.open(TEMP_CONF);
		CHECK(file.is_open());

		file << "u32 Window.Width = 42\n";
		file << "u32 Window.Height = 42\n";
		file << "bool FullScreen = no\n";
		file << "string Window.Title = \"testing\"\n";
		file << "u32 Window.A.B.C = 42\n";
	}

	CHECK(!data.LoadConfig(NONEXISTANTFILE));
	CHECK(data.LoadConfig(TEMP_CONF));

	CHECK_STUPID

	data.Defaults();

	CHECK_DEFAULTS

	CHECK(data.LoadConfig(TEMP_CONF));
	
	CHECK_STUPID

	CHECK(data.LoadConfig(TEST_DIR "Example.conf"));
	CHECK_DEFAULTS

	//--- remove our temporary config file
	remove(TEMP_CONF);

}
