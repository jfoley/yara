#include "Util.h"
#include "Assert.h"
#include <cmath>

i32 Wrap(i32 input, i32 size) {
	input %= size;
	while(input < 0)
		input += size;
	Assert(input < size && input >= 0);
	return input;
}

real Wrap(real input, real size) {
	input = fmod(input, size);
	if(input < 0) {
		input += size;
	}
	if(input == size)
		input = 0;
	
	bool assertion = input < size && input >= 0;
	if(!assertion) {
		show(input);
		show(size);
		Assert(0);
	}

	return input;
}

v2r Wrap(const v2r &in, real n) {
	return v2r( Wrap(in[0], n), Wrap(in[1], n) );
}

