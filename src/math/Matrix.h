
#ifndef __MATRIX_H
#define __MATRIX_H

#include <iostream>
#include "macros.h" // MIN

template <typename T>
class Matrix{
	public:
		// Constructors/Destructor
		Matrix(){ Init();	}
		Matrix(const Matrix<T>& other){ Init(); Copy(other); }
		Matrix(unsigned rows, unsigned cols, T* data){ Init(); FromArray(rows, cols, data); }
		template <typename F> Matrix(unsigned rows, unsigned cols, F func){ Init(); FromFunction(rows, cols, func); }
		~Matrix(){ Resize(0,0);	}

		// Assignement
		Matrix& operator= (const Matrix<T>& other){ return Copy(other);	}
		Matrix& Copy(const Matrix<T>& other){ DeepCopy(*this, other); return *this; }
		Matrix& Move(Matrix<T>& other){ Move(*this, other); return *this; }
		template <typename F> void FromFunction(unsigned rows, unsigned cols, F f);
		void FromArray(unsigned rows, unsigned cols, const T* t);

		void Fill(unsigned rows, unsigned cols, T val){ Resize(0,0); Resize(rows,cols,val); }
		void Fill(T val){ Fill(Rows(),Cols(),val); }

		// Dimensions
		unsigned Rows() const { return rows; }
		unsigned Cols() const { return cols; }
		unsigned Elements() const { return rows*cols; }

		// Expansion capacity
		unsigned RowsCapacity() const { return rowsCap; }
		unsigned ColsCapacity() const { return colsCap; }
		unsigned Capacity() const { return rowsCap*colsCap; }

		// Memory Realloc
		void Resize(unsigned newRows, unsigned newCols);
		void Resize(unsigned newRows, unsigned newCols, const T& defaultValue);

		// Element accessors/mutator
		T& operator() (unsigned row, unsigned col){ return data[Index(row,col)]; }
		const T& operator() (unsigned row, unsigned col) const { return data[Index(row,col)]; }
		Matrix<T> SubMatrix(unsigned rows, unsigned cols, unsigned ri, unsigned ci) const;
		Matrix<T> Row(unsigned row) const { return SubMatrix(1, Cols(), row, 0); }
		Matrix<T> Col(unsigned col) const { return SubMatrix(Rows(), 1, 0, col); }

		// Row operations
		void RowSwap(unsigned r1, unsigned r2);
		void RowAdd(unsigned dst, unsigned src, T ratio=1);
		void RowMul(unsigned row, T ratio);

		// Column operations
		void ColSwap(unsigned c1, unsigned c2);

		// Arithmetic
		Matrix<T>  operator-  () const { return (*this)*(-1); }
		Matrix<T>& operator+= (const Matrix<T>& other){ return Add(*this, other); }
		Matrix<T>& operator-= (const Matrix<T>& other){ return Add(*this, other, -1); }
		Matrix<T>& operator*= (const Matrix<T>& other){ return Mul(*this, other); }
		Matrix<T>  operator+  (const Matrix<T>& other) const { return Matrix<T>(*this)+=other; }
		Matrix<T>  operator-  (const Matrix<T>& other) const { return Matrix<T>(*this)-=other; }
		Matrix<T>  operator*  (const Matrix<T>& other) const { return Matrix<T>(*this)*=other; }
		Matrix<T>& operator+= (T scalar){ AddIdentity( scalar); return *this; }
		Matrix<T>& operator-= (T scalar){ AddIdentity(-scalar); return *this; }
		Matrix<T>& operator*= (T scalar){ return Mul(*this,   scalar); }
		Matrix<T>& operator/= (T scalar){ return Mul(*this, 1/scalar); }
		Matrix<T>  operator+  (T scalar) const { return Matrix<T>(*this)+=scalar; }
		Matrix<T>  operator-  (T scalar) const { return Matrix<T>(*this)-=scalar; }
		Matrix<T>  operator*  (T scalar) const { return Matrix<T>(*this)*=scalar; }
		Matrix<T>  operator/  (T scalar) const { return Matrix<T>(*this)/=scalar; }
		friend Matrix<T> operator+ (T scalar, const Matrix<T>& A) { return ( A)+scalar; }
		friend Matrix<T> operator- (T scalar, const Matrix<T>& A) { return (-A)+scalar; }
		friend Matrix<T> operator* (T scalar, const Matrix<T>& A) { return ( A)*scalar; }

		void AddIdentity(T scalar=1);
		static Matrix<T>& Add(Matrix<T>& A, const Matrix<T>& B, T scalar=1.0);
		static Matrix<T>& Mul(Matrix<T>& A, T scalar);
		static Matrix<T>& Mul(Matrix<T>& A, const Matrix<T>& B, T scale=1.0);

		Matrix<T> Inverse(T* determinent=0) const;
		Matrix<T> Transpose() const;
		T Trace() const;

		// Misc operations
		static Matrix<T> Identity(unsigned n);
		static T DotProduct(const Matrix<T>& A, const Matrix<T>& B);
		T EuclideanNormSqared() const { return DotProduct(*this, *this); };
		T EuclideanNorm() const { return sqrt(EuclideanNormSqared()); };

		// Stream operations
		void Print(std::ostream& os) const;
		void Scan(std::istream& is);

	private:
		unsigned Index(unsigned row, unsigned col) const;
		void Init();
		static void Move(Matrix& dst, Matrix& src);	
		static void DeepCopy(Matrix& dst, const Matrix& src);
		static void ShallowCopy(Matrix& dst, const Matrix& src);

		unsigned rows;
		unsigned cols;
		unsigned rowsCap;
		unsigned colsCap;
		T* data;
};


template <typename T>
inline unsigned Matrix<T>::Index(unsigned row, unsigned col) const {
	if(row>=Rows() || col>=Cols())
		throw "Matrix::Index: Index out of bounds.";
	return row*colsCap+col;
}

template <typename T>
inline std::ostream& operator << (std::ostream& os, const Matrix<T>& m){
	m.Print(os);
	return os;
}

template <typename T>
inline std::istream& operator >> (std::istream& is, Matrix<T>& m){
	m.Scan(is);
	return is;
}

///////////////////////////////////////////////////////////


static unsigned CeilNextPowerOf2(unsigned x){
	x--;
	x = (x >>  1) | x;
	x = (x >>  2) | x;
	x = (x >>  4) | x;
	x = (x >>  8) | x;
	x = (x >> 16) | x;
	x++;
	return x;
}

template <typename T>
void Matrix<T>::DeepCopy(Matrix& dst, const Matrix& src){
	if(&dst==&src)
		return;
	dst.Resize(0,0);
	dst.Resize(src.Rows(), src.Cols());
	for(unsigned i=0; i<src.Rows(); i++)
		for(unsigned j=0; j<src.Cols(); j++)
			dst(i,j)=src(i,j);
}

template <typename T>
void Matrix<T>::ShallowCopy(Matrix& dst, const Matrix& src){
	dst.rows=src.rows;
	dst.cols=src.cols;
	dst.rowsCap=src.rowsCap;
	dst.colsCap=src.colsCap;
	dst.data=src.data;
}

template <typename T>
void Matrix<T>::Move(Matrix& dst, Matrix& src){
	ShallowCopy(dst,src);
	src.Init();
}

template <typename T>
void Matrix<T>::Init(){
	rowsCap=0;
	colsCap=0;
	rows=0;
	cols=0;
	data=0;
}

template <typename T>
template <typename F>
void Matrix<T>::FromFunction(unsigned rows, unsigned cols, F f){
	Resize(rows,cols);
	for(unsigned i=0; i<rows; i++){
		for(unsigned j=0; j<cols; j++){
			T& el=(*this)(i,j);
			el=f(i,j,el);
		}
	}
}

template <typename T>
void Matrix<T>::FromArray(unsigned rows, unsigned cols, const T* t){
	Resize(rows, cols);
	for(unsigned i=0; i<rows; i++)
		for(unsigned j=0; j<cols; j++)
			(*this)(i,j)=t[i*cols+j];
}

template <typename T>
void Matrix<T>::Resize(unsigned newRows, unsigned newCols){
	// Don't resize if nothing changes
	if(newRows==Rows() && newCols==Cols())
		return;
	
	// ReInit if Resize(0,0)
	if(newRows==0 || newCols==0){
		if(data)
			delete[] data;
		Init();
		return;
	}

	unsigned oughtRows=CeilNextPowerOf2(newRows);
	unsigned oughtCols=CeilNextPowerOf2(newCols);

	if(newRows<=rowsCap && newCols<=colsCap){
		// We already have enough memory to expand into
		rows=newRows;
		cols=newCols;
	}
	else{
		// More memory is needed

		// Copy current matrix to "old"
		Matrix old;
		Move(old,*this);

		// Construct new matrix
		rows=newRows;
		cols=newCols;
		rowsCap=oughtRows;
		colsCap=oughtCols;
		data=new T[Capacity()];

		// Copy old elements unsigned to new matrix
		for(unsigned i=0; i<MIN(old.Rows(), Rows()); i++)
			for(unsigned j=0; j<MIN(old.Cols(), Cols()); j++)
				(*this)(i,j)=old(i,j);

		// old.~Matrix() implicitly deletes old memory
	}
}

template <typename T>
void Matrix<T>::Resize(unsigned newRows, unsigned newCols, const T& defaultValue){
	unsigned oldRows=rows;
	unsigned oldCols=cols;

	Resize(newRows, newCols);

	for(unsigned i=0; i<newRows; i++)
		for(unsigned j=oldCols; j<newCols; j++)
			(*this)(i,j)=defaultValue;

	for(unsigned i=oldRows; i<newRows; i++)
		for(unsigned j=0; j<oldCols; j++)
			(*this)(i,j)=defaultValue;
}


template <typename T>
void Matrix<T>::RowSwap(unsigned r1, unsigned r2){
	if(r1>=Rows() || r2>=Rows())
		throw "Matrix::RowSwap: Invalid row.";
	
	if(r1==r2)
		return;

	for(unsigned j=0; j<Cols(); j++){
		// Store references rather than calling operator() for every access
		T& a=(*this)(r1,j);
		T& b=(*this)(r2,j);

		// Swap
		T tmp=a;
		a=b;
		b=tmp;
	}
}

template <typename T>
void Matrix<T>::ColSwap(unsigned c1, unsigned c2){
	if(c1>=Rows() || c2>=Rows())
		throw "Matrix::ColSwap: Invalid column.";
	if(c1==c2)
		return;

	for(int i=0; i<Rows(); i++){
		// Store references rather than calling operator() for every access
		T& a=(*this)(i, c1);
		T& b=(*this)(i, c2);

		// Swap
		T tmp=a;
		a=b;
		b=tmp;
	}
}

template <typename T>
Matrix<T> Matrix<T>::SubMatrix(unsigned rows, unsigned cols, unsigned ri, unsigned ci) const {
	Matrix<T> m;
	
	if(ri+rows > Rows() || ci+cols > Cols())
		throw "Matrix::SubMatrix: Out of bounds.";
	
	m.Resize(rows,cols);
	for(unsigned i=0; i<rows; i++)
		for(unsigned j=0; j<cols; j++)
			m(i,j)=(*this)(i+ri, j+ci);
	return m;
}

template <typename T>
void Matrix<T>::Print(std::ostream& os) const {
	os << Rows() << " " << Cols() << "\n";
	for(unsigned i=0; i<Rows(); i++){
		for(unsigned j=0; j<Cols(); j++){
			os << (*this)(i,j) << "\t";
		}
		if(i!=Rows()-1)
			os << "\n";
	}
}

template <typename T>
void Matrix<T>::Scan(std::istream& is){
	unsigned r;
	unsigned c;
	is >> r >> c;
	Resize(r,c);

	for(int i=0; i<Rows(); i++)
		for(int j=0; j<Cols(); j++)
			is >> (*this)(i,j);

}

template<typename T>
Matrix<T>& Matrix<T>::Add(Matrix<T>& A, const Matrix<T>& B, T scalar){
	if(A.Cols()!=B.Cols() || A.Rows()!=B.Rows())
		throw "Matrix::Add: Mismatched dimentions.";

	for(unsigned i=0; i<A.Rows(); i++)
		for(unsigned j=0; j<A.Cols(); j++)
			A(i,j)+=B(i,j)*scalar;
	return A;
}

template<typename T>
Matrix<T>& Matrix<T>::Mul(Matrix<T>& A, T scalar){
	for(unsigned i=0; i<A.Rows(); i++)
		for(unsigned j=0; j<A.Cols(); j++)
			A(i,j)*=scalar;
	return A;
}

template<typename T>
Matrix<T>& Matrix<T>::Mul(Matrix<T>& A, const Matrix<T>& B, T scale){
	Matrix<T> m;
	if(A.Cols()!=B.Rows())
		throw "Matrix::operator*=: Mismatched dimentions.";

	m.Resize(A.Rows(), B.Cols());

	for(unsigned i=0; i<A.Rows(); i++){
		for(unsigned j=0; j<B.Cols(); j++){
			T el=0;
			for(unsigned k=0; k<A.Cols(); k++)
				el+=A(i,k)*B(k,j);
			m(i,j)=el;
		}
	}
	
	Move(A, m);
	return A;
}

template<typename T>
void Matrix<T>::RowAdd(unsigned dst, unsigned src, T ratio){
	if(src>=Rows() || dst>=Rows())
		throw "Matrix::RowAdd: Invalid Row.";
	for(unsigned j=0; j<Cols(); j++)
		(*this)(dst, j)+=(*this)(src, j) * ratio;
}

template<typename T>
void Matrix<T>::RowMul(unsigned row, T ratio){
	RowAdd(row,row, ratio-1);
}


template <typename T>
Matrix<T> Matrix<T>::Transpose() const{
	Matrix<T> m;
	m.Resize(Cols(), Rows());
	for(unsigned i=0; i<Rows(); i++)
		for(unsigned j=0; j<Cols(); j++)
			m(j,i)=(*this)(i,j);
	return m;
}

template <typename T>
T Matrix<T>::Trace() const{
	T sum=0;
	for(unsigned i=0; i<Rows() || i<Cols(); i++)
			sum+=(*this)(i,i);
	return sum;
}

template <typename T>
void Matrix<T>::AddIdentity(T scalar){
	for(unsigned i=0; i<MIN(Rows(), Cols()); i++)
		(*this)(i,i)+=scalar;
}


template<typename T>
Matrix<T> Matrix<T>::Inverse(T* determinent) const {
	if(Rows()!=Cols())
		throw "Matrix<T>::Inverse: Not a square matrix.";

	Matrix m(*this);

	// Right-Append Identity
	m.Resize(m.Rows(), 2*m.Cols(), 0);
	for(unsigned k=0; k<m.Rows(); k++)
		m(k,k+m.Cols()/2)=1;
	
	T det=1;

	// Reduce lower triangle to zeros
	for(unsigned i=0; i<Rows(); i++){

		/* Swap rows for largest pivot*/ {
			T maxPivot=-1;
			unsigned l=i;
			for(unsigned k=i; k<Rows(); k++){
				T pivot=abs(m(k,i));
				if(pivot > maxPivot){
					l=k;
					maxPivot=pivot;
				}
			}
			if(i!=l)
				m.RowSwap(i,l);
		}

		T pivot=m(i,i);
		det*=pivot;

		if(pivot==0)
			continue;

		m.RowMul(i, 1/pivot);
		for(unsigned k=i+1; k<Rows(); k++)
			m.RowAdd(k, i, -m(k,i));
	}

	if(det!=det)
		det=T(0);

	if(determinent)
		*determinent=det;

	// Reduce upper triangle to zeros
	for(unsigned i=Rows()-1; i>=0 && i<Rows(); i--){
		if(m(i,i)==0)
			continue;
		for(unsigned k=i-1; k>=0 && k<Rows(); k--)
			m.RowAdd(k, i, -m(k,i));
	}

	// Remove left half of double matrix
	for(unsigned i=0; i<Rows(); i++)
		for(unsigned j=0; j<Cols(); j++)
			m(i,j)=m(i,j+Cols());

	m.Resize(Rows(), Cols());

	return m;
}

template <typename T>
Matrix<T> Matrix<T>::Identity(unsigned n){
	Matrix<T> m;
	m.Resize(n,n,0);
	for(unsigned i=0; i<n; i++)
		m(i,i)=1;
	return m;
}

template <typename T>
T Matrix<T>::DotProduct(const Matrix<T>& A, const Matrix<T>& B){
	if(A.Rows()!=B.Rows() || A.Cols()!=B.Cols())
		throw "Matrix::DotProduct: Mismatched dimensions.";
	T sum=0;
	for(unsigned i=0; i<A.Rows(); i++)
		for(unsigned j=0; j<A.Cols(); j++)
			sum+=A(i,j)*B(i,j);
	return sum;
}

/*
template <typename T>
T Matrix<T>::EuclideanNormSqared() const {
	return DotProduct(*this,*this);
}

template <typename T>
T Matrix<T>::EuclideanNorm() const {
	return sqrt(EuclideanNormSqared());
}*/


#endif
