#include "geom.h"
#include "const.h" // PI
#include "Assert.h"

bool SegmentsIntersect(v2r a, v2r b, v2r c, v2r d) {
	const real ax = X(a);
	const real bx = X(b);
	const real cx = X(c);
	const real dx = X(d);
	const real ay = Y(a);
	const real by = Y(b);
	const real cy = Y(c);
	const real dy = Y(d);

	const real overd = 1 / ((dx-cx)*(ay-by)-(ax-bx)*(dy-cy));
	const real s = overd*( (ay-by)*(ax-cx) + (bx-ax)*(ay-cy) );
	const real t = overd*( (cy-dy)*(ax-cx) + (dx-cx)*(ay-cy) );

	//show(s);
	//show(t);

	return 0 < t && t < 1 && 0 < s && s < 1;
}

bool TriangleContains(const v2r &a, const v2r &b, const v2r &c, const v2r &p) {
	v2r bc = b-c;
	v2r ba = b-a;
	real BC = bc.Magnitude();
	real BA = ba.Magnitude();
	v2r x = bc.Normalized();
	v2r y = ba.Normalized();
	v2r pb = b-p;
	real t = v2r::DotProduct(pb,x)/BC;
	real s = v2r::DotProduct(pb,y)/BA;
	return (s >= 0) && (t >= 0) && (s <= 1) && (t<=1);
}

/// Returns true if Convex Quad ABCD contains P
bool QuadContains(const v2r &a, const v2r &b, const v2r &c, const v2r &d, const v2r &p) {
	return TriangleContains(a,b,c,p) && TriangleContains(d,b,c,p);
}

v2r VectorRight(v2r input) {
	return v2r(input[1], -input[0]);
}

v2r VectorLeft(v2r input) {
	return v2r(-input[1], input[0]);
}

vector<v3r> SplitSegment(const v3r &a, const v3r &b, const v3r &da, const v3r &db, const real size) {
	vector<v3r> results;

	const v3r segment = b-a;
	const real dist = segment.Magnitude();

	if(size > dist) // no need to split this segment
		return results;

	u32 numSteps = (dist / size)-1;
	//real dstep = dist/numSteps;
	//real dstep2 = dstep*dstep;

	uloop(i, numSteps) {
		real t = RATIO(i,numSteps);

		v3r cur = EvalBezier<v3r>(a, da, db, b, t);

		if(MostlyEqual(cur,a,size/2))
			continue;
		if(MostlyEqual(cur,b,size/2))
			break;

		results.push_back(cur);
	}
	return results;
}


