
#ifndef __CALIBRATION_H
#define __CALIBRATION_H

#include <iostream>
#include <sstream>
#include "types.h"
#include "util/parse.h"

struct Calibration { 
	real lower, upper;
	real lowerp, upperp;
	
	Calibration(real l=0.0, real u=1.0, real lp=0.0, real up=1.0):
		lower(l),
		upper(u),
		lowerp(lp),
		upperp(up) {}

	real operator() (real x) const {
		return (x-lower)/(upper-lower)*(upperp-lowerp)+lowerp;
	}

	void Print(std::ostream& os) const {
		os << lower << " " << upper << " " << lowerp << " " << upperp;
	}

	void Scan(std::istream& is){
		is >> lower >> upper >> lowerp >> upperp;
	}

	string ToString() const {
		stringstream ss;
		Print(ss);
		return ss.str();
	}

	static Calibration FromString(string s){
		stringstream ss;
		Calibration c;
		ss << s;
		c.Scan(ss);
		return c;
	}

};


#endif
