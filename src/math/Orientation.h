
#ifndef __ORIENTATION_H
#define __ORIENTATION_H

#include "math/seq.h"
#include "math/const.h"

class Orientation {
	public:
		enum EgoDirection {
			RIGHTWARD = 0,
			UPWARD    = 1,
			FORWARD   = 2,
		};

		Orientation(
				const v3r& A=AXIS_Z,
				const v3r& B=AXIS_Y,
				EgoDirection primary=FORWARD,
				EgoDirection secondary=UPWARD ) { Set(A,B,primary,secondary); }

		void Set(
				const v3r& A=AXIS_Z,
				const v3r& B=AXIS_Y,
				EgoDirection primary=FORWARD,
				EgoDirection secondary=UPWARD);

		const v3r& Get(EgoDirection dir); // Performs bounds checking
		const v3r& operator[] (EgoDirection dir) const { return ego[dir]; }; // No bounds checking
		
		const v3r& GetForward() const { return F(); }
		const v3r& GetUp()      const { return U(); }
		const v3r& GetRight()   const { return R(); }
		
		const v3r& F() const { return ego[FORWARD];   }
		const v3r& U() const { return ego[UPWARD];    }	
		const v3r& R() const { return ego[RIGHTWARD]; }
		
		void SetYPR(const v3r& ypr);
		v3r  GetYPR() const;
		
		void Increment(real dy, real dp, real dr, real scale=1.0);
	
	private:
		v3r ego[3];
};

#endif

