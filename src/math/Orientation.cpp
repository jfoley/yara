
#include "Orientation.h"
#include "Assert.h"

namespace {
	v3r ypr_to_spherical(const v3r& ypr){
		return v3r(
				cos(ypr[0])*cos(ypr[1]),
				sin(ypr[0])*cos(ypr[1]),
				sin(ypr[1]));
	}

	void OrthoRotate(v3r* A, v3r* B, real d){
		v3r  a=*A;
		v3r  b=*B;
		real c=cos(d);
		real s=sin(d);
		*A=a*c+b*s;
		*B=b*c-a*s;
	}

	void OrientRotate(v3r* F, v3r* U, v3r* R, real dy, real dp, real dr){
		OrthoRotate(R,F,dy); // Yaw
		OrthoRotate(F,U,dp); // Pitch
		OrthoRotate(R,U,dr); // Roll
	}
}

const v3r& Orientation::Get(EgoDirection dir){
	if(0 <= dir && dir < 3)
		return ego[dir];
	cerr << "Orientation::Get: Invalid direction: " << dir << "\n";
	return ego[0];
}

void Orientation::Set(const v3r& A, const v3r& B, EgoDirection primary, EgoDirection secondary){
	Assert(A.MagnitudeSquared() != 0 && B.MagnitudeSquared() != 0);
	
	EgoDirection tritiary=EgoDirection( primary ^ secondary ^ 3 ); // (0,1 => 2) (0,2 => 1) (1,2 => 0)
	if(0 <= tritiary && tritiary < 3){
		ego[primary  ]=A;
		ego[primary  ].Normalize();
		ego[tritiary ]=v3r::CrossProduct(B,A).Normalize(); 
		ego[secondary]=v3r::CrossProduct(ego[primary],ego[tritiary]);
		return;
	}
	cerr << "Orientation::Set: Invalid primary,secondary pair (" << primary << "," << secondary << ")\n";
}

void Orientation::SetYPR(const v3r& ypr){
	ego[FORWARD]=ypr_to_spherical(ypr);
	ego[UPWARD]=ypr_to_spherical(ypr+v3r(0,PI/2,0));
	ego[RIGHTWARD]=v3r::CrossProduct(ego[FORWARD], ego[UPWARD]).Normalize();
}

v3r Orientation::GetYPR() const {
	real yaw=atan2(ego[FORWARD][1],ego[FORWARD][2]);
	real pitch=atan2(ego[FORWARD][2], (ego[FORWARD]*v3r(1,1,0)).Magnitude());
	v3r unr=ypr_to_spherical(v3r(yaw, pitch+PI/2, 0));
	real roll=v3r::SignedAngle(ego[UPWARD], unr, ego[FORWARD]);
	return v3r(yaw,pitch,roll);
}

void Orientation::Increment(real dy, real dp, real dr, real scale){
	OrientRotate(&ego[FORWARD], &ego[UPWARD], &ego[RIGHTWARD], dy*scale, dp*scale, dr*scale);
}

