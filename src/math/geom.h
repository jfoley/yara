#ifndef __GEOM_H
#define __GEOM_H

#include "seq.h"

//--- for treating seq as vectors
#define X(pt) pt[0]
#define Y(pt) pt[1]
#define Z(pt) pt[2]

/// Determines whether line segments AB and CD intersect
bool SegmentsIntersect(v2r a, v2r b, v2r c, v2r d);

/// Returns true if triangle ABC contains point P
bool TriangleContains(const v2r &a, const v2r &b, const v2r &c, const v2r &p);

/// Returns true if Convex Quad ABCD contains P
bool QuadContains(const v2r &a, const v2r &b, const v2r &c, const v2r &d, const v2r &p);

vector<v3r> SplitSegment(const v3r &a, const v3r &b, const v3r &da, const v3r &db, const real size);

/// MostlyEqual takes two vectors of the same order and compares them by location
template <class T>
bool MostlyEqual(const T &a, const T &b, real tolerance) {
	return (a-b).MagnitudeSquared() < tolerance*tolerance;
}

/// Evaluates a n-dimension cubic bezier curve
/// see: http://en.wikipedia.org/wiki/Bezier_curve 
template <class T>
T EvalBezier(const T &p0, const T &p1, const T &p2, const T &p3, real t) {
	const real s = 1-t;
	const real t2 = t*t;
	const real s2 = s*s;
	const real t3 = t2*t;
	const real s3 = s2*s;

	return s3*p0 + (3*s2*t)*p1 + (3*s*t2)*p2 + t3*p3;
}

/// Return right hand normal in 2d
v2r VectorRight(v2r input);

/// Return left-hand normal in 2d
v2r VectorLeft(v2r input);



#endif

