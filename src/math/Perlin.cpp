#include "Perlin.h"
#include "math/Random.h"
#include <cstdio>

//--- this is why we need double the hash array
#define UNHASH(a,b,c) (hash[(c)+hash[(b)+ hash[(a)]]])

//--- this is to use integer casts rather than floor()
#define FLOOR(T, f)   (((f)<0) ? T((f)-1) : T(f))

//--- fifth degree polynomial for curve interpolation
//    6t^5+15t^4+10t^3
//    since this has derivative at zero at zero and one, the noise function will have a continuous second derivative everywhere
#define FADE(t)       (t*t*t*(t*(t*6-15)+10))
//#define FADE(t) ( t*t*(3. - (2. * t)))

namespace {
	v3r gradients[] = {
      v3r( 1., 1., 0.),
      v3r(-1., 1., 0.),
      v3r( 1.,-1., 0.),
      v3r(-1.,-1., 0.),
      v3r( 1., 0., 1.),
      v3r(-1., 0., 1.),
      v3r( 1., 0.,-1.),
      v3r(-1., 0.,-1.),
      v3r( 0., 1., 1.),
      v3r( 0.,-1., 1.),
      v3r( 0., 1.,-1.),
      v3r( 0.,-1.,-1.)
    };
};


//--- beware: Math notation for period conflicts with template notation
Perlin::Perlin(u32 T, u32 s) {
	period = T;
	seed = s;
	Random rng(seed);

	hash.resize(T*2 + 2);

	//--- fill the array with indices 0-255
	loop(i, T) { hash[i] = i; }

	//--- Fisher-Yates shuffle
	//    http://en.wikipedia.org/wiki/Fisher-Yates_shuffle
	//    with the added complexity, of doubling the array
	//    eg. hash[0] = hash[256]
	//
	//    look, I used rloop again :)
	rloop(i, T) {
		u32 r = (i==0) ? 0 : u32(rng) % i;

		//--- swap and copy to i+T
		hash[i+T] = hash[r];
		hash[r]   = hash[i];
		hash[i]   = hash[i+T];
	}
	hash[T*2   ] = hash[0];
	hash[T*2 +1] = hash[1];
}

real Perlin::Get(real x, real y, real z) const {
	real xc, yc, zc;
	int  xi, yi, zi;

	xc = x - (int) x;
	yc = y - (int) y;
	zc = z - (int) z;

	xi = int(x) % GetPeriod();
	yi = int(y) % GetPeriod();
	zi = int(z) % GetPeriod();

	real rx0 = xc;
	real rx1 = xc-1.;
	real ry0 = yc;
	real ry1 = yc-1.;
	real rz0 = zc;
	real rz1 = zc-1.;

	//--- now interpolate between those reference normals
	real ut = FADE(xc);
	real vt = FADE(yc);
	real wt = FADE(zc);
	real n[8];

	u32 NumGradients = lengthof(gradients);
	n[0] = v3d::DotProduct(gradients[ UNHASH(  xi,   yi,   zi) % NumGradients], v3d(rx0,ry0,rz0));
	n[4] = v3d::DotProduct(gradients[ UNHASH(1+xi,   yi,   zi) % NumGradients], v3d(rx1,ry0,rz0));
	n[1] = v3d::DotProduct(gradients[ UNHASH(  xi, 1+yi,   zi) % NumGradients], v3d(rx0,ry1,rz0));
	n[5] = v3d::DotProduct(gradients[ UNHASH(1+xi, 1+yi,   zi) % NumGradients], v3d(rx1,ry1,rz0));
	n[2] = v3d::DotProduct(gradients[ UNHASH(  xi,   yi, 1+zi) % NumGradients], v3d(rx0,ry0,rz1));
	n[6] = v3d::DotProduct(gradients[ UNHASH(1+xi,   yi, 1+zi) % NumGradients], v3d(rx1,ry0,rz1));
	n[3] = v3d::DotProduct(gradients[ UNHASH(  xi, 1+yi, 1+zi) % NumGradients], v3d(rx0,ry1,rz1));
	n[7] = v3d::DotProduct(gradients[ UNHASH(1+xi, 1+yi, 1+zi) % NumGradients], v3d(rx1,ry1,rz1));

	const real nx0 = LERP(n[0], n[4], ut);
	const real nx1 = LERP(n[1], n[5], ut);
	const real nx2 = LERP(n[2], n[6], ut);
	const real nx3 = LERP(n[3], n[7], ut);

	const real ny0 = LERP(nx0, nx1, vt);
	const real ny1 = LERP(nx2, nx3, vt);

	return LERP(ny0, ny1, wt);
}

