// Random.hpp

#ifndef __RANDOM_H
#define __RANDOM_H

#include "types.h"
#include <ctime>

class Random
{
	public:
		Random(){ 
			static u32 rndseed=0;
			init(time(0)+rndseed++); 
		}
		Random(u64 seed){
			init(seed);
		}
		operator bool  () { return Get32()%2; }
		operator u8    () { return Get32(); }
		operator u16   () { return Get32(); }
		operator u32   () { return Get32(); }
		operator u64   () { return ((u64(Get32())<<32)+Get32()); }
		operator i8    () { return Get32(); }
		operator i16   () { return Get32(); }
		operator i32   () { return Get32(); }
		operator i64   () { return u64(*this); }
		operator float () { return (Get32()/(0xFFFFFFFF+1.0)); }
		operator double() { return double(float(*this))/(0xFFFFFFFF+1.0)+float(*this); }

	private:
		u64 x; // Random state variable
		u64 a; // Coefficient
		u64 c; // Carry

		u32 Get32()
		{
			x=a*x+c;
			c=x>>32;
			return (x=u32(x));
		}

		void init(u64 seed){
			x=u32(seed);
			a=18782;
			c=362436+(seed>>32);
		}
};

#endif
