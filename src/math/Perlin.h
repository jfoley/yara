#ifndef __PERLIN_H
#define __PERLIN_H

#include "math/seq.h"
#include <vector>
using std::vector;

/// An implementation of 3d perlin noise
class Perlin {
	public:
		Perlin(u32 period=256, u32 seed = 42);
		
		u32  GetPeriod() const { return period; }
		real Get(real x, real y, real z) const;
		real operator()(real x, real y, real z) const { return Get(x,y,z); }
	private:
		u32 period;
		u32 seed;
		vector<u32>  hash;
};



#endif

