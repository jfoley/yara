// seq.h

#ifndef __SEQ_H
#define __SEQ_H

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <cmath>
#include "macros.h"

//////////////////////////////////////////////////////////////////////
// Macros to avoid annoying repetition for operator overloading

// Defines operations like: seq<N,T> + seq<N,T>	
// Operations are performed per index
#define SEQ_ARITHMETIC(OPER) \
	seq<N,T> operator OPER (const seq<N,T>& other) const { \
		seq<N,T> r; \
		loop(i,N) \
		r[i]=data[i] OPER other.data[i]; \
		return r; \
	}

// Defines operations like: seq<N,T> + scalar and scalar + seq<N,T>
// Operations are performed per index
#define SEQ_ARITHMETIC_EQUALS(OPER) \
	seq<N,T>& operator OPER##= (const seq<N,T>& other){ \
		loop(i,N) \
		data[i] OPER##= other.data[i]; \
		return *this; \
	}

// Defines operations like: seq<N,T> += seq<N,T>	
// Operations are performed per index
#define SEQ_ARITHMETIC_SCALAR(OPER) \
	template <typename U> seq<N,T> operator OPER (const U& scalar) const { \
	seq<N,T> r; \
	loop(i,N) \
	r[i]=data[i] OPER scalar; \
	return r; \
} \
template <typename U> friend seq<N,T> operator OPER (const U& scalar, const seq<N,T>& o){ \
	return o OPER scalar; \
}

// Defines operations like: seq<N,T> += scalar
// Operations are performed per index
#define SEQ_ARITHMETIC_SCALAR_EQUALS(OPER) \
	template <typename U> seq<N,T>& operator OPER##= (const U& scalar) { \
	loop(i,N) \
		data[i] OPER##= scalar; \
	return *this; \
}

#define SEQ_OPERS(OPER) \
	SEQ_ARITHMETIC(OPER) \
	SEQ_ARITHMETIC_SCALAR(OPER) \
	SEQ_ARITHMETIC_EQUALS(OPER) \
	SEQ_ARITHMETIC_SCALAR_EQUALS(OPER)

// Template qualifiers
#define SEQ_TEMPLATE(N, T) template <size_t N, typename T>
#define SEQNT SEQ_TEMPLATE(N,T)
#define SEQMU SEQ_TEMPLATE(M,U)


//////////////////////////////////////////////////////////////////////
// Class Definition
SEQNT struct seq {
	// Data Members
	T data[N];

	// Constructor
	explicit seq(const T& a=0, const T& b=0, const T& c=0, const T& d=0);

	// Assignment
	SEQMU seq<N,T>& operator= (const seq<M,U>& other);

	// Type cast
	SEQMU operator seq<M,U> () const;

	// Algebraic Operators
	seq<N,T> operator-() const { return seq<N,T>()-*this; }
	SEQ_OPERS(+);
	SEQ_OPERS(-);
	SEQ_OPERS(*);
	SEQ_OPERS(/);

	// Setter/Getter
	const T& operator[](unsigned d) const { return data[d]; }
	      T& operator[](unsigned d)       { return data[d]; }

	// Geometric Operations
	seq<N,T>& Normalize        ()       { (*this)/=sqrt(MagnitudeSquared()); return *this; }
	seq<N,T>  Normalized       () const { seq<N,T> v=*this; return v.Normalize(); }
	double    MagnitudeSquared () const { return DotProduct(*this, *this); }
	double    Magnitude        () const { return sqrt(MagnitudeSquared()); }

	static T        DotProduct   (const seq<N,T>& a, const seq<N,T>& b);
	static seq<N,T> CrossProduct (const seq<N,T>& a, const seq<N,T>& b);
	static double   Angle        (const seq<N,T>& a, const seq<N,T>& b);
	static double   SignedAngle  (const seq<N,T>& a, const seq<N,T>& b, const seq<N,T>& positive);
	static seq<N,T> Projection   (const seq<N,T>& of, const seq<N,T>& into);

	// Misc. Operations
	static seq<N,T> Normal(const seq<N,T> a, const seq<N,T>& b, const seq<N,T>& c){	return seq<N,T>::CrossProduct(b-a,c-b);	}
	seq<N,T> Round(double r=1.0) const { return seq<N,T>(seq<N,int>(*this/r)*r); }
	seq<N,T> Roll(int left) const;
	size_t Size() const { return N; }
	unsigned Hash() const;

	// StreamIO
	void Print(std::ostream& os) const;
	void Scan(std::istream& is);

	// Explicit Casts
	static seq<N,T> FromArray  (const T* init, size_t n=N);
	static seq<N,T> FromVector (const std::vector<T>& init);
	static seq<N,T> FromString (const std::string& s);

	seq<N,T>& LoadArray  (const T* init, size_t n=N);
	seq<N,T>& LoadVector (const std::vector<T>& v);
	seq<N,T>& LoadString (const std::string& s);

	const T*       ToArray  () const { return data; }
	std::vector<T> ToVector () const;
	std::string    ToString () const;
};

//////////////////////////////////////////////////////////////////////
// Implementation

///////////////////////////////////
// Constructor
SEQNT seq<N,T>::seq(const T& a, const T& b, const T& c, const T& d) {
	T tmp[]={a,b,c,d};
	this->LoadArray(tmp, lengthof(tmp));
}

///////////////////////////////////
// Casting/Assignment Methods
SEQNT SEQMU seq<N,T>::operator seq<M,U> () const {
	seq<M,U> result;
	loop(i,MIN(N,M))
		result[i]=U(data[i]);
	return result;
}

SEQNT SEQMU seq<N,T>& seq<N,T>::operator= (const seq<M,U>& other){
	if((void*)this==(void*)&other)
		return *this;
	loop(i,N)
		data[i]=T( i < int(M) ? other.data[i] : 0 );
	return *this;
}

///////////////////////////////////
// Geometric Operations
SEQNT T seq<N,T>::DotProduct(const seq<N,T>&a, const seq<N,T>& b){
	T result=0;
	loop(i,N)
		result+=a[i]*b[i];
	return result;
}

SEQNT seq<N,T> seq<N,T>::CrossProduct(const seq<N,T>& a, const seq<N,T>& b){
	seq<N,T> p;
	loop(i,N)
		p[i]=a[(i+1)%N]*b[(i+2)%N] - b[(i+1)%N]*a[(i+2)%N];
	return p;
}

SEQNT double seq<N,T>::Angle(const seq<N,T>&a, const seq<N,T>& b){
	return acos(DotProduct(a,b)/sqrt(a.MagnitudeSquared()* b.MagnitudeSquared()));
}

SEQNT double seq<N,T>::SignedAngle(const seq<N,T>& a, const seq<N,T>& b, const seq<N,T>& ref){
	seq<N,T> c=CrossProduct(a,b)/sqrt(a.MagnitudeSquared()*b.MagnitudeSquared());
	return (( (c-ref).MagnitudeSquared() < (c+ref).MagnitudeSquared() ) ? 1 : -1)*asin(c.Magnitude());
}

SEQNT seq<N,T> seq<N,T>::Projection(const seq<N,T>& of, const seq<N,T>& into){
	return (seq<N,T>::DotProduct(of,into)/into.MagnitudeSquared())*into;
}

///////////////////////////////////
// Misc.
SEQNT unsigned seq<N,T>::Hash() const {
	const unsigned char* str=(const unsigned char*)data;
	unsigned hash=1315423911; // Magic number
	loop(i,sizeof(data)){
		hash^=((hash<<5) + (*str) + (hash>>2));
		str++;
	}
	hash^=hash << 16 | hash >> 16;
	return hash;
}

SEQNT seq<N,T> seq<N,T>::Roll(int left) const {
	while(left<0)
		left+=N;
	seq<N,T> o;
	loop(i,N)
		o.data[i]=data[(i+left)%N];
	return o;
}

///////////////////////////////////
// Stream IO
template <size_t N, typename T>
std::ostream& operator<< (std::ostream& os, const seq<N,T>& v){ v.Print(os); return os; }
template <size_t N, typename T>
std::istream& operator>> (std::istream& is, seq<N,T>& v){ v.Scan(is); return is; }

template <size_t N, typename T>
void seq<N,T>::Print(std::ostream& os) const {
	loop(i,N){
		os << data[i];
		if(i!=N-1)
			os << " ";
	}
}

template <size_t N, typename T>
void seq<N,T>::Scan(std::istream& is){
	loop(i,N)
		is >> data[i];
}

///////////////////////////////////
// Explicit Casting
SEQNT seq<N,T> seq<N,T>::FromArray (const T* init, size_t n){
	seq<N,T> v;
	v.LoadArray(init, n);
	return v;
}

SEQNT seq<N,T> seq<N,T>::FromVector(const std::vector<T>& init){
	seq<N,T> v;
	v.LoadVector(init);
	return v;
}

SEQNT seq<N,T> seq<N,T>::FromString(const std::string& s){
	seq<N,T> v;
	v.LoadString(s);
	return v;
}

SEQNT seq<N,T>& seq<N,T>::LoadVector(const std::vector<T>& v){
	return LoadArray(&v[0], v.size());
}

SEQNT seq<N,T>& seq<N,T>::LoadArray(const T* init, size_t n){
	loop(i, N)
		data[i]=(unsigned(i) < n) ? T(init[i]) : T(0);
	return *this;
}

SEQNT seq<N,T>& seq<N,T>::LoadString (const std::string& s){
	std::stringstream ss;
	ss << s;
	ss >> *this;
	return *this;
}

SEQNT std::vector<T> seq<N,T>::ToVector() const {
	return std::vector<T>(&data[0], &data[N]);
}

SEQNT std::string seq<N,T>::ToString() const { 
	std::stringstream ss;
	ss << *this;
	return ss.str();
}


//////////////////////////////////////////////////////////////////////
// Useful typedefs and constants

#include "types.h"

#define SEQ_V_DEF(T,t) \
	typedef seq<1,T> v1##t; \
	typedef seq<2,T> v2##t; \
	typedef seq<3,T> v3##t; \
	typedef seq<4,T> v4##t;

SEQ_V_DEF(u8,     b);
SEQ_V_DEF(int,    i);
SEQ_V_DEF(float,  f);
SEQ_V_DEF(double, d);
SEQ_V_DEF(real,   r);

typedef v4f color;

#define AXIS_X v3r(1,0,0)
#define AXIS_Y v3r(0,1,0)
#define AXIS_Z v3r(0,0,1)

/*
namespace axis{
	const v3r x(1,0,0);
	const v3r y(0,1,0);
	const v3r z(0,0,1);
}
*/
namespace Crayon{
	inline color Opaque(real r, real g, real b){ return color(r,g,b,1); }
	inline color Opaque(color c) { c[3]=1; return c; }

	inline u32 ColorToInt(color c){
		u32 z=0;
		loop(i,4){
			z<<=8;
			z|=int(CLAMP(c[3-i], 0, 1) * 0xFF);
		}
		return z;
	}


	const color Black   = Opaque(0.0, 0.0, 0.0);
	const color White   = Opaque(1.0, 1.0, 1.0);

	const color Red     = Opaque(1.0, 0.0, 0.0);
	const color Green   = Opaque(0.0, 1.0, 0.0);
	const color Blue    = Opaque(0.0, 0.0, 1.0);

	const color Magenta = LERP(Red,     Blue,   0.5);
	const color Yellow  = LERP(Red,     Green,  0.5);
	const color Cyan    = LERP(Blue,    Green,  0.5); 
	const color Purple  = LERP(Magenta, Black,  0.5);
	const color Orange  = LERP(Red,     Yellow, 0.5);
};

#endif
