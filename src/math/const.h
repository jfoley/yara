
#ifndef __MATHCONST_H
#define __MATHCONST_H

#include <cmath>
#include "types.h"

namespace math {
	const real CAN_BE_UNUSED NINF=log(0.0);
	const real CAN_BE_UNUSED INF=-NINF;
	const real CAN_BE_UNUSED NaN=log(-1.0);
	const real CAN_BE_UNUSED E=exp(1.0);
	const real CAN_BE_UNUSED PI=atan(1.0)*4;
	const real CAN_BE_UNUSED DPI=2.0*PI;
	const real CAN_BE_UNUSED HPI=PI/2.0;
}

using math::PI;

#endif
