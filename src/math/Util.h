#ifndef __MATH_UTIL_H
#define __MATH_UTIL_H

#include "seq.h"

i32 Wrap(i32 x, i32 value);
real Wrap(real x, real value);
v2r Wrap(const v2r &x, real value);

#endif

