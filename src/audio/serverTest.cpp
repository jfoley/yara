#include "SoundServer.h"
#include "types.h"
#include "util/strings.h"
#include <iostream>
#include "concur/Chrono.h"
#include "concur/RepeaterThread.h"

string commandPrompt(string prompt) {
	using namespace util;

	//--- show prompt
	cout << prompt;
	cout.flush();

	//--- get line and trim
	string line;
	getline(cin, line);
	line = toLower(compactSpaces(line));

	//--- handle errors TODO DRY
	if(!cin.good())
		return "quit";

	return line;
}

int main(int argc, char **argv) {

	SoundServer audio;

	while(1) {
		string cmd = commandPrompt("sound> ");
		
		if(cmd == "quit") {
			break;
		} else if(cmd == "explode") {
			audio.PlaySound("res/sfx/explode.wav");
		} else if(cmd == "waves") {
			audio.PlaySound("res/sfx/waves.mp3");
		} else if(cmd == "count") {
			cout << "Sounds Running: " << audio.NumSounds() << "\n";
		} else {
			show(cmd);
		}

	}

	return 0;
}

