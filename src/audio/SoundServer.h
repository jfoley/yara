#ifndef __SOUNDSERVER_H
#define __SOUNDSERVER_H

#include "Track.h"
#include "concur/Lockable.h"
#include "concur/RepeaterThread.h"
#include <map>

/// A Thread-safe Sound Server Implementation
class SoundServer : public RepeaterThread {
	public:
		SoundServer();
		virtual ~SoundServer();

		virtual void Rep() {
			CollectFinished();
		}
		int PlaySound(string path);

		void SetSoundPaused(int id, bool value);
		void KillSound(int id);

		void CollectFinished();
		
		bool HasSound(int id);

		u32 NumSounds() const {
			ScopeLock lock(mutex);
			return sfx.size();
		}

		Lockable mutex;
	private:
		int GetNextId();


		int nextID;
		AudioEngine audioEngine;
		Track* music;
		std::map<int, Track*> sfx;
};



#endif

