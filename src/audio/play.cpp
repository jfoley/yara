
#include "SoundServer.h"
#include "types.h"
#include "util/Arguments.h"
#include <iostream>
#include "concur/Chrono.h"

const u32 BufferSize = 4096;
const string PARAM_FILE = "mediaFile";

int main(int argc, char **argv) {
	ArgumentParser arg_parse;
	arg_parse.DefineParameter(PARAM_FILE, "The file to play.");
	//arg_parse.DefineFlag("silent", "s", "Don't play any sound, just ensure that VLC can load it.\n");

	Option<Arguments> result = arg_parse.Parse(argc, argv);
	if(!result.Valid()) {
		printf("\n");
		arg_parse.PrintUsage(std::cout, argv[0]);
		return -1;
	}
	Arguments args(result);

	const string inFileStr = args.GetParameter(PARAM_FILE);
	//const bool silent = args.GetFlag("silent");
	//bool silent = false;
	
	SoundServer soundServer;

	int this_sound = soundServer.PlaySound(inFileStr);

	while(soundServer.HasSound(this_sound)) {
		ForceYield();
	}

	return 0;
}

