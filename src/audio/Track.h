#ifndef __TRACK_H
#define __TRACK_H

#include "Audio.h"

class Track {
	public:
		Track(const AudioEngine&, string path);
		~Track();

		/// VLC will fail under some circumstances
		bool Valid() const;

		void Start();
		void Stop();
		bool IsPlaying() const;
		bool IsDone() const;

		void Wait() {
			if(IsPlaying()) {
				while(!IsDone())
					;
			}
		}

		void SetPaused(bool);

	private:
		bool paused;
		libvlc_media_t *media;
		libvlc_media_player_t *player;
};


#endif

