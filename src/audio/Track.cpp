#include "Track.h"

Track::Track(const AudioEngine &engine, string path) : paused(false), media(0), player(0) {

	if(!engine.Valid())
		return;

	media = libvlc_media_new_path(engine.Handle(), path.c_str());
	if(!media)
		return;
	
	player = libvlc_media_player_new_from_media(media);
	if(!player)
		return;
}

Track::~Track() {
	if(IsPlaying()) {
		Stop();
	}
	if(player) {
		libvlc_media_player_release(player);
	}
	if(media) {
		libvlc_media_release(media);
	}

}

void Track::Start() {
	libvlc_media_player_play(player);
	//while(!IsPlaying())
		//;
}

void Track::Stop() {
	if(player) {
		libvlc_media_player_stop(player);
	}
}

bool Track::IsPlaying() const {
	if(!Valid())
		return false;
	return !paused && libvlc_media_player_is_playing(player);
}

bool Track::IsDone() const {
	if(!Valid())
		return true;
	return !paused && !libvlc_media_player_is_playing(player);
}

void Track::SetPaused(bool value) {
	if(Valid()) {
		paused = value;
#if WORKS_AT_HOME
		libvlc_media_player_set_pause(player, paused);
#elif USE_MUTE_INSTEAD
		libvlc_audio_set_mute(player, paused);
#else //ON OFF DERP
		if(!paused) Start();
		else Stop();
#endif
	} else {
		show(Valid());
	}
}

bool Track::Valid() const {
	return media && player;
}

