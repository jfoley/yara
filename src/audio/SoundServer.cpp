#include "SoundServer.h"
#include <algorithm>
using std::map;

typedef map<int, Track*> TrackPool;

namespace {
	void deleteTrack(const std::pair<int, Track*> &in) {
		delete in.second;
	}
};

SoundServer::SoundServer() {
	nextID = 0;

	if(!audioEngine.Valid()) {
		cout << "Sound didn't init correctly.\n";
		return;
	}

	SetInterval(1);
	Start();
}

SoundServer::~SoundServer() {
	ScopeLock lock(mutex);
	
	//--- delete all
	std::for_each(sfx.begin(), sfx.end(), deleteTrack);
	sfx.clear();
}

int SoundServer::PlaySound(string path) {
	if(path == "")
		return -1;

	if(!audioEngine.Valid())
		return -1;

	//--- start track and add it
	int id = GetNextId();
	Track *snd = new Track(audioEngine, path);
	snd->Start();
	sfx[id] = snd;
	return id;
}

void SoundServer::KillSound(int id) {
	if(sfx.find(id) != sfx.end()) {
		delete sfx[id];
		sfx.erase(id);
	}
}

bool SoundServer::HasSound(int id) {
	return sfx.find(id) != sfx.end();
}

//--- don't ever return an id in use or a negative id
int SoundServer::GetNextId() {
	while(sfx.find(nextID) != sfx.end()) {
		nextID++;
		if(nextID < 0) nextID = 0;
	}
	return nextID;
}

void SoundServer::CollectFinished() {
	ScopeLock lock(mutex);

	//--- better, in my opinion than a while loop on invalid iterators
	vector<int> toBeRemoved;

	foreach(TrackPool::const_iterator, iter, sfx) {
		int id = iter->first;
		Track *track = iter->second;

		if(track->IsDone()) {
			toBeRemoved.push_back(id);
			delete track;
		}
	}

	uloop(i, toBeRemoved.size()) {
		sfx.erase(toBeRemoved[i]);
	}
}

