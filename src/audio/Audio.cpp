#include "Audio.h"

AudioEngine::AudioEngine() {
	char const *argv[] = {
		"--no-xlib", // don't use X windowing anything, causes crashes with SDL_Quit
	};
	const int argc = lengthof(argv);

	data = 0;
	data = libvlc_new(argc,argv);
}

AudioEngine::~AudioEngine() {
	if(data) {
		libvlc_release(data);
	}
}

