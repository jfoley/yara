#ifndef __AUDIO_H
#define __AUDIO_H

#include <vlc/vlc.h>
#include "types.h"
#include "Assert.h"

class AudioEngine {
	public:
		AudioEngine();
		~AudioEngine();
		bool Valid() const { return data; }
		libvlc_instance_t* Handle() const { return data; }
	private:
		libvlc_instance_t *data;
};


#endif

