#ifndef __ASSERT_H
#define __ASSERT_H

#include "macros.h"
#include <cstdio>
#include <exception>

//--- this is an attempt to catch modules using the old assert
#if defined(assert)
#undef assert
#endif

//--- rather than having a static function that fails on assert or whatever, just use unhandled exception behavior
class Assertion : public std::exception {
	public:
		Assertion(const char * const c, const char * const f, const int l) : condition(c), file(f), line(l) { }
		const char* what() const throw() {
			static char buffer[1024];
			snprintf(buffer, 1024, START_RED "*** %s:%d Assert(%s) failed.\n" END_COLOR, file, line, condition);
			return buffer;
		}
	private:
		const char* condition;
		const char* file;
		int line;
};

#define Assert(condition) \
	do { \
		if(!(condition)) throw Assertion(#condition, __FILE__, __LINE__); \
	} while(0)


#endif

