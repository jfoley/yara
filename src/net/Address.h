#ifndef __ADDRESS_H
#define __ADDRESS_H

#include "types.h"
#include "Option.h"
#include "util/HumanReadable.h"
#include "util/parse.h"


const u16 AnyPort = 0;

class Address : public Printable {
	public:
		static Option<Address> Resolve(const string &host, u16 port=AnyPort);
		static Option<Address> FromString(const string &input);

		/// default to localhost
		Address() : a(127), b(0), c(0), d(1), port(AnyPort) { }
		/// from raw number data
		Address(u8 a, u8 b, u8 c, u8 d, u16 port=AnyPort);
		/// from binary blob
		Address(void* data, size_t len);
		/// Write to binary blob
		void Write(void *dest, size_t len) const;
		/// For sanity checking
		virtual void Print(std::ostream &os) const;
		/// Also for sanity checking
		bool operator==(const Address &rhs) const;
	private:
		u8 a, b, c, d;
		u16 port;
};

#endif

