#include "Address.h"
#include "Assert.h"

#include "util/hexdump.h"

#include "util/strings.h"
#include <cstring> // for memset

//--- network specific
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>

Address::Address(u8 x, u8 y, u8 z, u8 w, u16 p)
	: a(x), b(y), c(z), d(w), port(p) { }

Address::Address(void *blob, size_t len) {
	Assert(len >= sizeof(sockaddr_in));

	//util::hexdump(std::cout, blob, len);

	//--- sockaddr_t is a union that's not specified as a union; it's two alternatives are sockaddr_in and sockaddr_in6
	const sockaddr_in* addr = (const sockaddr_in*) blob;

	if(addr->sin_family) {
		//--- this assert will go off if I need to support IPv6
		//show(addr->sin_family);
		Assert(addr->sin_family != AF_INET6);
		Assert(addr->sin_family == AF_INET);
	}

	//--- deconstruct word
	const u32 ip_word = ntohl(addr->sin_addr.s_addr);
	a = (ip_word >> 24) & 0xff;
	b = (ip_word >> 16) & 0xff;
	c = (ip_word >> 8)  & 0xff;
	d = (ip_word)       & 0xff;

	port = ntohs(addr->sin_port);
}

void Address::Write(void *dest, size_t len) const {
	Assert(len >= sizeof(sockaddr_in));

	//--- sockaddr_t is a union that's not specified as a union; it's two alternatives are sockaddr_in and sockaddr_in6
	sockaddr_in* addr = (sockaddr_in*) dest;

	//--- construct word
	const u32 ip_word = (a << 24 | b << 16 | c << 8 | d);

	//--- fill in our data
	addr->sin_family = AF_INET; //--- IPv4
	addr->sin_addr.s_addr = htonl(ip_word);
	addr->sin_port = htons(port);
}

void Address::Print(std::ostream &os) const {
	os << util::stringf("%d.%d.%d.%d", a, b, c, d);
	if(port != AnyPort) os << ":" << port;
}

/// a.b.c.d[:port]
Option<Address> Address::FromString(const string &input) {
	using namespace util;

	u16 port = 0;
	string host(input);

	if(contains(input, ':')) {
		port = parse<u16>(after(input, ':')).GetOrElse(0);
		host = before(input, ':');
	}
	show(host);
	show(port);

	return None;
}

Option<Address> Address::Resolve(const string& host, u16 port) {
	
	addrinfo *results = 0;

	//--- gen request parameters
	{
		const string service = util::stringf("%d", port);
		const char* service_ptr = (port == 0) ? 0 : service.c_str();
		const char* host_ptr = (host == "localhost" || host == "") ? 0 : host.c_str();

		addrinfo hints;
		memset(&hints, 0, sizeof(hints));
		hints.ai_family = AF_INET; //--- prefer only IPv4 addresses
		//--- TODO revisit this being conditional
		if(host_ptr == 0) hints.ai_flags = AI_PASSIVE;

		int status = getaddrinfo(host_ptr, service_ptr, &hints, &results);

		if(status != 0) {
			std::cout << "getaddrinfo: " << gai_strerror(status) << "\n";
			results = 0;
			return None;
		}
	}

	Address result;
	bool found = false;
	//--- process results
	for(addrinfo *cur = results; cur != 0; cur = cur->ai_next ) {
		if(cur->ai_family != AF_INET) continue;
		
		found = true;
		result = Address(cur->ai_addr, cur->ai_addrlen);
		break;
	}


	//--- free linked list from getaddrinfo
	freeaddrinfo(results);
	if(found) return result;

	return None;
}

bool Address::operator==(const Address &rhs) const {
	return port == rhs.port
			&& a    == rhs.a
			&& b    == rhs.b
			&& c    == rhs.c
			&& d    == rhs.d;
}

