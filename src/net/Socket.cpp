#include "Socket.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>

#include "util/hexdump.h"
#include <cstring> // memset


Socket::Socket() : handle(-1) { }
bool Socket::Valid() const { return handle != -1; }
void Socket::Close() { 
	if(Valid()) {
		close(handle); 
		handle = -1;
	}
}

bool Socket::Open(u16 port) {
	handle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if( !Valid() ) {
		perror("Socket::Open::socket");
		return false;
	}

	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons(port);

	if ( bind(handle, (const sockaddr*) &address, sizeof(address)) == -1 ) {
		perror("Socket::Open::bind");
		return false;
	}

	const int Yes = 1;
	if (fcntl ( handle, F_SETFL, O_NONBLOCK, &Yes)  == -1 ) {
		perror("Socket::Open::fcntl");
		return false;
	}

	return true;
}

bool Socket::Send(const Address &dest, const void *msg, size_t len) {
	//show(dest);
	//--- get a native address struct from dest
	sockaddr_storage raw_addr;
	dest.Write(&raw_addr, sizeof(raw_addr));

	//--- get bytes sent
	ssize_t sent_len = sendto(handle, msg, len, 0, (const sockaddr*) &raw_addr, sizeof(raw_addr));

	//--- return if error code or wrong amount sent
	if(sent_len == -1 || size_t(sent_len) != len) {
		perror("Socket::Send");
		return false;
	}

	return true;
}

Option<size_t> Socket::Receive(Address *src, void *buf, size_t len) {
	//--- get a native address struct for building src
	sockaddr_storage raw_addr;
	memset(&raw_addr, 0, sizeof(raw_addr));
	socklen_t addr_size = 0;
	
	const ssize_t read_len = recvfrom(handle, buf, len, 0, (sockaddr*) &raw_addr, &addr_size);
	if(read_len <= 0) return None;

	//show(read_len);
	//show(addr_size);

	Assert(addr_size != 0);
	//util::hexdump(std::cout, &raw_addr, addr_size);
	//--- return source address
	*src = Address(&raw_addr, addr_size);

	return size_t(read_len);

}

