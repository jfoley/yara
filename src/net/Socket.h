#ifndef __SOCKET_H
#define __SOCKET_H

#include "Address.h"

class Socket {
	public:
		/// Default Socket is invalid
		Socket();
		/// Close socket on cleanup
		~Socket() { Close(); }

		/// This is where initialization happens; no need to specify a port for outgoing sockets
		bool Open(u16 port=AnyPort);
		/// Echoes the return value of Open; whether current socket is open or not
		bool Valid() const;
		/// This is used to clean up the socket; called automatically in destructor
		void Close();

		/// Sends msg of len to dest
		bool Send(const Address &dest, const void *msg, size_t len);
		/// Returns valid len if there is a message from src which has been placed into buffer of len at msg
		Option<size_t> Receive(Address *src, void *buf, size_t len);

		/// Disallow copying of sockets
		Socket(const Socket &rhs)            { Assert(0); }
		/// Disallow assignment of sockets
		Socket& operator=(const Socket &rhs) { Assert(0); }
	
	private:
		int handle;
};



#endif

