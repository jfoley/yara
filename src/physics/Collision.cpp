
#include "Collision.h"
v2r QuadraticFormula(real A, real B, real C){
	v2r result(-B,-B);
	real s=B*B - 4*A*C;
	if(s<0)
		return v2r(math::NaN, math::NaN);
	result+=v2r(-1,1)*sqrt(s);
	result/=2*A;
	return result;
}

bool HandleCollision(World& world, GameObject& obj, real dt){
	v2r p=v2r(obj.pos);
	v3r w=world.PointOnTerrain(p);

	if(obj.pos[2]-obj.radius <= w[2]){
	v3r n=world.GetNormal(p);
		n.Normalize();
		if(v3r::DotProduct(n, obj.vel) < 0){
			v3r proj=v3r::Projection(obj.vel, n);
			obj.vel-=(1+obj.stiffness)*proj;
		}
		obj.pos+=n*(obj.radius-v3r::DotProduct(n, obj.pos-w));
		
		obj.onGround=true;
	}
	else
		obj.onGround=false;

	obj.pos[2]=MAX(obj.pos[2],w[2]+obj.radius);
	return obj.onGround;
}


bool HandleCollision(GameObject& A, GameObject& B, real dt){

	// Don't try to collide something with itself
	if(&A==&B)
		return false;

	v3r BAx=B.pos-A.pos;
	v3r BAv=B.vel-A.vel;
	real radius=A.radius+B.radius;
	real mass=A.mass+B.mass;

	v2r collisionTime=QuadraticFormula(
			BAv.MagnitudeSquared(),
			2*v3r::DotProduct(BAx,BAv),
			BAx.MagnitudeSquared() - radius*radius
			);

	// Make sure collision times are real numbers
	if(collisionTime[0]!=collisionTime[0])
		return false;

	const real stage=CLAMP(collisionTime[0], 0, collisionTime[1]);
	// stage = 0, we are in the middle of a collision
	// stage < 0, a collision happened in the past (extrapolated)
	// stage > 0, a collision will happen in "stage" seconds

	if(stage<0){
		// No future collision
		return false;
	}
	else if(stage>0){
	// Future collision
		if(stage>=dt){
			// Too far in the future to bother
			return false;
		}
		else{
			// Close enough to handle now
			// Move objects together
			A.pos+=A.vel*collisionTime[0];
			B.pos+=B.vel*collisionTime[0];
		}
	}
	else if(stage==0){
		// Currently colliding
		real c=(radius) / ( BAx.Magnitude() ) - 1;
		// c = 0: Kissing
		// c < 0: No collision
		// c > 0: Overlap

		// Place spheres outside of each other
		v3r com=(A.mass*A.pos + B.mass*B.pos)/mass;
		A.pos+=(A.pos-com)*c;
		B.pos+=(B.pos-com)*c;
		BAx*=c;
	}

	// http://www.gamasutra.com/view/feature/131424/pool_hall_lessons_fast_accurate_.php?page=3

	// Update velocities
	v3r p=v3r::Projection(BAv, BAx) * 2/mass;
	real stiffness=A.stiffness*B.stiffness;

#if 1
	v3r comv=(A.vel*A.mass + B.vel*B.mass)/(mass);
	A.vel = LERP(comv, A.vel + (p*B.mass), stiffness);
	B.vel = LERP(comv, B.vel - (p*A.mass), stiffness);
#else
	p*=stiffness;
	A.vel+=B.mass*p;
	B.vel-=A.mass*p;
#endif

	return true;
}

