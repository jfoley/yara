#ifndef __COLLISION_H
#define __COLLISION_H

#include "gob/GameObject.h"
#include "platform/Capstone/World.h"

bool HandleCollision(World& world, GameObject& obj, real dt);
bool HandleCollision(GameObject& A, GameObject& B, real dt=0);
#endif
