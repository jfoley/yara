
#ifndef __PHYSICSLOOP_H
#define __PHYSICSLOOP_H

#include "concur/concur.h"
#include "types.h"

class PhysicsLoop: public Thread {
	public:

		struct Physlet{
			virtual bool operator() (real dt) = 0;
		};

		struct CPhyslet: public Physlet {
			CPhyslet(bool (*_func)(real, void*), void* _data=0){
				func=_func;
				data=_data;
			}
			virtual bool operator() (real dt){
				return func(dt, data);
			}
	
			bool (*func)(real dt, void* user);
			void* data;
		};



	public:
		PhysicsLoop(){ done=false; }
		void Stop(){ done=true; Nap(0); }
		void StopAndJoin(){ Stop(); Join(); }

		bool AddPhyslet(Physlet* physlet){
			physlets.push_back(physlet);
			return true;
		}

		bool AddPhyslet(bool (*func)(real,void*), void* data=0){
			CPhyslet* p=new CPhyslet(func, data);
			AddPhyslet(p);
			return true;
		}

	private:
		virtual void Run(){
			TimeValue lastTime;
			while(!done){
				TimeValue now;
				TimeValue dt=now-lastTime;

				loop(i, physlets.size()){
					if(!(*(physlets[i]))(dt)){
						physlets.erase(physlets.begin()+i);
						i--;
					}
				}

				lastTime=now;
				Nap(0); // Yield
			}
		}

		vector<Physlet*> physlets;
		volatile bool done;

};
#endif
