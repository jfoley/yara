
#ifndef __LINKAGE_H
#define __LINKAGE_H

#include "gob/GameObject.h"
#include "gob/Rod.h"

struct Linkage{
	public:
	GameObject* gob[2];
	virtual void Apply(){}
	virtual void Draw(GameObject& cam){
		DrawRod(gob[0]->pos, gob[1]->pos, .2);
	}
	virtual ~Linkage(){}
};



#endif
