#include "SpringLinkage.h"

void SpringLinkage::Apply(){
	v3r r=gob[1]->pos-gob[0]->pos;

	real x=r.Magnitude();
	r.Normalize();
	real v=v3r::DotProduct(gob[1]->vel - gob[0]->vel, r);
	real a=v3r::DotProduct(gob[1]->acc - gob[0]->acc, r);

	real f=(x-x0)*k + (v-v0)*c + (a-a0)*m;

	gob[0]->acc+=r*(f/gob[0]->mass);
	gob[1]->acc-=r*(f/gob[1]->mass);
}

