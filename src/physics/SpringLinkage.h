
#ifndef __SPRINGLINKAGE_H
#define __SPRINGLINKAGE_H

#include "Linkage.h"

class SpringLinkage: public Linkage{
	public:

		SpringLinkage(){
			m=0;
			c=0;
			k=0;
			x0=0;
			v0=0;
			a0=0;
		}

	real m;
	real c;
	real k;

	real x0;
	real v0;
	real a0;

	virtual void Apply();
	virtual ~SpringLinkage(){}
};

#endif
