// types.h

#ifndef __TYPES_H
#define __TYPES_H

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
using std::vector;
using std::string;
using std::stringstream;

namespace std {
	struct nullstream: std::ostream { nullstream(): std::ios(0), std::ostream(0) {} };
	static struct nullstream cnull;
}

using std::cout;
using std::cin;
using std::cerr;
using std::cnull;

typedef      unsigned char u8;
typedef               char i8;
typedef     unsigned short u16;
typedef              short i16;
typedef       unsigned int u32;
typedef                int i32;
typedef          long long i64;
typedef unsigned long long u64;

#define REAL_IS_DOUBLE
#ifdef REAL_IS_DOUBLE
typedef double real;
#else
typedef float  real;
#endif


#endif

