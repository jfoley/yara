
#ifndef __CUBE_H
#define __CUBE_H

#include "platform/GLContext.h"
#include "GameObject.h"
#include "math/seq.h"

class Cube: public GameObject{
	public:
		Cube(real sz=1.0): size(sz) {}
		void Draw(const Camera& cam);
	private:
		real size;
};

#endif
