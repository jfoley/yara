#ifndef __GRID_H
#define __GRID_H

#include "gob/GameObject.h"

class Grid: public GameObject {
	public:
		Grid(real _gridspacing, int _gridlines=11):
			gridspacing(_gridspacing), gridlines(_gridlines) {}

		real gridspacing;
		int gridlines;

		void Draw(const Camera& cam);
};


#endif
