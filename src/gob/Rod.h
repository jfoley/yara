
#ifndef __ROD_H
#define __ROD_H

#include "platform/GLContext.h"

void DrawRod(const v3r& from, const v3r& to, real r, int nSegments=3);

#endif
