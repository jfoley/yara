
#include "GameObject.h"
#include "platform/GLContext.h"
#include <cmath>

void GameObject::DrawSphere() const {
	int SphereDetail=10*log(radius+1);
	GLSaveStack(){
		GLContext::Translate(pos);
		glutSolidSphere(radius, SphereDetail, SphereDetail);
	}
}

void GameObject::Follow(const GameObject& leader, real dist, real rate){
	pos=LERP(pos, leader.pos-leader.dir.GetForward()*dist, rate);
	dir.Set(
				LERP(dir.F(), leader.dir.F(), rate),
				LERP(dir.U(), leader.dir.U(), rate)
			);
}


real GameObject::COMmass() const {
	real total=mass;
	loop(i, children.size())
		total+=children[i]->COMmass();
	return total;
}


#define COMmethod(property) \
	v3r GameObject::COM##property() const { \
		real m=mass; \
		v3r result=mass * property; \
		loop(i, children.size()){ \
			real cm=children[i]->COMmass(); \
			result+=children[i]->COM##property() * cm; \
			m+=cm; \
		} \
		return result/m; \
	}

COMmethod(pos);
COMmethod(vel);
COMmethod(acc);

