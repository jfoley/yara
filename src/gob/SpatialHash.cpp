
#include "SpatialHash.h"
#include "Assert.h"
#include <cmath>

using std::set;
using std::map;
using std::multiset;


template <typename C, typename D>
	void setcpy(C* dest, const D* src){
		if(!src || !dest)
			return;
		foreach(typename D::const_iterator, o, *src)
			dest->insert(*o);
	}

struct zindexer {
	const v3r& camref;
	zindexer(v3r _campos): camref(_campos) {}
	bool operator() (GameObject* a, GameObject* b){
		// Distant objects are "less than" close objects so they can be plotted first
		return (a->pos-camref).MagnitudeSquared() > (b->pos-camref).MagnitudeSquared();
	}
};


void SpatialHash::Draw(const Camera& cam){
	zindexer comp(cam.pos);
	typedef multiset<GameObject*, zindexer> draw_set_t;
	
	draw_set_t drawlist(comp);
	GetDrawList(&drawlist, cam);
	
	foreach(draw_set_t::iterator, i, drawlist)
		(*i)->Draw(cam);
}

bool SpatialHash::Insert(GameObject* g){
	// If the GameObject is already in the set, fail.
	if(go_set.count(g)!=0)
		return false;
	// If not, put it in the set.
	go_set.insert(g);
	// Map the position vector to an integer hash
	hash_t h=Hash(g->pos);
	// If a cell doesn't exist for that particular hash, create it
	if(go_map.count(h)==0)
		go_map[h]=set<GameObject*>();
	// Place the GameObject in a cell
	go_map[h].insert(g);
	return true;
}

bool SpatialHash::Remove(GameObject* g){
	if(go_set.count(g)==0)
		return false;
	go_set.erase(g);
	go_map[Hash(g->pos)].erase(g);
	return true;
}

template <typename C>
void SpatialHash::GetWithinRadius(C* collection, const v3r& center, real radius){
	Assert(collection);
	int N=ceil(radius/cellsize);
	loop3(ii,jj,kk,N,N,N){
		int i=ii-N/2;
		int j=jj-N/2;
		int k=kk-N/2;
		v3r c(center+v3r(i,j,k)*cellsize);
		setcpy(collection, &go_map[Hash(c)]);
	}
}

template <typename C>
void SpatialHash::GetWithinCone(C* collection, const v3r& center, const v3r& direction){
	Assert(collection);
	int N=ceil(farview/cellsize);
	loop3(ii,jj,kk,N,N,N){
		int i=ii-N/2;
		int j=jj-N/2;
		int k=kk-N/2;
		v3r d2(i,j,k);
		v3r c=center+d2*cellsize;
		if(v3r::Angle(d2,direction)>camangle)
			continue;
		setcpy(collection, &go_map[Hash(c)]);
	}
}

void SpatialHash::SetCellSize(real size){
	cellsize=size;
	go_map=std::map< hash_t , std::set<GameObject*> >();
	foreach(std::set<GameObject*>::iterator, i, go_set)
		go_map[Hash((*i)->pos)].insert(*i);
}

SpatialHash::hash_t SpatialHash::Hash(const v3r& v) const {
	hash_t h=0;
	v3r i=v/cellsize;
	loop(j,3){
		h<<=(sizeof(hash_t)*8)/3;
		h+=u32(i[j]);
	}
	return h;
}

