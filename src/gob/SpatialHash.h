
#ifndef __SPATIALHASH_H
#define __SPATIALHASH_H

#include "GameObject.h"
#include "types.h"

#include <set>
#include <map>

class SpatialHash: public GameObject {
	typedef u32 hash_t;
	
	public:
		SpatialHash(real _cellsize, real _camangle, real _farview): 
			cellsize(_cellsize),
			farview(_farview),
			camangle(_camangle) {}
		void Draw(const Camera& cam);
		
		bool Insert(GameObject* g);
		bool Remove(GameObject* g);

		template <typename C>
		void GetWithinRadius(C* collection, const v3r& center, real radius);

		template <typename C>
		void GetWithinCone(C* collection, const v3r& center, const v3r& direction);

		template <typename C>
		void GetDrawList(C* collection, const Camera& cam){ 
			GetWithinCone(collection, cam.pos, cam.dir.GetForward());
		}

		real CellSize() const { return cellsize; }
		real FarField() const { return farview; }
		real CamAngle() const { return camangle; }

		void SetCamAngle(real angle) { camangle=angle; }
		void SetFarField(real dist) { farview=dist; }
		void SetCellSize(real size); 

	private:

		real cellsize;
		real farview;
		real camangle;
	
		std::map< hash_t , std::set<GameObject*> > go_map;
		std::set<GameObject*> go_set;

		hash_t Hash(const v3r& v) const;


};


#endif
