#include "Rod.h"

void DrawRod(const v3r& from, const v3r& to, real r, int nSegments){
	using namespace GLContext;
	real dt=2*PI/nSegments;
	real L=(to-from).Magnitude();
	GLSaveStack(){
		Transform(from, Orientation(to-from, AXIS_Z));
		GLPrimitive(GL_QUADS){
			loop(i, nSegments){
				loop(k, 4){
					real t=(i + ((k == 1 || k == 2) ? 1 : 0 ))*dt;
					real l=(k == 2 || k == 3) ? L : 0 ;
					v3r vtx(
							r*cos(t),
							r*sin(t),
							l
							);
					v3r nml(
							cos(t),
							sin(t),
							0
							);
					Normal(nml);
					Vertex(vtx);
				}
			}
		}
	}
}

