#ifndef __GAMEOBJECT_H
#define __GAMEOBJECT_H

#include "math/seq.h"
#include "math/Orientation.h"
#include "GameObject.h"

struct Linkage;

struct GameObject{
	GameObject() {
		radius = 1;
		mass = 1;
		stiffness = 1;
		onGround = false;
	}
	// Kinematics
	v3r pos;
	v3r vel;
	v3r acc;

	Orientation dir;

	bool onGround;

	// Physical properties for sphere collisions
	real radius;
	real mass;
	real stiffness;

	// Sub-GameObjects for complex configurations of bodies
	vector<GameObject*> children;
	vector<Linkage*> joints;

	// Recursive Methods
	real COMmass() const;
	v3r COMpos() const;
	v3r COMvel() const;
	v3r COMacc() const;

	// Defaults to drawing a representative sphere
	virtual void Draw(const GameObject& cam){ DrawSphere(); }
	void DrawSphere() const;

	// Defaults to incrementing kinematics
	virtual void PhysUpdate(real dt) { vel+=acc*dt; pos+=vel*dt; }

	//
	void Follow(const GameObject& leader, real dist, real rate);

	// Dummy Destructor
	virtual ~GameObject() {}
};

#include "physics/Linkage.h"
#include "gob/Camera.h"
#endif
