
#ifndef __CAMERA_H
#define __CAMERA_H

#include "platform/GLContext.h"
#include "gob/GameObject.h"

class Camera: public GameObject{
	public:

		Camera(v3r _pos=v3r(), v3r _dir=v3r(1)){
			pos=_pos;
			dir.Set(_dir, AXIS_Z);
		}

		Camera(v3r _pos, Orientation _dir){
			pos=_pos;
			dir=_dir;
		}
		

		void Clear() const {
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
		}

		void Look() const {
			v3r d=pos+dir.GetForward();
			v3r up=dir.GetUp();
			gluLookAt(pos[0],pos[1],pos[2],d[0],d[1],d[2],up[0],up[1],up[2]);
		}

		virtual void Draw(const GameObject& cam){
			Clear();
			Look();
		}

		void Draw(){
			Draw(*this);
		}
};

#endif
