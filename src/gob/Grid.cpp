
#include "backend.h"
#include "platform/GLContext.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include "Grid.h"

void Grid::Draw(const Camera& cam){
	glLineWidth(1);
	glBegin(GL_LINES);
	loop2(i,j,gridlines,gridlines){
		int ii=i-gridlines/2;
		int jj=j-gridlines/2;
		v3r a=v3r(ii,jj,-gridlines)*gridspacing;
		v3r b=v3r(ii,jj, gridlines)*gridspacing;
		loop(k,3){
			v3r aa=(cam.pos+a).Round(gridspacing);
			v3r bb=(cam.pos+b).Round(gridspacing);
			real d=gridlines*gridspacing/(aa+bb-2*cam.pos).MagnitudeSquared();
			glColor3f(d,d,d);

			using namespace GLContext;
			
			Vertex(aa);
			Vertex(bb);
			a=a.Roll(1);
			b=b.Roll(1);
		}
	}
	glEnd();
}
