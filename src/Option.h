#ifndef __OPTION_H
#define __OPTION_H

#include "macros.h"
#include "Assert.h"

/// Singleton static instance that allows for "return None;" illusion
class NoneOption { } static CAN_BE_UNUSED None;

template <class T>
class Option {
	public:
		/// This constructor, when used implicitly, allows the "return None;" to create any Option<T>
		Option(NoneOption n): valid(false) { }
		/// This constructor, when used implicitly, allows you to simply return the value from a procedure that returns an option type
		Option(T item): valid(true), value(item) { }

		/// Check whether result of type T is present or not
		bool Valid() const { return valid; }
		/// Unsafe get unless you checked for valid already
		T    Get()   const { Assert(valid); return value; }
		operator T() const { return Get(); }
		/// One liner to replace with default
		T    GetOrElse(T backup) const { return valid ? value : backup; }
	private:
		bool valid;
		T    value;
};

#endif

