#ifndef _PATH_H
#define _PATH_H

#include "types.h"

class Path {
	public:
		Path(string path=".");
		
		void Set(string path);
		/// calculate the file's information again
		void Refresh();
		
		/// returns true if it is a directory and it is empty
		bool Empty() const;
		/// returns true if the path exists
		bool Exists() const { return exists; }
		/// returns true if the path resolves to a directory
		bool Directory() const { return directory; }
		/// returns true if the path resolves to a file
		bool File() const { return file; }
		/// returns the size in bytes of the file
		u32  Size() const { return size; }
		/// returns true if the name begins with "."
		bool Hidden() const;

		/// returns the relative name of the file or directory
		string Name() const;


		/// returns true if the given path is a subdirectory of this path
		bool Contains(const string &other) const;
		
		/// returns a new path going up a directory
		Path UpDir() const;
		/// Gets a Dir from a file
		Path GetDir() const;
		
		/// returns a new path by changing the relative path or to the absolute path
		Path ChDir(string rel) const;
		Path operator+(string rel) const { return ChDir(rel); }
		Path& operator+=(string rel) { *this = ChDir(rel); return *this; }
		
		/// casting to string for fun and profit
		operator string() const { return path; }
		const char* c_str() const { return path.c_str(); }
		friend std::ostream& operator<<(std::ostream& out, const Path &p) { return out << string(p); }

		/// if this is a directory, return a list of it's contents
		vector<Path> List() const;

	protected:
		string path;
		
		bool exists;
		bool directory;
		bool file;
		u32  size;
};



#endif

