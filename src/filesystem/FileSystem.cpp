#include "FileSystem.h"
#include "Path.h"
#include "Assert.h"

#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
using std::ios;
using std::fstream;

#include "PathSep.h"

#include "util/strings.h"
using namespace util;

const u32 CopyFileBufferSize = 4096;

namespace {
	//--- this is private because anyone else can get it by using "." or ""
	string currentDirectory() {
		string result;
		result.resize(1024);

		if(getcwd(&result[0], result.size()) == 0)
			return "";

		return string(result.c_str());
	}
}

string Which(string name) {
	string path = getenv("PATH");
	// string is PATH=a:b:c:d:...
	path = after(path, '=');

	vector<string> options = split(path, ':');

	for(size_t i=0; i<options.size(); i++) {
		Path current(options[i]);

		//--- find name in files
		vector<Path> files = current.List();
		for(size_t j=0; j<files.size(); j++) {
			if(files[j].Name() == name) {
				return files[j];
			}
		}
	}

	return "";
}

bool MakeDir(string path) {
	Path p(path);

	//--- return success if directory already exists
	if(p.Exists())
		return p.Directory();

	if(mkdir(p.c_str(), 0777) == -1) {
		//perror("mkdir");
		return false;
	}

	return true;
}

bool Remove(string path) {
	Path p(path);

	if(remove(p.c_str()) == -1) {
		//perror("rmdir");
		return false;
	}

	return true;
}

bool RemoveAll(string path) {
	const Path p(path);

	if(!p.Exists() || !p.Directory())
		return false;

	vector<Path> contents = p.List();
	for(size_t i=0; i<contents.size(); i++) {
		const Path &cur = contents[i];

		if(cur.Directory() && !cur.Empty()) {
			//--- recursively remove contents
			if(!RemoveAll(cur))
				return false;
		} else {
			//--- remove this file
			if(!Remove(cur))
				return false;
		}
	}
	
	//--- remove directory itself
	if(!Remove(p))
		return false;

	return true;
}

bool MakeDirR(string path) {
	vector<string> pieces = split(path, PathSepChar);

	// iterate through building the path for /a/b/c/d
	// /a, /a/b, /a/b/c ...
	for(size_t i=0; i<pieces.size(); i++) {
		//--- join path so far
		string cur;
		for(size_t j=0; j<=i; j++) {
			if(j != 0) cur += '/';
			cur += pieces[j];
		}

		//--- attempt to create if it doesn't exist
		Path dir(cur);
		if(dir.Exists()) {
			if(!dir.Directory())
				return false;
			continue;
		}
		
		//--- create if doesn't exist
		if(!MakeDir(dir)) return false;
	}
	return true;
}

string SimplifyPath(string path) {
	string result;

	result += PathSepChar;

	//--- if not absolute, make it so
	if(!startsWith(path, PathSepStr)) {
		path = currentDirectory() + PathSepChar + path;
	}

	vector<string> input = split(path, PathSepChar);
	vector<string> output;
	output.reserve(input.size());

	for(size_t i=0; i<input.size(); i++) {
		const string &cur = input[i];
		
		//--- don't preserve needless dots
		if(cur == "." || cur == "")
			continue;

		if(cur == "..") {
			if(output.size())
				output.pop_back();
			continue;
		}

		output.push_back(cur);
	}
	
	result += join(output, PathSepChar);

	return string(result);
}

string UniqueSubDir(string path, string prefix) {
	Assert(Path(path).Directory());
	string base = path + PathSepChar + prefix;

	if(!Path(base).Exists())
		return base;
	
	uloop(i, 0xffffffff) {
		string addition = stringf("%0d", i);
		if(!Path(base + addition).Exists())
			return base+addition;
	}
	Assert(0);
	return "";
}

bool CopyFile(string src, string dest) {
	Assert(Path(src).Exists());

	//--- handle destination as directory
	if(Path(dest).Directory()) {
		dest = Path(dest) + Path(src).Name();
	}

	cout << "Copy " << src << " to " << dest << "\n";
	

	fstream in;
	fstream out;

	in.open(src.c_str(), (fstream::in | fstream::binary));
	out.open(dest.c_str(), (fstream::out | fstream::binary));

	/*
	in.seekg(0, ios::end);
	u32 length = in.tellg();
	in.seekg(0, ios::beg);
	*/

	if(in.bad() || out.bad())
		return false;

	while(!in.eof() && out.good()) {
		char buffer[CopyFileBufferSize];
		in.read(buffer, sizeof(buffer));
		out.write(buffer, in.gcount());
	}

	return true;
}

bool MoveFile(string src, string dest) {
	Path in(src);
	Path out(dest);

	show(in);
	show(out);

	Assert(in.Exists());
	//--- make sure the target either:
	//    a) doesn't exist
	//    b) isn't a directory
	//    c) is an empty directory
	Assert(!out.Exists() || !out.Directory() || !out.Empty());

	int rc = rename(in.c_str(), out.c_str());
	return rc == 0;
}

void ChangeDir(string src) {
	Path target(src);
	if(!target.Exists()) {
		cout << "\"" << src << "\" does not exist\n";
		return;
	}
	if(!target.Directory()) {
		cout << "\"" << src << "\" is not a directory\n";
		return;
	}
	if( chdir(src.c_str()) == -1) {
		cout << "could not change directory to \"" << target << "\"\n";
	}
	Assert(string(Path(".")) == string(target));
}

