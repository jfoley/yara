#ifndef __PATHSEP_H
#define __PATHSEP_H

#if YARA_WINDOWS
#error FileSystem API not supported on Windows
#else
	const string PathSepStr = "/";
	const char PathSepChar = PathSepStr[0];
#endif

#endif

