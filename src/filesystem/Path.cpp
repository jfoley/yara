#include "Path.h"

#include "FileSystem.h"

#include "util/strings.h"
using namespace util;
#include "concur/Lockable.h"

#include <unistd.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdio>

#include "PathSep.h"

namespace {
	Lockable lsMutex;
}

Path::Path(string p) {
	Set(p);
}

void Path::Set(string p) {
	path = SimplifyPath(p);
	Refresh();
}

void Path::Refresh() {
	exists = false;
	file = false;
	directory = false;
	size = 0;

	struct stat file_stats;

	//--- if stat fails, we consider the file as not existing
	if(stat(path.c_str(), &file_stats) == -1)
		return;

	exists = true;

	file = S_ISREG(file_stats.st_mode);
	directory = S_ISDIR(file_stats.st_mode);
	size = file_stats.st_size;
}

string Path::Name() const {
	return afterLast(path, PathSepChar);
}

Path Path::UpDir() const {
	string result = path;

	//--- backtrack over file
	if(File()) {
		result = beforeLast(result, PathSepChar);
	}
	
	//--- backtrack one dir
	return Path(result + PathSepChar + "..");
}

Path Path::GetDir() const {
	//--- backtrack over file
	if(!File()) {
		return path;
	} else {
		return beforeLast(path, PathSepChar);
	}
}

Path Path::ChDir(string rel) const {
	//--- make a new path if given absolute
	if(startsWith(rel, PathSepStr))
		return Path(rel);
	else
		return Path(path + PathSepChar + rel);
}

bool Path::Hidden() const {
	string n = Name();
	return startsWith(n, ".");
}

vector<Path> Path::List() const {
	vector<Path> results;
	
	if(!Directory())
		return results;
	
	DIR *dir = 0;
	dirent *entry = 0;

	ScopeLock lock(lsMutex);

	dir = opendir(path.c_str());

	if(!dir)
		return results;

	do {
		entry = readdir(dir);
		if(entry) {
			string name = entry->d_name;
			
			if(name == "." || name == "..")
				continue; //--- skip special elements

			results.push_back(string(path) + PathSepChar + string(name));
		}
	} while(entry);

	closedir(dir);

	return results;
}

bool Path::Empty() const {
	return List().size() == 0;
}

bool Path::Contains(const string &p) const {
	Path other(p);
	return startsWith(other.path, path);
}

