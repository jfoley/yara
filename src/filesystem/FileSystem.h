#ifndef __FILESYSTEM_H
#define __FILESYSTEM_H

#include "types.h"

/// Resolves all . and .. elements logically
string SimplifyPath(string input);

/// Equivalent to *nix which command, finding executable within getenv(PATH)
string Which(string name);
/// Create a directory, passes if a:elready exists
bool MakeDir(string path);
/// Recursively create directories for fun
bool MakeDirR(string path);
/// Remove a file or empty directory
bool Remove(string path);
/// Remove a directory recursively deleting all subdirs and contents;;; don't sudo RemoveAll ("/");
bool RemoveAll(string path);

bool MoveFile(string src, string dest);

string UniqueSubDir(string path, string prefix="unique");

bool CopyFile(string src, string dest);

/// Set current working path to...
void ChangeDir(string src);

#endif

