
#include "YaraDevice.h"
#include "types.h"
#include <cstdio>



void DrawBar(float val){
	int width=80;
	cout << "[";
	for(int i=0; i<width; i++){
		int crit=width*val;
		if(i<crit)
			cout << "#";
		if(i>=crit)
			cout << " ";
	}
	cout << "]";
}

const char* btn="#####\n#####\n#####\n#####";

int main(void){
	YaraDevice ydev;
	ydev.Open();

	if(!ydev.Valid()) {
		fprintf(stderr, "\nNo Yara Device Found\n");
		return -1;
	} else {
		printf("\nYara Device Found!\n");
	}



	ydev.calibration=Calibration(0, 1024, 0, 1);
	while(1){
		ydev.Reconnect();
		if(!ydev.Valid())
			continue;

		ydev.Update();

		const char* colors[]={
			START_PURPLE,
			START_BLUE,
			START_CYAN
			START_GREEN,
			START_YELLOW,
			START_RED,
		};


		cout << "\e[2J";

		for(int i=0; i<ydev.NumAnalog(); i++){
			cout << colors[i%lengthof(colors)];
			DrawBar(ydev.GetCalibratedAnalog(i));
			cout << "\n";
		}
		cout << "\n";

		loop(i, ydev.NumDigital()){
			if(ydev.GetDigital(i))
				cout << colors[i%lengthof(colors)];
			else
				cout << END_COLOR;
			cout << btn << "\n\n";
		}

	}


	return 0;
}
