#ifndef __JOYSTICK_H
#define __JOYSTICK_H

#include "backend.h"
#include "InputDevice.h"

class Joystick : public InputDevice {
	public:
		static u32 Available();
		static string AvailableName(u32 i);

		Joystick() : id(0), handle(0) { }
		~Joystick() { Close(); }

		bool Open(u32 id=0);
		void Reconnect();
		void Close();

		bool Valid() const;

		//--- nothing to do here
		void Update() { }

		string Name() const;

		u16  NumDigital() const;
		bool GetDigital(u32 i) const;

		u16  NumAnalog() const;
		i16  GetAnalog(u32 i) const;
	
	private:
		u32 id;
		SDL_Joystick* handle;
};

#endif

