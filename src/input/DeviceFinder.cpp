#include "YaraDevice.h"
#include "Joystick.h"

int main(int argc, char **argv) {
	YaraDevice ydev;
	ydev.Open();

	if(!ydev.Valid()) {
		fprintf(stderr, "\nNo Yara Device Found\n");
	} else {
		printf("\nYara Device Found!\n");
	}

	if(SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		fprintf(stderr, "\nCan't initialize Joysticks: %s\n", SDL_GetError());
		return 1;
	}
	show(Joystick::Available());

#if 0
	SDL_Event event;

	loop(i, Joystick::Available()) {
		Joystick js;

		string name = Joystick::AvailableName(i);
		show(name);

		if(js.Open(i)) {
			show(js.NumDigital());
			show(js.NumAnalog());

			while(js.Valid()) {
				while(SDL_PollEvent(&event)) { }

				loop(j, js.NumAnalog()) {
					cout << js.GetAnalog(j) << " ";
				}
				cout << "\t";
				loop(j, js.NumDigital()) {
					cout << js.GetDigital(j) << " ";
				}
				cout << "\n";
				Nap(0.001);
			}
		}
	}
#endif



}

