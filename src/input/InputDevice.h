

#ifndef __INPUTDEVICE_H
#define __INPUTDEVICE_H

#include "concur/RepeaterThread.h"
#include "math/Calibration.h"
#include "types.h"


struct InputDevice {
	virtual void Update(){}
	virtual void Reconnect(){}
	virtual bool Valid () const { return false; }
	virtual u16  NumAnalog () const { return 0; }
	virtual u16  NumDigital() const { return 0; }
	virtual i16  GetAnalog (u32 i) const { return 0; }
	virtual bool GetDigital(u32 i) const { return 0; }

	virtual real GetCalibratedAnalog(u32 i) const { return calibration(GetAnalog(i)); }

	virtual ~InputDevice() {}

	Calibration calibration;
};

class InputDeviceUpdater: public RepeaterThread {
	public:
		InputDeviceUpdater(InputDevice* dev=0): inputDevice(dev) { SetInterval(0); }
		void Rep(){
			if(inputDevice){
				if(inputDevice->Valid())
					inputDevice->Update();
				else
					inputDevice->Reconnect();
			}
			else
				Nap(0.05);
		}
	private:
		InputDevice* inputDevice;
};

#endif
