#ifndef __BIKE_INPUT
#define __BIKE_INPUT
#define BIKE_ANALOG_COUNT 5
#define BIKE_DIGITAL_COUNT 5

#include "types.h"
#include "TTY.h"
#include "macros.h"
#include "concur/RepeaterThread.h"
#include "concur/Lockable.h"

class BikeInput: private Lockable {
	public:
		BikeInput(): expavg(.5) {}
		bool Update();
		real GetAnalog(unsigned i) const;
		u8   GetDigital(unsigned i) const;	
		void Tare();
		volatile real expavg;

		TTY tty;

	private:
		volatile real analog[BIKE_ANALOG_COUNT];
		volatile real analogTare[BIKE_ANALOG_COUNT];
		volatile u8   digital[BIKE_DIGITAL_COUNT];
};

class BikeInputThread: public RepeaterThread{
	public:
		BikeInputThread() { SetInterval(0); }
		void Rep() {
			input.Update();
		}
		BikeInput input;
};
#endif

