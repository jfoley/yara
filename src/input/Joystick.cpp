#include "Joystick.h"

u32 Joystick::Available() {
	int count = SDL_NumJoysticks();
	return MAX(0, count);
}

string Joystick::AvailableName(u32 id) {
	return string(SDL_JoystickName(id));
}

bool Joystick::Valid() const {
	return handle && id < Available() && SDL_JoystickOpened(id);
}

void Joystick::Reconnect() {
	Open(id);
}

bool Joystick::Open(u32 i) {
	id = i; // store for reconnect

	if(handle) Close();
	
	//SDL_JoystickEventState(SDL_ENABLE);
	
	handle = SDL_JoystickOpen(id);
	if(!handle) return false;

	return true;
}

void Joystick::Close() {
	if(Valid()) SDL_JoystickClose(handle);
	if(handle)  handle = 0;
}

string Joystick::Name() const {
	if(!Valid()) return "NO_JOYSTICK";
	return AvailableName(id);
}

u16 Joystick::NumDigital() const {
	if(!Valid()) return 0;
	int count = SDL_JoystickNumButtons(handle);
	return MAX(0, count);
}

u16 Joystick::NumAnalog() const {
	if(!Valid()) return 0;
	int count = SDL_JoystickNumAxes(handle);
	return MAX(0, count);
}

bool Joystick::GetDigital(u32 i) const {
	if(!Valid()) return 0;
	return SDL_JoystickGetButton(handle, i);
}

i16 Joystick::GetAnalog(u32 i) const {
	if(!Valid()) return 0;
	i16 data = SDL_JoystickGetAxis(handle, i);
	return data;
}

