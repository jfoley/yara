#include "SerialDevice.h"
#include "Assert.h"
#include "concur/Chrono.h"
#include "util/strings.h"

#if YARA_LINUX
	#include <unistd.h>
	#include <fcntl.h>
	#include <errno.h>
	#include <termios.h>
	#include <sys/ioctl.h>
#endif


#define FUCK_SERIAL_ERRORS

#ifdef FUCK_SERIAL_ERRORS
#	define PERROR(msg) // Sweet Silence
#else
#	define PERROR(msg) perror(msg)
#endif

SerialDevice::SerialDevice() {
	handle = -1;
}


bool SerialDevice::Open(int id) {
	Close();

	struct termios options;
	int baud_rate = B115200;

	string dev_name = util::stringf("/dev/ttyUSB%d", id);
	int fd = open(dev_name.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);

	if(fd == -1) {
		PERROR("open");
		return false;
	}

	if(tcgetattr(fd, &options) == -1) {
		PERROR("tcgetattr");
		return false;
	}
	if(cfsetispeed(&options, baud_rate) == -1) {
		PERROR("cfsetispeed");
		return false;
	}
	if(cfsetospeed(&options, baud_rate) == -1) {
		PERROR("cfsetispeed");
		return false;
	}

	options.c_cflag |= (CLOCAL | CREAD);

	//8N1
	options.c_cflag &= ~PARENB;
	options.c_cflag &= ~CSTOPB;
	options.c_cflag &= ~CSIZE;
	options.c_cflag &= ~CRTSCTS; // disable hw flow control
	options.c_cflag |= CS8;

	// Raw Input
	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);

	// Raw Output
	options.c_oflag &= ~OPOST;

	// Set configuration
	if(tcsetattr(fd, TCSANOW, &options) == -1) {
		PERROR("tcsetattr");
		return false;
	}

	handle=fd;
	return true;
}

bool SerialDevice::Valid() const {
	return handle != -1;
}


void SerialDevice::Close() {
	if(Valid()){
		int f=handle;
		handle=-1;
		close(f);
	}
}

string SerialDevice::GetLine(int timeout, u32 max) {
	if(!Valid())
		return "";
	string result;
	u32 len=0;
	int triesLeft = timeout;

	while(len < max) {
		int x = GetChar();
		if(!Valid())
			return "";

		//--- block if !timeout
		if(x == EOF || x == -1) {
			if(timeout) {
				if(triesLeft-- <= 0)
					break;
				//--- delay 10ms
				Nap(0.01);
			}
			continue;
		}

		// got a valid char, reset triesLeft
		triesLeft = timeout;
		char c = x & 0xff;

		//--- on a newline type character, finish if we have data
		if(c == '\r')
			continue;
		if(c == '\n')// || c == '\r')
			break;
		result += c;
		len++;
	}

	return result;
}

int SerialDevice::GetChar() {
	if(!Valid())
		return EOF;
	
	char buffer;
	int result = read(handle, &buffer, 1);

	if(result < 0) {
		PERROR("read");
		if(errno == EAGAIN)
			return EOF;
		Close();
	}
	if(result != 1)
		return EOF;

	return buffer;
}

void SerialDevice::PutChar(char c) {
	if(!Valid())
		return;
	if(1 != write(handle, &c, 1)) {
		PERROR("write");
		Close();
	}

 	if(tcflush(handle, TCIOFLUSH) == -1)
 		PERROR("tcflush");
 
}

int SerialDevice::Avail() const {
	Assert(Valid());
	int bytes;
	if(ioctl(handle, FIONREAD, &bytes)<0)
		return 0;
	return bytes;
}

