#include "YaraDevice.h"
#include "TicTac.h"
#include "util/strings.h"
#include "Option.h"
#define MAX_DEVICES 16
#define TIMEOUT 10

using namespace util;

YaraDevice::YaraDevice() {
	currentVibrate = 0;
	currentFanSpeed = 0;
}

bool YaraDevice::Probe() {
	if(!dev.Valid())
		return false;

	dev.PutChar(TICTAC_PROBE);
	loop(i,3){
		string result=dev.GetLine(TIMEOUT);
		if(result=="")
			continue;
		if(result[0]==TICTAC_PROBE_ACK)
			return true;
	}

	Close();
	return false;
}

void YaraDevice::Close() {
	dev.Close();
}

void YaraDevice::Reconnect() {
	if(Probe())
		return;
	loop(i, MAX_DEVICES) {
		if(dev.Open(i) && Probe())
			break;
	}
}

void YaraDevice::Update() {
	Reconnect();
	if(!Valid())
		return;

	dev.PutChar(TICTAC_DUMP_NORESET);
	string response = dev.GetLine(TIMEOUT);
	if(isSpaces(response)) return;

	//--- parsing
	u32 numAnalog, numDigital;

	stringstream ss;
	ss << response;

	//--- update vectors, hopefully only the first time
	ss >> numAnalog;
	ss >> numDigital;

	if(numAnalog != analog.size()) {
		analog.resize(numAnalog);
	}
	if(numDigital != digital.size()) {
		digital.resize(numDigital);
	}

	// TODO check for at least NUM_ANALOG and NUM_DIGITAL
	loop(i, NumAnalog())
		ss >> analog[i];
	loop(i, NumDigital()) {
		i16 data;
		ss >> data;
		digital[i] = data;
	}
}

void YaraDevice::Print(std::ostream &os) const {
	loop(i, NumAnalog())
		os << analog[i] << ' ';
	loop(i, NumDigital())
		os << digital[i] << ' ';
}

i16 YaraDevice::GetAnalog(unsigned i) const {
	if(i<NumAnalog())
		return analog[i];
	return 0;
}

bool YaraDevice::GetDigital(unsigned i) const {
	if(i<NumDigital())
		return digital[i];
	return false;
}

void YaraDevice::SetFan(u8 fanSpeed){
	currentFanSpeed=CLAMP(fanSpeed, 0, 3);
	DigitalOut();
}

void YaraDevice::SetVibrate(bool on){
	currentVibrate=on;
	DigitalOut();
}

void YaraDevice::DigitalOut(){
	dev.PutChar(
			(
			(TICTAC_FAN0+currentFanSpeed)
			|
			(currentVibrate ? TICTAC_MOTOR1 : TICTAC_MOTOR0)
			) ^ 3 // Invert last two bits because Jay fucked up and inverted these digital outs
			);
	dev.GetLine(TIMEOUT);
}
