#include <sstream>
using namespace std;

#include "BikeInput.h"
#include <iostream>

u8 BikeInput::GetDigital(unsigned i) const {
	if(i>=BIKE_DIGITAL_COUNT)
		return 0;
	ScopeLock l(*this);
	return digital[i];
}

real BikeInput::GetAnalog(unsigned i) const {
	if(i>=BIKE_ANALOG_COUNT)
		return 0;
	ScopeLock l(*this);
	return analog[i]-analogTare[i]; 
}

void BikeInput::Tare(){
	Lock();
	loop(i, BIKE_ANALOG_COUNT)
		analogTare[i]=analog[i];
	Unlock();
}

bool BikeInput::Update(){
	if(!tty.Valid()){
		return false;
	}
	const char c='X';
	tty.Write(&c,1);
	int to=30;
	while(tty.Avail()<20 && tty.Valid() && --to)
		usleep(100000);

	if(!tty.Valid() || !to){
		Lock();
		loop(i,BIKE_ANALOG_COUNT){
			analog[i]=0;
			analogTare[i]=0;
		}
		loop(i,BIKE_DIGITAL_COUNT)
			digital[i]=0;
		Unlock();

		return false;
	}

	char buf[256]={0};
	tty.Read(buf,MIN(tty.Avail(),int(sizeof(buf))));

	loop(i,sizeof(buf)){
		if(buf[i]=='\n'){
			buf[i]=0;
			break;
		}
	}

	buf[sizeof(buf)-1]=0;
	stringstream ss;
	ss<<buf;

	Lock();
	loop(i,BIKE_ANALOG_COUNT){
		int v;
		ss >> v;
		analog[i]=LERP(analog[i], (real(v)/(1<<10)), expavg);
	}

	loop(i,BIKE_DIGITAL_COUNT){
		int v;
		ss >> v;
		digital[i]=v;
	}
	Unlock();

	return true;
}

