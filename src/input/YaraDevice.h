#ifndef __YARADEVICE_H
#define __YARADEVICE_H

#include "InputDevice.h"
#include "SerialDevice.h"

class YaraDevice: public InputDevice {
	public:
		YaraDevice();
		virtual ~YaraDevice() { Close(); }

		void Open() { Reconnect(); }
		void Close();
		void Reconnect();

		bool Probe();


		//--- As Input Device
		void Update();
		bool Valid() const { return dev.Valid(); }
		u16 NumAnalog() const { return analog.size(); }
		u16 NumDigital() const { return digital.size(); }

		i16 GetAnalog(unsigned i) const;
		bool GetDigital(unsigned i) const;

		void Print(std::ostream& os) const;

		//--- As output device
		void SetFan(u8 fanSpeed);
		void SetVibrate(bool on);

	private:

		void DigitalOut();

		u8 currentFanSpeed;
		bool currentVibrate;

		SerialDevice dev;

		vector<bool> digital;
		vector<i16>  analog;
};



#endif
