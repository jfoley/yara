#ifndef __SERIALDEVICE_H
#define __SERIALDEVICE_H

#include "types.h"
#include "backend.h"

class SerialDevice {
	public:
		static const u32 MaxLineLength = 256;
		SerialDevice();
		~SerialDevice() { Close(); }
		
		/// Opens a connection to the serial port with the OS specific name
		bool Open(int id);

		/// Close the handle
		void Close();

		/// Returns true if the serial port is still in a good state
		bool Valid() const;

		/// Reads a line from the serial port or returns an empty string
		string GetLine(int timeout=0, u32 max=MaxLineLength);

		/// Reads a character
		int GetChar();

		/// Sends down a character
		void PutChar(char data);

		/// Returns number of bytes available in the input buffer
		int Avail() const;

	private:
		int    handle;
};

#endif

