#ifndef __TICTAC_H
#define __TICTAC_H

#define NUM_ANALOG  5
#define NUM_DIGITAL 7

/*
A[0] = Gas
A[1] = Break
A[2] = Steering

I[0] = Radio0
I[1] = Radio1
I[2] = Radio2
I[3] = Radio3
I[4] = Lights
I[5] = Radio

I[RC4] = Horn

O[0] = Fan & 0b01
O
O[2] = Motors
*/

#define TICTAC_PROBE     '?'
#define TICTAC_PROBE_ACK '!'

#define TICTAC_DUMP      'X'
#define TICTAC_DUMP_NORESET 'Y'

#define TICTAC_BASE      0x30 // '0'
#define TICTAC_FAN0      TICTAC_BASE + 0b000
#define TICTAC_FAN1      TICTAC_BASE + 0b001
#define TICTAC_FAN2      TICTAC_BASE + 0b010
#define TICTAC_FAN3      TICTAC_BASE + 0b011
#define TICTAC_MOTOR0    TICTAC_BASE + 0b000
#define TICTAC_MOTOR1    TICTAC_BASE + 0b100

/*
     Motor   Fan
'0'    0      0
'1'    0      1
'2'    0      2
'3'    0      3
'4'    1      0
'5'    1      1
'6'    1      2
'7'    1      3
*/



#endif

