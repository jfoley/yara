#ifndef __KEYS_H
#define __KEYS_H

#include "backend.h"

typedef u16 KeyCode;

const KeyCode KEY_BACKSPACE = SDLK_BACKSPACE;
const KeyCode KEY_TAB       = SDLK_TAB;
const KeyCode KEY_ENTER     = SDLK_RETURN;
const KeyCode KEY_ESCAPE    = SDLK_ESCAPE;
const KeyCode KEY_SPACE     = SDLK_SPACE;
const KeyCode KEY_QUOTE     = SDLK_QUOTE;
const KeyCode KEY_COMMA     = SDLK_COMMA;
const KeyCode KEY_MINUS     = SDLK_MINUS;
const KeyCode KEY_PERIOD    = SDLK_PERIOD;
const KeyCode KEY_SLASH     = SDLK_SLASH;
const KeyCode KEY_0         = SDLK_0;
const KeyCode KEY_1         = SDLK_1;
const KeyCode KEY_2         = SDLK_2;
const KeyCode KEY_3         = SDLK_3;
const KeyCode KEY_4         = SDLK_4;
const KeyCode KEY_5         = SDLK_5;
const KeyCode KEY_6         = SDLK_6;
const KeyCode KEY_7         = SDLK_7;
const KeyCode KEY_8         = SDLK_8;
const KeyCode KEY_9         = SDLK_9;
const KeyCode KEY_SEMICOLON = SDLK_SEMICOLON;
const KeyCode KEY_EQUALS    = SDLK_EQUALS;

const KeyCode KEY_BACKSLASH = SDLK_BACKSLASH;
const KeyCode KEY_A         = SDLK_a;
const KeyCode KEY_B         = SDLK_b;
const KeyCode KEY_C         = SDLK_c;
const KeyCode KEY_D         = SDLK_d;
const KeyCode KEY_E         = SDLK_e;
const KeyCode KEY_F         = SDLK_f;
const KeyCode KEY_G         = SDLK_g;
const KeyCode KEY_H         = SDLK_h;
const KeyCode KEY_I         = SDLK_i;
const KeyCode KEY_J         = SDLK_j;
const KeyCode KEY_K         = SDLK_k;
const KeyCode KEY_L         = SDLK_l;
const KeyCode KEY_M         = SDLK_m;
const KeyCode KEY_N         = SDLK_n;
const KeyCode KEY_O         = SDLK_o;
const KeyCode KEY_P         = SDLK_p;
const KeyCode KEY_Q         = SDLK_q;
const KeyCode KEY_R         = SDLK_r;
const KeyCode KEY_S         = SDLK_s;
const KeyCode KEY_T         = SDLK_t;
const KeyCode KEY_U         = SDLK_u;
const KeyCode KEY_V         = SDLK_v;
const KeyCode KEY_W         = SDLK_w;
const KeyCode KEY_X         = SDLK_x;
const KeyCode KEY_Y         = SDLK_y;
const KeyCode KEY_Z         = SDLK_z;
const KeyCode KEY_DELETE    = SDLK_DELETE;
const KeyCode KEY_PAD_0     = SDLK_KP0;
const KeyCode KEY_PAD_1     = SDLK_KP1;
const KeyCode KEY_PAD_2     = SDLK_KP2;
const KeyCode KEY_PAD_3     = SDLK_KP3;
const KeyCode KEY_PAD_4     = SDLK_KP4;
const KeyCode KEY_PAD_5     = SDLK_KP5;
const KeyCode KEY_PAD_6     = SDLK_KP6;
const KeyCode KEY_PAD_7     = SDLK_KP7;
const KeyCode KEY_PAD_8     = SDLK_KP8;
const KeyCode KEY_PAD_9     = SDLK_KP9;

const KeyCode KEY_UP        = SDLK_UP;
const KeyCode KEY_DOWN      = SDLK_DOWN;
const KeyCode KEY_RIGHT     = SDLK_RIGHT;
const KeyCode KEY_LEFT      = SDLK_LEFT;
const KeyCode KEY_INSERT    = SDLK_INSERT;
const KeyCode KEY_HOME      = SDLK_HOME;
const KeyCode KEY_END       = SDLK_END;
const KeyCode KEY_PGUP      = SDLK_PAGEUP;
const KeyCode KEY_PGDN      = SDLK_PAGEDOWN;

const KeyCode KEY_F1        = SDLK_F1;
const KeyCode KEY_F2        = SDLK_F2;
const KeyCode KEY_F3        = SDLK_F3;
const KeyCode KEY_F4        = SDLK_F4;
const KeyCode KEY_F5        = SDLK_F5;
const KeyCode KEY_F6        = SDLK_F6;
const KeyCode KEY_F7        = SDLK_F7;
const KeyCode KEY_F8        = SDLK_F8;
const KeyCode KEY_F9        = SDLK_F9;
const KeyCode KEY_F10       = SDLK_F10;
const KeyCode KEY_F11       = SDLK_F11;
const KeyCode KEY_F12       = SDLK_F12;
const KeyCode KEY_F13       = SDLK_F13;
const KeyCode KEY_F14       = SDLK_F14;
const KeyCode KEY_F15       = SDLK_F15;

const KeyCode KEY_NUMLOCK   = SDLK_NUMLOCK;
const KeyCode KEY_CAPSLOCK  = SDLK_CAPSLOCK;
const KeyCode KEY_SCROLLOCK = SDLK_SCROLLOCK;
const KeyCode KEY_RSHIFT    = SDLK_RSHIFT;
const KeyCode KEY_LSHIFT    = SDLK_LSHIFT;
const KeyCode KEY_RCTRL     = SDLK_RCTRL;
const KeyCode KEY_LCTRL     = SDLK_LCTRL;
const KeyCode KEY_RALT      = SDLK_RALT;
const KeyCode KEY_LALT      = SDLK_LALT;
const KeyCode KEY_RMETA     = SDLK_RMETA;
const KeyCode KEY_LMETA     = SDLK_LMETA;

#endif //__KEYS_H

