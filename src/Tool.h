#ifndef __TOOL_H
#define __TOOL_H

#include <cstdio>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <vector>
using std::vector;

#include "util/parse.h"
#include "util/HumanReadable.h"
#include "util/Arguments.h"
#include "util/ConfigParser.h"

using namespace util;

const string HeaderExt(".h");
const string ImplExt(".cpp");

const string FromStringMethod("FromString");

class Indentation : public Printable {
	public:
		Indentation(int n = 0) : count(n) { }
		void More() { count ++; }
		void Less() { count --; }
		void Clear() { count = 0; }
	private:
		virtual void Print(std::ostream& os) const { loop(i, count) { os << "\t"; } }
		int count;
};

#if YARA_WINDOWS
	const char PathSep = '\\';
#else //YARA_LINUX
	const char PathSep = '/';
#endif

inline string ConstRef(string type) { return "const " + type + "&"; }
inline string MakeIncludeGuard(string fileName) {
	string relative = ifContainsAfterLast(fileName, PathSep);
	string base = before(relative, '.');
	string ext  = after(relative, '.');

	return "__" + toUpper(base) + "_" + toUpper(ext);
}


#endif

