//macros.h

#ifndef __MACROS_H
#define __MACROS_H

#include "types.h"

#if 0
#define MAX(a,b) (((a)>(b)) ? (a) : (b))
#define MIN(a,b) (((a)>(b)) ? (b) : (a))
#define SIGN(x)   (((x) < 0) ? -1 : 1)
#define RATIO(a,b) (real(a)/real(b))
#define SQUARE(x) ((x)*(x))
#define ABS(x)    (((x)>0) ? (x) : (-x))
#define LERP(x,y,a) ((x)*(1-(a)) + (y)*(a))
#else
template <typename T, typename U> inline T MAX(const T& a, const U& b){ return a>b?a:b; }
template <typename T, typename U> inline T MIN(const T& a, const U& b){ return a<b?a:b; }
template <typename T> inline u8 SIGN   (const T& a){ return (a<0) ? -1 : (a>0) ? 1 : 0; }
template <typename T> inline T  SQUARE (const T& x){ return x*x; }
template <typename T> inline T  ABS    (const T& x){ return x>=0 ? x : -x; }
template <typename T, typename U> inline real RATIO(const T& a, const U& b){ return real(a)/real(b); }
template <typename T, typename U, typename V> inline T LERP(const T& a, const U& b, const V& x){ return a*(1-x)+b*x; }
#endif

#define isNaN(x) ((x)!=(x))

#define MIN3(a,b,c) MIN(MIN(a,b),c)
#define MAX3(a,b,c) MAX(MAX(a,b),c)
#define CLAMP(x,a,b) MIN(MAX(x,a), b)

// Operators
#define lengthof(x) ((sizeof(x))/(sizeof(x[0])))
#define bitsizeof(x) (8*sizeof(x))


// Control Structures
#define case(n) case n:
#define once() for(static bool __once=false; !__once; __once=true)
#define once_every(N) for(static int __every=0, __once=0; (++__once)%2 ; ) if((++__every)%N==0)

#define foreach(T, iter, obj) for( T iter=(obj).begin(); iter!=(obj).end(); iter++ )

#define match(T, var) \
	for(bool __outside=true, __cascade=false, __implicit_break=false ; __outside; __outside=false) \
		for(T __el=(var); __outside; __outside=false)
#define match_one(T, var) \
	for(bool __outside=true, __cascade=false, __implicit_break=true ; __outside; __outside=false) \
		for(T __el=(var); __outside; __outside=false)
#define with_cond(cond) if((__cascade=__cascade ? !__implicit_break : (cond)))
#define with(val) with_cond(__el==(val))
#define within(low, high) with_cond( (low)<=__el && __el<=(high) )
#define default() if((__cascade && !__implicit_break) || !__cascade)

#define floop(var, start, end, N) loop(__i, N+1) for(real var=(start)+__i*((real(end)-(start))/(N)); var==var; var=math::NaN)
#define uloop(i,N) for(unsigned i=0; i<unsigned(N); i++)
#define rloop(i,N) for(int i=int(N)-1; i>=0; i--)
#define loop(i,N)  for(int i=0; i<int(N); i++)
#define loop1(i,      N      ) loop (i,    N    )
#define loop2(i,j,    N,M    ) loop1(i,    N    ) loop(j,M)
#define loop3(i,j,k,  N,M,O  ) loop2(i,j,  N,M  ) loop(k,O)
#define loop4(i,j,k,l,N,M,O,P) loop3(i,j,k,N,M,O) loop(l,P)

// Color
#ifdef YARA_WINDOWS
#define START_WHITE     ""
#define START_CYAN      ""
#define START_PURPLE    ""
#define START_BLUE      ""
#define START_YELLOW    ""
#define START_GREEN     ""
#define START_RED       ""
#define START_BLACK     ""
#define END_COLOR       ""
#else
#define START_WHITE    "\e[1;37m"
#define START_CYAN     "\e[1;36m"
#define START_PURPLE   "\e[1;35m"
#define START_BLUE     "\e[1;34m"
#define START_YELLOW   "\e[1;33m"
#define START_GREEN    "\e[1;32m"
#define START_RED      "\e[1;31m"
#define START_BLACK    "\e[1;30m"
#define END_COLOR      "\e[0m"
#endif

// Text
#ifdef YARA_WINDOWS
	#define snprintf _snprintf
#endif

#define PLURAL(var) ((var == 1) ? "" : "s")

// regular show
/*
#define show(var) \
	do { std::cout << START_BLUE #var END_COLOR "\t= " << START_YELLOW << (var) << END_COLOR "\n"; } while (0)
*/
// show on crack, for those really annoying ones
#define show(var) \
	do { std::cout << START_GREEN << __func__ << END_COLOR << ":" << __LINE__ << ":\t" << START_BLUE #var END_COLOR "\t= " << START_YELLOW << (var) << END_COLOR "\n"; } while (0)

#define showf(fmt, var) printf(START_BLUE #var END_COLOR "\t= " START_YELLOW fmt END_COLOR "\n", var)

// Misc.
#ifdef __GNUC__
	#define CAN_BE_UNUSED __attribute__((unused))
#else
	#define CAN_BE_UNUSED
	//#error "Not using a variant of GCC"
#endif

#endif

