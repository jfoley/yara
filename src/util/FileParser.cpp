#include "FileParser.h"
#include "macros.h"

#include "strings.h"

#include <iostream>
#include <fstream>

using namespace util;
using namespace FileFormat;

bool FileData::Parse(string inputFile) {
	fileName = inputFile.c_str();

	std::ifstream file;
	file.open(fileName);
	if(!file.is_open()) return false;

	u32 lineNumber=0;
	while(file.good() && !file.eof()) {
		lineNumber++;

		string text;
		getline(file, text);

		//--- if there's a comment, chop it.
		if(contains(text, Comment))	{
			text = before(text, Comment);
		}

		//--- see if we're an empty line, and continue with no error
		if(isSpaces(text)) continue;

		Line line;
		line.number = lineNumber;
		line.data = text;

		//--- look for special directives
		if(contains(text, Special)) {
			line.data = after(text, Special);
			directives.push_back(line);
		} else {
			line.data = text;
			lines.push_back(line);
		}
	}
	return true;
}

