#include "hexdump.h"
#include "util/strings.h"
#include "Assert.h"
#include <cstdio>

namespace {
	u32 toBoundary(u32 num, u32 modulus) {
		if(num % modulus != 0) {
			num += modulus - (num % modulus);
		}
		return num;
	}
	char safeChar(char c) {
		if(c <= ' ') return '.';
		if(c >  '~') return '.';
		return c;
	}
}

void util::hexdump(
		std::ostream &os,
		const void *data,
		size_t len,
		bool colored,
		bool label,
		u32 label_start,
		bool show_ascii,
		u32 bytesPerCol,
		u32 numCols
		)
{
	//--- a few reasonable assumptions
	Assert(len < 0xffffffff); 
	Assert(data);
	Assert(len);

	u32 offset = 0;
	u32 size = u32(len);
	u32 bytesPerRow = bytesPerCol * numCols;
	u32 square_size = toBoundary(size, bytesPerRow);

	string ascii;

	char buffer[256]; //--- using this is better than stringfing everywhere

	const char *byte_ptr = (const char*) data;
	for(offset = 0; offset < square_size; offset++) {
		if(label && offset % bytesPerRow == 0) {
			snprintf(buffer, sizeof(buffer), "0x%08x: ", offset+label_start);
			os << buffer;
		}

		if(offset % bytesPerCol == 0) {
			if(show_ascii) ascii += ' ';
			os << "  ";
		}

		if(offset < size) {
			u8 c = byte_ptr[offset];
			snprintf(buffer, sizeof(buffer), "%02x ", c);
			os << buffer;
			if(show_ascii) ascii += safeChar(c);
		} else {
			if(show_ascii) ascii += ' ';
			os << "   "; //--- space for 2 hex digits and a space
		}
		
		if( (offset+1) % bytesPerRow == 0 ) {
			if(show_ascii) {
				os << "  " << ascii;
				ascii = "";
			}
			os << "\n";
		}
	}
	os << ascii << "\n";
}


