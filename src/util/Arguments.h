#ifndef __ARGUMENTS_H
#define __ARGUMENTS_H

#include "types.h"
#include "HumanReadable.h"
#include "Option.h"
#include <vector>
#include <map>

class Arguments {
	public:
		bool GetFlag(const string &name);
		Option<string> GetOption(const string &name);
		string GetParameter(const string &name);

		void Set(const string &key, const string &value);
		virtual void Print(std::ostream &os) const;
	private:
		std::map<string, string> data;
};

//--- private
class ArgDef;

class ArgumentParser : public Printable {
	public:
		ArgumentParser(bool loud=true) : printErrors(loud) { }
		void DefineFlag(string longName, string shortName, string description);
		void DefineOption(string longName, string shortName, string description);
		void DefineParameter(string name, string description);

		Option<Arguments> Parse(int argc, const char * const *argv);
		
		virtual void Print(std::ostream& os) const;
		//--- prints usage, but needs to be given program name
		void PrintUsage(std::ostream &os, const string &argv0) const;
	private:
		bool printErrors;
		std::vector<ArgDef> allowable;
};



//--- internal representation of Argument Definitions
struct ArgDef : public Printable {
	ArgDef(string ln, string sn, string desc): longName(ln), shortName(sn), description(desc) { }
	bool mandatory;
	bool parameter;
	string longName;
	string shortName;
	string description;

	//--- methods
	virtual void Print(std::ostream &os) const;
};




#endif

