#ifndef __HEXDUMP_H
#define __HEXDUMP_H

#include "types.h"
#include <iostream>

namespace util {
	void hexdump(std::ostream &os, const void *data, size_t len, bool colored=true, bool label=true, u32 label_start=0, bool show_ascii=true, u32 bytesPerCol=4, u32 numCols=2);
}

#endif

