#ifndef __STRINGS_H
#define __STRINGS_H

#include "types.h"
#include <vector>

#define ifContainsAfterLast(str, delim) \
  (util::contains(str, delim) ? util::afterLast(str, delim) : str)

namespace util {
	/// printf style C++ string generator
	string stringf(const char*, ...);
	/// apply tolower to each character of input for case-insensitive comparisons
	string toLower(const string &);
	/// apply toupper to each character of input
	string toUpper(const string &);

	/// return true if all spaces
	bool isSpaces(const string &);
	/// remove preceeding and trailing whitespace and compact inner whitespace to one space only; ex: " a  b " -> "a b"
	string compactSpaces(const string &);

	/// remove n characters from the end of the string
	string removeEnd(const string&, int n);
	
	/// returns true if source starts with prefix
	bool startsWith(const string& source, const string &prefix);
	/// returns true if source ends with suffix
	bool endsWith(const string& source, const string &suffix);
	
	/// returns at(0) unless index is negative, where it returns the last character for -1, etc
	char smartAt(const string& src, int index);
	
	/// returns true if needle is in haystack
	bool contains(const string& haystack, const string &needle);
	/// returns true if needle is in haystack
	bool contains(const string& haystack, const char needle);

	/// splits source string into pieces at delimiter
	std::vector<string> split(const string &source, const char delimiter);

	/// returns the part of the string in source before delimiter
	string before(const string &source, const char delimiter);
	/// returns the part of the string in source after delimiter
	string after(const string &source, const char delimiter);
	
	/// returns the part of the string in source before last delimiter
	string beforeLast(const string &source, const char delimiter);
	/// returns the part of the string in source after last delimiter
	string afterLast(const string &source, const char delimiter);

	/// Combines a vector of strings into one string using the given delimiter between each
	string join(const vector<string> &input, const char delim);
}



#endif

