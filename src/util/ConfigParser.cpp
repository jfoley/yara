#include "macros.h"
#include "ConfigParser.h"
#include "strings.h"
#include <cstdio>

using namespace util;
using namespace FileFormat;

bool ConfigFileData::Parse(string confName) {
	FileData raw;
	if(! raw.Parse(confName)) return false;

	//--- copy over specials
	directives = raw.directives;

	//--- parse lines
	loop(i, raw.lines.size()) {
		const char* fileName = raw.fileName;
		u32 lineNumber = raw.lines[i].number;
		string data    = raw.lines[i].data;

		//--- if nonempty and without an = sign, flag an error
		if(!contains(data, Assignment)) {
			printf("%s:%d: expected assignment character '%c'\n", fileName, lineNumber, Assignment);
			continue; //--- try and skip that line and move on...
		}

		//--- ignore semicolons
		if(contains(data, ';')) {
			data = before(data, ';');
		}

		//--- line of the format
		//    
		//    TYPE NAME = VALUE
		//    can't compact spaces because that doesn't respect quotes
		//    split on equals to find VALUE
		//    split on space to find TYPE and NAME

		ConfigVar cur;

		//--- split around =
		cur.value = after(data, Assignment);
		data = compactSpaces(before(data, Assignment));

		//--- split around space
		if(!contains(data, ' ')) {
			printf("%s:%d: expected type and value, only received \"%s\"\n", fileName, lineNumber, data.c_str());
			continue;
		}

		cur.name = afterLast (data, ' ');
		if(isSpaces(cur.name)) {
			printf("%s:%d: expected variable name, only received \"%s\"\n", fileName, lineNumber, data.c_str());
			continue;
		}

		cur.type = beforeLast(data, ' ');
		
		defines.push_back(cur);
	}

	return true;
}

