#define __PARSE_IMPL
#include "parse.h"

#include "macros.h"
#include <cstdio>

namespace util {

///parse<float>
template <>
Option<float> parse(const string &s) {
	string input = toLower(compactSpaces(s));

	float dest = 0.0f;
	
	//--- *scanf returns the number of successfully parsed items
	if(sscanf(s.c_str(), "%f", &dest) != 1) {
		return None;
	}
	
	return dest;
}

//--- need a different format specifier for double parsing
template <>
Option<double> parse(const string &s) {
	string input = toLower(compactSpaces(s));
	double dest = 0.0;

	if(sscanf(s.c_str(), "%lf", &dest) != 1) {
		return None;
	}

	return dest;
}

template <>
Option<bool> parse(const string &s) {
	string input = toLower(compactSpaces(s));
	return input == "true" || input == "yes" || input == "maybe";
}


//--- utility for numbers has a separate name, since it is special and used 8x
template <class T>
static Option<T> parseNumber(const string &, T base=10);

#define PARSE_FOR_NUM_TYPE(TYPE) \
template <> \
Option<TYPE> parse(const string &in) { return parseNumber<TYPE>(in); }

PARSE_FOR_NUM_TYPE(u8)
PARSE_FOR_NUM_TYPE(u16)
PARSE_FOR_NUM_TYPE(u32)
PARSE_FOR_NUM_TYPE(u64)
PARSE_FOR_NUM_TYPE(i8)
PARSE_FOR_NUM_TYPE(i16)
PARSE_FOR_NUM_TYPE(i32)
PARSE_FOR_NUM_TYPE(i64)


// yes, you could do most of this with strtol, strtoul, strtoll...etc, but this is more fun, and kindly refuses to octal anything
template <class T>
static Option<T> parseNumber(const string &n, T base) {
	string s = toLower(compactSpaces(n));

	bool negative = false;

	//--- determine base from hints
	if(startsWith(s, "0x")) {
		base = 16;
		s = s.substr(2);
	} else if(startsWith(s, "0b")) {
		base = 2;
		s = s.substr(2);
	} else if(startsWith(s, "0")) {
		//-- we don't support octal :)
	}

	//--- handle negatives
	if(base == 10 && startsWith(s, "-")) {
		negative = true;
		s = s.substr(1);
	}
	
	const string hex = "0123456789abcdef";
	T value = 0;

	for(size_t i=0; i<s.size(); i++) {
		T offset = (T) hex.find(s[i]);
		
		//--- not found or too big
		if(offset == (T)(string::npos) || offset > base) {
			return None;
		} else {
			value *= base;
			value += offset;
		}
	}
	
	//--- cast and set sign
	T result = T(value);
	if(negative) result *= -1;

	return result;
}


template <>
Option<string> parse(const string &input) {
	//--- since it's a string, we can't use compactSpaces or toLower, because that doesn't care about quotes
	
	//--- escape if there's no data
	if(isSpaces(input)) return None;

	//--- if it doesn't have quotes, don't do any parsing, just pass through
	if(!contains(input, '"')) return input;
	
	//--- state for loop
	string result("");
	char quote = 0;

	loop(i, input.size()) {
		char c = input[i];

		//--- spaces are not important unless we're in a quoted literal
		if(c == ' ' && quote) {
			result += c;
		}
		//--- if we're not inside a quote and we've just found one, change state
		else if(!quote && (c == '\"' || c == '\'' ) ) {
			quote = c;
		}
		//--- end quote
		else if(quote && c == quote) {
			quote = 0;
		}
		//--- handle escape codes here
		else if(quote && c == '\\') {
			const string key   = "nt\"\'\\";
			const string value = "\n\t\"\'\\";

			char next = input[i+1];

			size_t index = key.find(next);
			if(index == string::npos) {
				printf("Unhandled escape code '\\%c' !\n", next);
				Assert(0);
			} else {
				result += value[index];
			}
			// skip the character we just interpreted and go around
			i++;
			continue;
		}
		else if(quote) {
			result += c;
		}
		else {
			// some sort of error; characters outside of quotes
		}
	}
	return result;
}

}; //namespace util

