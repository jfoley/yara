#include "Arguments.h"

#include "macros.h"
#include "Assert.h"

#include "strings.h"
#include "parse.h"
using namespace util;
using std::vector;

#include <algorithm>
#include <cstdio>

namespace {
	const string LongOpt  = "--";
	const string ShortOpt = "-";
};

bool Arguments::GetFlag(const string &name) {
	return data.find(name) != data.end();
}

Option<string> Arguments::GetOption(const string &name) {
	if(data.find(name) != data.end()) {
		return data[name];
	}
	return None;
}

string Arguments::GetParameter(const string &name) {
	Assert(data.find(name) != data.end());
	return data[name];
}

void Arguments::Set(const string &key, const string &value) {
	data[key] = value;
}

void Arguments::Print(std::ostream& os) const {
	os << "Arguments{\n";
	for(std::map<string,string>::const_iterator it = data.begin(); it!=data.end(); it++) {
		os << "  " << it->first << ":" << it->second << ",\n";
	}
	os << "}\n";
}

void ArgumentParser::DefineFlag(string ln, string sn, string desc) {
	ArgDef flag(ln, sn, desc);
	flag.mandatory = false;
	flag.parameter = false;

	allowable.push_back(flag);
}

void ArgumentParser::DefineOption(string ln, string sn, string desc) {
	ArgDef option(ln, sn, desc);
	option.mandatory   = false;
	option.parameter   = true;
	
	allowable.push_back(option);
}

void ArgumentParser::DefineParameter(string name, string description) {
	ArgDef param(name, "", description);
	param.mandatory = true;
	param.parameter = false;
	
	allowable.push_back(param);
}

Option<Arguments> ArgumentParser::Parse(const int argc, const char * const *argv) {
	Arguments results;
		
	//--- count expected parameters
	vector<int> expected;
	size_t foundCount = 0;

	loop(i, allowable.size()) {
		if(allowable[i].mandatory) expected.push_back(i);
	}

	/// start from 1 to skip executable name
	for(int i=1; i<argc; i++) {
		//printf("argv[%d]=\"%s\"\n",i,argv[i]);
		const string &cur = argv[i];

		int index = -1;

		if(startsWith(cur, LongOpt)) {
			string match = cur.substr(LongOpt.size());
			loop(j, allowable.size()) {
				if(allowable[j].longName == match) {
					index = j;
					break;
				}
			}
		} else if(startsWith(cur, ShortOpt)) {
			string match = cur.substr(ShortOpt.size());
			loop(j, allowable.size()) {
				if(allowable[j].shortName == match) {
					index = j;
					break;
				}
			}
		} else {
			if(foundCount < expected.size()) {
				int id = expected[foundCount];
				Assert(allowable[id].mandatory);
				foundCount++;
				index = id;
			} else {
				if(printErrors) printf("%s: *** Extra parameter \"%s\" found\n", argv[0], argv[i]);
				return None;
			}
		}
		
		//--- throw error if not expected
		if(index == -1) {
			if(printErrors) printf("%s: *** Unrecognized option \"%s\"\n", argv[0], argv[i]);
			return None;
		}

		const ArgDef &def = allowable[index];

		//--- prepare a key
		string key = (def.longName == "") ? def.shortName : def.longName;
		//--- prepare a value
		string value = cur;

		if(def.parameter) {

			//--- make sure there is a next, and that it doesn't start with a "-" or a "--"
			if(i+1 < argc && !startsWith(argv[i+1], ShortOpt)) { 
				value = argv[i+1];
				++i; //--- don't process the next item twice
			} else {
				if(printErrors)	printf("%s: *** Expected parameter after \"%s\"\n", argv[0], argv[i]);
				return None;
			}
		}
		
		results.Set(key, value);
	} //--- end for argvs

	//--- ensure that we found all mandatory parameters
	if(foundCount<expected.size()) {
		int amount = expected.size() - foundCount;
		if(printErrors) printf("%s: *** Expected %d more parameter%s\n" , argv[0], amount, PLURAL(amount));
		return None;
	}

	return results;
}

void ArgDef::Print(std::ostream &os) const {
	if(!mandatory) {
		if(longName != "") os << LongOpt << longName << " ";
		if(shortName != "") os << ShortOpt << shortName << " ";
		if(parameter) os << "(parameter) ";
	} else {
		os << longName << " ";
	}

	os << "  :  ";
	if(!mandatory) os << "(Optional) ";
	
	os << description;
}

namespace {
static bool __MandatoryFirst(const ArgDef &a, const ArgDef &b) { return a.mandatory; }
}

void ArgumentParser::PrintUsage(std::ostream &os, const string& argv0) const {
	vector<int> parameters;
	loop(i, allowable.size()) {
		if(allowable[i].mandatory == true) parameters.push_back(i);
	}

	os << "*** Usage: " << argv0;
	loop(i, parameters.size()) {
		os << " " << allowable[i].longName;
	}
	os << "\n\n";

	Print(os);
}

void ArgumentParser::Print(std::ostream &os) const {
	vector<ArgDef> sorted(allowable);

	std::sort(sorted.begin(), sorted.end(), __MandatoryFirst);

	loop(i, sorted.size()) {
		os << "  " << sorted[i] << "\n";
	}
	os << "\n";
}


