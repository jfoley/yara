#ifndef __HUMANREADABLE_H
#define __HUMANREADABLE_H

#include <iostream>

#define PRINTABLE(T) inline std::ostream& operator<< (std::ostream& os, const T& obj){ obj.Print(os); return os; } 

struct Printable{
	virtual void Print(std::ostream& os) const = 0;
	virtual ~Printable() {}
};

PRINTABLE(Printable);

#endif
