#ifndef __FILEPARSER_H
#define __FILEPARSER_H

#include "types.h"
#include <vector>
using std::vector;

struct Line {
	u32 number;
	string data;
};

struct FileData {
	bool Parse(string fileName);
	const char* fileName;
	vector<Line> lines;
	vector<Line> directives;
};

namespace FileFormat {
	static const char Comment    = '#';
	static const char Assignment = '=';
	static const char ScopeSep   = '.';
	static const char Special    = '@';
}

#endif

