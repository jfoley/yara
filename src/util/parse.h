#ifndef __PARSE_H
#define __PARSE_H

#include "strings.h"
#include "Option.h"

#ifndef __PARSE_IMPL
	#define TEMPLATE_SCOPE extern
#else
	#define TEMPLATE_SCOPE
#endif

#define FOR_PRIMITIVE(type) \
	template <> \
	Option<type> parse(const string&)

namespace util {

template <class T>
Option<T> parse(const string &input) {
	return T::FromString(input);
}

//--- explicitly define these types as having special callbacks; they can't implement FromString
FOR_PRIMITIVE(string);
FOR_PRIMITIVE(float);
FOR_PRIMITIVE(double);
FOR_PRIMITIVE(bool);
FOR_PRIMITIVE(u8);
FOR_PRIMITIVE(u16);
FOR_PRIMITIVE(u32);
FOR_PRIMITIVE(u64);
FOR_PRIMITIVE(i8);
FOR_PRIMITIVE(i16);
FOR_PRIMITIVE(i32);
FOR_PRIMITIVE(i64);


}; //namespace util

#endif

