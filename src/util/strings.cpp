#include "strings.h"
#include "macros.h"
#include <cstdarg>
#include <cstdio>
#include "Assert.h"

#define INITIAL_BUFFER_SIZE 256

string util::stringf(const char *fmt, ...) {
	string buf("");
	int len = 0;
	va_list args;

#if YARA_WINDOWS
	va_start(args, fmt);
	len = _vscprintf(fmt, args);
	va_end(args);
#else
	va_start(args, fmt);
	len = vsnprintf(0, 0, fmt, args)+1;
	va_end(args);
#endif

	buf.resize(len);

	va_start(args, fmt);
	vsnprintf(&buf[0], buf.size(), fmt, args);
	va_end(args);

#if YARA_LINUX
	buf.resize(len-1); // chop null
#endif

	return buf;
}

string util::toLower(const string &mixed) {
	string result(mixed);
	
	loop(i, result.size())
		result[i] = tolower(result[i]);

	return result;
}

string util::toUpper(const string &mixed) {
	string result(mixed);
	
	loop(i, result.size())
		result[i] = toupper(result[i]);

	return result;
}

bool util::isSpaces(const string &s) {
	loop(i, s.size())
		if(s[i] > ' ')
			return false;
	return true;
}

string util::compactSpaces(const string &s) {
	string result;

	if(isSpaces(s)) {
		return "";
	}

	int lastChar = -1;
	for(size_t i=0; i<s.length(); i++, lastChar++) {
		const char &c = s[i];

		if(lastChar == -1 || s[lastChar] <= ' ') {
			if(c <= ' ') {
				continue;
			} else {
				result.push_back(c);
			}
		} else {
			result.push_back(c);
		}
	}

	//--- delete last space if any
	if(s[lastChar] <= ' ' && result.size()) {
		result = removeEnd(result, 1);
	}

	return string(result.c_str());
}

string util::removeEnd(const string& source, int n) {
	Assert(n > 0);
	if(int(source.size()) <= n) {
		return "";
	} else {
		string result(source);
		result.resize(source.size()-n);
		return result;
	}
}

char util::smartAt(const string &source, int index) {
	if(index >= 0) {
		return source[index];
	} else {
		return source[source.size()+index];
	}
}

bool util::startsWith(const string& source, const string &prefix) {
	if(prefix.size() > source.size()) {
		return false;
	} else if(prefix.size() == source.size()) {
		return prefix == source;
	}

	loop(i, prefix.size())
		if(source[i] != prefix[i]) return false;

	return true;
}

bool util::endsWith(const string& source, const string& suffix) {
	if(suffix.size() > source.size()) {
		return false;
	} else if(suffix.size() == source.size()) {
		return suffix == source;
	}

	loop(i, suffix.size()) {
		int x = -(i+1);
		if(smartAt(source, x) != smartAt(suffix, x)) return false;
	}

	return true;
}

bool util::contains(const string &haystack, const char needle) {
	return (haystack.find(needle) != string::npos);
}
bool util::contains(const string &haystack, const string& needle) {
	return (haystack.find(needle) != string::npos);
}

std::vector<string> util::split(const string &src, const char delimiter) {
	std::vector<string> result;
	
	string temp = src;
	while(contains(temp, delimiter)) {
		result.push_back(before(temp, delimiter));
		temp = after(temp, delimiter);
	}
	result.push_back(temp);
	
	return result;
}

string util::before(const string &src, const char delimiter) {
	size_t pos = src.find(delimiter);
	return (pos == string::npos) ? src : src.substr(0,pos);
}

string util::after(const string &src, const char delimiter) {
	size_t pos = src.find(delimiter);
	return (pos == string::npos) ? "" : src.substr(pos+1);
}

string util::beforeLast(const string &src, const char delimiter) {
	size_t pos = src.rfind(delimiter);
	return (pos == string::npos) ? src : src.substr(0,pos);
}

string util::afterLast(const string &src, const char delimiter) {
	size_t pos = src.rfind(delimiter);
	return (pos == string::npos) ? "" : src.substr(pos+1);
}

string util::join(const vector<string> &src, const char delimiter) {
	string result;

	uloop(i, src.size()) {
		if(i != 0)
			result += delimiter;
		result += src[i];
	}

	return result;
}

