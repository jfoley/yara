
#ifndef __COMPARE_H
#define __COMPARE_H

/*
 *  This caused issues with templated types
 *  ComparableTo(MyClass<ABC,DEF>)
 */
 
#define ComparableTo(T) \
	bool operator< (const T& _t_) const { return CompareTo(_t_)< 0; } \
	bool operator<=(const T& _t_) const { return CompareTo(_t_)<=0; } \
	bool operator==(const T& _t_) const { return CompareTo(_t_)==0; } \
	bool operator!=(const T& _t_) const { return CompareTo(_t_)!=0; } \
	bool operator> (const T& _t_) const { return CompareTo(_t_)> 0; } \
	bool operator>=(const T& _t_) const { return CompareTo(_t_)>=0; } 


	/*
template <class T, class U> bool operator< (const T& t, const U& u) { return t.CompareTo(u)< 0; }
template <class T, class U> bool operator<=(const T& t, const U& u) { return t.CompareTo(u)<=0; }
template <class T, class U> bool operator==(const T& t, const U& u) { return t.CompareTo(u)==0; }
template <class T, class U> bool operator!=(const T& t, const U& u) { return t.CompareTo(u)!=0; }
template <class T, class U> bool operator> (const T& t, const U& u) { return t.CompareTo(u)> 0; }
template <class T, class U> bool operator>=(const T& t, const U& u) { return t.CompareTo(u)>=0; }
*/

/* Example Usage:
class MyClass{
	public:
	ComparableTo(MyClass);
	i8 CompareTo(const MyClass& other) const {
		if( this < other )
			return -43;
		if( this = other )
			return  0;
		if( this > other )
			return  7;
	}
}
*/



#endif

