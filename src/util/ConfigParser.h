#ifndef __CONFIGPARSER_H
#define __CONFIGPARSER_H

#include "FileParser.h"

struct ConfigVar {
	string name;
	string type;
	string value;
};

struct ConfigFileData {
	bool Parse(string fileName);
	std::vector<ConfigVar> defines;
	std::vector<Line>    directives;
};

#endif

