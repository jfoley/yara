#include "StoredMesh.h"
#include "Assert.h"

void StoredMesh::Reset(u32 numVertices, u32 numIndices) {
	nVertices = numVertices;
	nIndices  = numIndices;
	vertices.clear();
	normals.clear();
	colors.clear();
	indices.clear();
	texcoords1.clear();
	texcoords2.clear();
	texcoords3.clear();
}

#define PTR_IF_SIZE(vector) \
	((vector).size() ? &(vector)[0] : 0)

void StoredMesh::Finalize(GLenum primitiveType) {

	nVertices=vertices.size();
	nIndices=indices.size();

	Assert(normals.size()   ==0 || normals.size()   ==nVertices);
	Assert(colors.size()    ==0 || colors.size()    ==nVertices);
	Assert(texcoords1.size()==0 || texcoords1.size()==nVertices);
	Assert(texcoords2.size()==0 || texcoords2.size()==nVertices);
	Assert(texcoords3.size()==0 || texcoords3.size()==nVertices);

/*	
	show(nIndices);
	show(nVertices);
	*/
	m = Mesh(primitiveType);

	m.nIndices = nIndices;
	m.nVertices = nVertices;

	m.indices  = PTR_IF_SIZE(indices);
	m.vertices = PTR_IF_SIZE(vertices);
	m.normals  = PTR_IF_SIZE(normals);
	m.colors   = PTR_IF_SIZE(colors);
	m.texcoords3 = PTR_IF_SIZE(texcoords3);
	m.texcoords2 = PTR_IF_SIZE(texcoords2);
	m.texcoords1 = PTR_IF_SIZE(texcoords1);

	// vbo.Free(); // Implicit
	vbo.Store(m);
}

