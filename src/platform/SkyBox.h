#ifndef __SKYBOX_H
#define __SKYBOX_H

#include "Texture.h"
#include "Mesh.h"
#include "gob/Camera.h"

class SkyBox: public GameObject {
	public:
		SkyBox(string path, real size=2000, bool bottom=false);
		void Load(string path);
		void Draw(const Camera& cam);
		void SetSize(real s) { size = s; }
	private:
		bool bottom;
		real size;
		Texture texture;
		Mesh mesh;
		VBO vbo;
};


#endif

