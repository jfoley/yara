
#include "GLContext.h"
#include "GLContextHandler.h"
#include <cstdio>
#include "macros.h"
#include "concur/concur.h"
#include "concur/Chrono.h"
#include <cmath>
#include <iostream>
#include "Assert.h"

int GLContextHandler::Run(int& argc, char* argv[]){

	if(!Init(argc,argv)){
		fprintf(stderr, "Game initialization failed.\n");
		return -1;
	}

	// Initialize SDL
	//   on my system, without SDL_INIT_JOYSTICK, key events are not always generated...?
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK)<0){
		fprintf(stderr, "Video initialization failed: %s\n", SDL_GetError());
		return -1;
	}
	//IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF);

	const SDL_VideoInfo* info = SDL_GetVideoInfo();
	bpp = info->vfmt->BitsPerPixel;

	// minimum values
	SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 5 );
	SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 5 );
	SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 5 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 16 );
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
		
	SDL_ShowCursor(SDL_DISABLE);

	GLLoop();
	SDL_Quit();

	return 0;
}

#define BlockTimer(stopwatch) for(int __i=0*stopwatch.Start(); __i++==0; stopwatch.Stop())

#define InvertedBlockTimer(stopwatch) for(int __i=0*stopwatch.Stop(); __i++==0; stopwatch.Start())

void GLContextHandler::GLLoop(){
	static Stopwatch framestimer;
	static Stopwatch drawtimer;

	BeforeLoop();
	ComputeFPS();
	fps_actual=fps_preferred;
	fps_correction=1;

	while(gfxrunning){
		if(display) {
			BlockTimer(drawtimer){
				once_every(10)
					ComputeFPS();
				HandleEvents();
				Redraw();
				framecount++;
				SDL_GL_SwapBuffers();
			}
		}
		real naptime=fps_correction/fps_preferred-drawtimer.Duration();
		Nap(naptime);
	}
	AfterLoop();
}

void GLContextHandler::ComputeFPS(){
	static u64 lastfc=framecount;
	static Stopwatch sw;
	InvertedBlockTimer(sw){
		fps_actual=(framecount-lastfc)/sw.Duration();
		lastfc=framecount;
		fps_correction*=fps_actual/fps_preferred;
		fps_correction=sqrt(fps_correction);
	}
}


void GLContextHandler::HandleEvents() {
	SDL_Event event;
	while(SDL_PollEvent(&event)!=0){
		switch(event.type){
			case (SDL_QUIT){
				Quit();
				break;
			}
			case (SDL_KEYDOWN){
				KeyDn(event.key.keysym.sym);
				break;
			}
			case (SDL_KEYUP){
				KeyUp(event.key.keysym.sym);
				break;
			}
			case (SDL_MOUSEMOTION){
				MouseMv(event.motion.x, event.motion.y);
				break;
			}
			case (SDL_MOUSEBUTTONDOWN){
				MouseDn(event.button.button, event.button.x, event.button.y);
				break;
			}
			case (SDL_MOUSEBUTTONUP){
				MouseDn(event.button.button, event.button.x, event.button.y);
				break;
			}
			case (SDL_VIDEORESIZE){
				SetWindow(event.resize.w,event.resize.h);
				break;
			}
		}
	}
}

bool GLContextHandler::SetWindow(int w, int h){
	width=w;
	height=h;
	
	display = SDL_SetVideoMode(width, height, bpp, flags);
	if(display==0){
		fprintf(stderr, "Video mode set failed: %s\n", SDL_GetError());
		return false;
	}
	glewInit();
	Resize(w,h);
	return true;
}

bool GLContextHandler::SetWindow(int w, int h, bool fullscreen){
	flags = SDL_OPENGL | SDL_RESIZABLE;
	if(fullscreen)
		flags|=SDL_FULLSCREEN;
	
	return SetWindow(w,h);
}

void GLContextHandler::SetCaption(string s){
	SDL_WM_SetCaption(s.c_str(), s.c_str());
}


bool GLContextHandler::GetFullScreen() const {
	return flags & SDL_FULLSCREEN;
}


int GLContextHandler::GetCurrentScreenWidth() const {
	return SDL_GetVideoInfo()->current_w;
}

int GLContextHandler::GetCurrentScreenHeight() const {
	return SDL_GetVideoInfo()->current_h;
}

