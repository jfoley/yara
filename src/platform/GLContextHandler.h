
#ifndef __GLCONTEXTHANDLER_H
#define __GLCONTEXTHANDLER_H

#include "concur/concur.h"
#include "types.h"
#include "keys.h"

#include "GLContext.h"

class GLContextHandler{
	public:
		// Makes all the calls to the following methods
		int Run(int& argc, char* argv[]);

		// Called immeditately after instantiation
		virtual bool Init(int& argc, char* argv[]) { return true; }
		// For Window Title
		virtual string GetName() const { return "Untitled"; }
		// Cleanup
		virtual ~GLContextHandler() {}

		//////////////////////////////////////////////////////
		// NOTE: All methods below are called *ONLY* when it is safe to make GL calls.

		volatile real fps_actual;
		volatile real fps_preferred;
		volatile bool gfxrunning;
		volatile u64 framecount;

		virtual void BeforeLoop() {}
		virtual void Redraw() {}
		virtual void AfterLoop() {}

		// Event handlers (called only between calls to Loop)
		// Blocking in these is not recommended.
		virtual void Quit    () {}
		virtual void KeyDn   (KeyCode k) {}
		virtual void KeyUp   (KeyCode k) {}
		virtual void MouseDn (int btn, int x, int y) {}
		virtual void MouseUp (int btn, int x, int y) {}
		virtual void MouseMv (int x, int y) {}
		virtual void Resize  (int w, int h) {}

		// Available for GLContextHandler derivatives
		bool SetWindow(int w, int h);
		bool SetWindow(int w, int h, bool fullscreen);
		void SetCaption(string s);
		void ToggleFullScreen() { SetWindow(width,height,!GetFullScreen()); }

		int GetWidth() const { return width; }
		int GetHeight() const { return height; }

		// Get Size of Current Display
		int GetCurrentScreenWidth() const;
		int GetCurrentScreenHeight() const;

		bool GetFullScreen() const;

	private:
		void GLLoop();
		void HandleEvents();
		void ComputeFPS();
		
		
		int width, height, bpp, flags;
		real fps_correction;

		Display_t display;
};


#endif
