
#ifndef __TESTGAME_H
#define __TESTGAME_H

#include "platform/platform.h"
#include "platform/GLContextHandler.h"


class TestGame: public GLContextHandler{
	public:
		void Resize(int w, int h);

		void BeforeLoop();
		void AfterLoop();
		void Redraw();

		string GetName() const { return "TestGame"; }

		void Quit(){ gfxrunning=false; }
		void KeyDn(KeyCode k);
		void KeyUp(KeyCode k);
};


#endif
