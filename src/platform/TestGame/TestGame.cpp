
#include "TestGame.h"
GAME(TestGame);

#include "math/seq.h"
#include "gob/GameObject.h"
#include "gob/Grid.h"
#include "gob/Cube.h"
#include "platform/GLContext.h"
#include "platform/Mesh.h"
#include "platform/OBJModel.h"
#include "platform/Texture.h"
#include "platform/StoredMesh.h"
#include "math/Matrix.h"

#include "gob/SpatialHash.h"
#include "input/Joystick.h"
#include "input/YaraDevice.h"

#include "physics/PhysicsLoop.h"
#include "physics/Collision.h"
#include "physics/Linkage.h"
#include "physics/SpringLinkage.h"

namespace {
	bool keys[0x10000];
	bool GetKey(KeyCode k){ return keys[k]; }

	YaraDevice picio;
	InputDeviceUpdater picioupdater;

}

void TestGame::KeyDn(KeyCode key){ 
	if(key==KEY_ESCAPE)
		Quit();
	if(key==KEY_F4)
		ToggleFullScreen();
	if(key==KEY_P)
		picio.Close();
	keys[key]=true;
}

void TestGame::KeyUp(KeyCode key){ 
	keys[key]=false;
}




namespace{
	PhysicsLoop physics;

	vector<GameObject> S;
	vector<Linkage*> L;

	bool SamplePhyslet(real dt, void* user=0){
		loop(i, S.size()){
			S[i].PhysUpdate(dt);
			S[i].acc=AXIS_Z*-9.8;
			for(unsigned j=i+1; j<S.size(); j++)
				HandleCollision(S[i], S[j], dt);
		}

		for(unsigned i=0; i<L.size(); i++)
			L[i]->Apply();

		return true;
	}

}

void TestGame::BeforeLoop(){
	int argc=0;
	glutInit(&argc, 0);
	loop(i, lengthof(keys))
		keys[i]=false;

	gfxrunning=true;
	fps_preferred=30.0f;
	SetWindow(800,600,false);

	picio.calibration=Calibration(-0x200, 0x200, -1, 1);
	picioupdater=InputDeviceUpdater(&picio);
	picioupdater.Start();

	physics.Start();
}

void TestGame::AfterLoop(){
	physics.StopAndJoin();
	picioupdater.StopAndJoin();
	picio.Close();
}


void TestGame::Resize( int width, int height ){
	float ratio = (float) width / (float) height;
	glShadeModel(GL_SMOOTH);
//	glCullFace(GL_BACK);
//	glFrontFace(GL_CCW);
//	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_SMOOTH);

	glClearColor(0, 0, 0, 0);

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective( 60.0, ratio, 1.0, 1024.0 * 2);
}



static Camera cam;

void TestGame::Redraw(){
	GLContext::Color(Crayon::Black);
	cam.Clear();
	cam.Look();


	GLContext::Light light0;
	light0.diffuse=light0.ambient=color(.5,.5,.5,1);
	light0.pos=v3r(-10,20,20);

	GLContext::Material mat0;
	mat0.ambient=
	mat0.diffuse=color(.5,.5,.5,1);
	mat0.specular=Crayon::White;
	mat0.shininess=0.5;

	light0.Apply();
	mat0.Apply();



	static real fuck=0;
	fuck+=.01;
	fuck=PI/3;

	cam.pos=v3r(cos(fuck),sin(fuck),.2)*100;
	cam.dir=Orientation(-cam.pos, AXIS_Z);

	once(){
		physics.AddPhyslet(SamplePhyslet);
		const unsigned N=6;
		loop(i, N){
			GameObject G;
			real p=i*2*PI/N;
			G.radius=1;//(real(r)+.1)/2;
			G.mass=pow(G.radius,3);
			G.pos=v3r(cos(p), sin(p))*10;
			if(i==4)
				G.pos=AXIS_Z*5;
			G.pos+=AXIS_Z*20+i;
			G.stiffness=.9;

			S.push_back(G);
		}
	}

	GameObject& G=S[5];

	G.radius=1000;
	G.mass=G.radius;
	G.pos=v3r(0,0,-G.radius);
	G.vel=v3r(0,0,0);
	G.acc=v3r(0,0,0);


	once(){
		for(int i=0; i<4; i++){
			SpringLinkage* l=new SpringLinkage;

			l->gob[0]=&S[(i+0)%4];
			l->gob[1]=&S[(i+1)%4];

			l->x0=20;
			l->k=100;
			l->c=10;

			L.push_back(l);
		}

		for(int i=0; i<2; i++){
			SpringLinkage* l=new SpringLinkage;

			l->gob[0]=&S[(i+0)%4];
			l->gob[1]=&S[(i+2)%4];

			l->x0=20*sqrt(2);
			l->k=100;
			l->c=10;

			L.push_back(l);
		}

		for(int i=0; i<4; i++){
			SpringLinkage* l=new SpringLinkage;

			l->gob[0]=&S[i];
			l->gob[1]=&S[4];

			l->x0=18;
			l->k=100;
			l->c=3;

			L.push_back(l);
		}

	}

	GLContext::Color(Crayon::Blue);
	loop(i, L.size()){
		L[i]->Draw(cam);
	}

	loop(i, S.size()){
		real a=i*2*PI/S.size();

		GLContext::Color(
				(1+cos(a+0*PI/3))/2,
				(1+cos(a+2*PI/3))/2,
				(1+cos(a+4*PI/3))/2,
				1
				
				);
		if(i==int(S.size()-1)){
			GLContext::Color(Crayon::White);
			GLCapability(GL_LIGHTING, GL_LIGHT0){
				S[i].Draw(cam);
			}
		}
		else{
			S[i].Draw(cam);
		}
	}

	return;


//	UpdateLasers();
	once_every(90){
		printf("fps=%f, %3.2f %% err\n", fps_actual, (fps_preferred-fps_actual)/fps_preferred*100);
	}
}

