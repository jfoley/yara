
#ifndef __OBJ_PARSE_H
#define __OBJ_PARSE_H

#include "types.h"
#include "math/seq.h"
#include <fstream>
#include <vector>
#include "platform/Mesh.h"


class OBJModel{
	public:
		struct PolyGroup{
			GLuint mode;
			string name;
			std::vector<v3r> v;
			std::vector<v2r> t;
			std::vector<v3r> n;

			const Mesh* GetMesh() const;
			void SlowDraw() const;
		};

		OBJModel(){}
		OBJModel(string filename){ Load(filename); }
		void Load(string filename){ LoadOBJModel(this, filename); }
		void SlowDraw() const;

		// These methods will probably never be needed externally
		std::vector<PolyGroup> polygroups;
		unsigned NGroups() const { return polygroups.size(); }
		const PolyGroup& operator[] (unsigned i) const { return polygroups[i]; }
		      PolyGroup& operator[] (unsigned i)       { return polygroups[i]; }

	private:
		static void LoadOBJModel(OBJModel* groupsresult, std::istream& f, std::ostream& log=std::cnull);
		static void LoadOBJModel(OBJModel* objmodel, string filename, std::ostream& log=std::cnull);
};




#endif
