
#include "GLContext.h"

#ifdef REAL_IS_DOUBLE
#	define GL_FUNC(fn,sfx) fn##d##sfx
#else
#	define GL_FUNC(fn,sfx) fn##f##sfx
#endif
using namespace GLContext;
namespace GLContext{
	bool PushMatrix  () { glPushMatrix(); return true; }
	bool PopMatrix    () { glPopMatrix(); return false; }
	void LoadIdentity () { glLoadIdentity(); }
	void Translate (const v3r& s)   { GL_FUNC(glTranslate,)(s[0],s[1],s[2]); }
	void Scale     (const v3r& s)   { GL_FUNC(glScale,)(s[0],s[1],s[2]); }
	void Vertex    (const v3r& s)   { GL_FUNC(glVertex3,v)(s.data); }
	void Normal    (const v3r& s)   { GL_FUNC(glNormal3,v)(s.data); }
	void Color     (const color& s) { glColor4fv(s.data); }
	void TexCoord  (const v2r& s)   { GL_FUNC(glTexCoord2,v)(s.data); }

	void Transform(const v3r& pos, const Orientation& dir){
		Transform( seq<4,v4r>(
					v4r(dir.GetRight()),
					v4r(dir.GetUp()),
					v4r(dir.GetForward()),
					v4r(pos)+v4r(0,0,0,1)
					));
	}

	void Transform (const seq<4,v4r>& mat){
#ifdef REAL_IS_DOUBLE
		glMultMatrixd((real*)&mat);
#else
		glMultMatrixf((real*)&mat);
#endif
	}

	void Rotate(real r, const v3r& axis){
#ifdef REAL_IS_DOUBLE
		glRotated(r*180/PI, axis[0],axis[1],axis[2]);
#else
		glRotatef(r*180/PI, axis[0],axis[1],axis[2]);
#endif
	}

	bool Enable(GLenum cap){ glEnable(cap); return true; }
	bool Disable(GLenum cap){ glDisable(cap); return false; }

	bool Enable(const GLenum* cap){
		for(int i=0; cap[i]; i++)
			glEnable(cap[i]);
		return true;
	}
	bool Disable(const GLenum* cap){
		for(int i=0; cap[i]; i++)
			glDisable(cap[i]);
		return false;
	}


	Light::Light(u8 n){
		light=0;
		ambient=diffuse=specular=Crayon::Black;
		pos=v3f(0);
		homo=1;
		spot.direction=v3f(1);
		spot.exponent=0;
		spot.cutoff=PI;
		attenuation=v3f(1);
	}


	void Light::Apply() const {
		v4f p=v4f(pos[0], pos[1], pos[2], homo);

		glLightfv(GL_LIGHT0+light, GL_AMBIENT,               ambient.data);
		glLightfv(GL_LIGHT0+light, GL_DIFFUSE,               diffuse.data);
		glLightfv(GL_LIGHT0+light, GL_SPECULAR,              specular.data);
		glLightfv(GL_LIGHT0+light, GL_POSITION,              p.data);
		glLightfv(GL_LIGHT0+light, GL_SPOT_DIRECTION,        spot.direction.data);


		glLightf (GL_LIGHT0+light, GL_SPOT_EXPONENT,         spot.exponent);

		glLightf (GL_LIGHT0+light, GL_SPOT_CUTOFF,           spot.cutoff*180/PI);

		glLightf (GL_LIGHT0+light, GL_CONSTANT_ATTENUATION,  attenuation[0]);
		glLightf (GL_LIGHT0+light, GL_LINEAR_ATTENUATION,    attenuation[1]);
		glLightf (GL_LIGHT0+light, GL_QUADRATIC_ATTENUATION, attenuation[2]);
	}


	Material::Material(){
		ambient=diffuse=emission=specular=Crayon::Black;
		shininess=0;
	}

	void Material::Apply() const {
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT  , ambient .data);
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE  , diffuse .data);
		glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION , emission.data);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR , specular.data);
		glMaterialf (GL_FRONT_AND_BACK, GL_SHININESS, shininess*128);
	}

	void Fog::Apply() const {
		glFogf (GL_FOG_START,   linStart);
		glFogf (GL_FOG_END,     linEnd);
		glFogf (GL_FOG_DENSITY, expDensity);
		glFogfv(GL_FOG_COLOR,   v4f(fogColor).data);
		glFogi (GL_FOG_MODE,    mode);
		glHint (GL_FOG_HINT,    GL_NICEST); // Who knows what this does?
	}

}




