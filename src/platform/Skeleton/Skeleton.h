
#ifndef __SKELETON_H
#define __SKELETON_H

#include "platform/platform.h"
#include "platform/GLContextHandler.h"

class Skeleton: public GLContextHandler{
	public:
		void Resize(int w, int h);

		void BeforeLoop();

		void AfterLoop();
		void Redraw();

		string GetName() const { return "Skeleton"; }

		void Quit(){ gfxrunning=false; }
		void KeyDn(KeyCode key);
		void KeyUp(KeyCode key);
		bool done;
};


#endif
