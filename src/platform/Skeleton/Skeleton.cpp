
#include "Skeleton.h"
GAME(Skeleton);

#include "gob/Camera.h"

#include <GL/gl.h>
#include <GL/glu.h>

#include <cstdio>

void Skeleton::Resize( int width, int height ){
	float ratio = (float) width / (float) height;
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_SMOOTH);

	glClearColor(0, 0, 0, 0);

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective( 60.0, ratio, 1.0, 1024.0 );
}

void Skeleton::BeforeLoop(){
	gfxrunning=true;
	fps_preferred=30.0f;
	SetWindow(800,450,false);
}


void Skeleton::AfterLoop(){
}

void Skeleton::Redraw(){
	static Camera cam;
	cam.Clear();
	cam.Look();
	once_every(30){
		printf("fps=%f, %3.2f %% err\n", fps_actual, (fps_preferred-fps_actual)/fps_preferred*100);
	}
}



void Skeleton::KeyDn(KeyCode key) { 
	if(key==KEY_ESCAPE)
		Quit();
	if(key==KEY_F4) // F4
		ToggleFullScreen();
}

void Skeleton::KeyUp(KeyCode key) { 
}

