// Mesh.h
// Usage at bottom

#ifndef __MESH_H
#define __MESH_H

#include "platform/GLContext.h"

struct  Mesh;
class   VertexBufferObject;
typedef VertexBufferObject VBO;

struct Mesh {
	Mesh(GLenum _mode=0);

	GLenum mode;
	u32 nVertices;
	u32 nIndices;

	v3r*   vertices;
	v3r*   normals;
	color* colors;
	v1r*   texcoords1;
	v2r*   texcoords2;
	v3r*   texcoords3;
	u32*   indices;

	void SlowDraw() const;
	VertexBufferObject* GetVBO(bool fallback=false) const;
};

std::ostream& operator<< (std::ostream& os, const Mesh& m);

class VertexBufferObject {
	public:
		static bool Test();

		VertexBufferObject(bool fb=false);
		VertexBufferObject(const Mesh& m, bool fb=false);
		~VertexBufferObject();

		bool KeepMem() const { return fallback; }

		void Store(const Mesh& m);
		void Pre();
		void Draw();
		void Post();
		void Free();
	
	private:
		VertexBufferObject(const VertexBufferObject&);
		VertexBufferObject& operator= (const VertexBufferObject&);

		void Init();
		void RawDraw();

		const Mesh* mesh;
		bool stored;
		bool bound;
		bool fallback;

		u32 vCount;
		u32 iCount;
		u32 tSize;
		u32 vSize;
		u32 nSize;
		u32 cSize;
		u32 iSize;
		u32 vOffset;
		u32 nOffset;
		u32 cOffset;
		u32 tOffset;
		u32 iOffset;
		u32 totalSize;

		GLenum mode;
		GLuint vertexbuf;
		GLuint indexbuf;

};

/*******************************
 *  Example usage:             
 *******************************
 * // Prepare Mesh
 * Mesh m(GL_QUADS);
 * m.nVertices  = 8;
 * m.nIndices   = 24;                     [optional]
 * m.vertices   = (v3r*)   vertexarray;
 * m.colors     = (color*) colorarray;    [optional]
 * m.normals    = (v3r*)   normalarray;   [optional]
 * m.texcoords2 = (v2r*)   texcoordarray; [optional]
 * m.indices    = (u16*)   indexarray;    [optional]
 * *****************************
 * 
 * // Draw it using various methods.
 *
 * // Case 1
 * m.SlowDraw();
 * 
 * // Case 2
 * VertexBufferObject* vbo=m.GetVBO();
 * vbo->Draw();
 * delete vbo;
 * 
 * // Case 3
 * VertexBufferObject vbo;
 * vbo.Store(m);
 * vbo.Draw();
 * 
 * // Case 4
 * VertexBufferObject vbo(m);
 * vbo.Draw();
 * 
 * // Case 5 (Fallback to VertexArrays)
 * VertexBufferObject vbo(m, true);
 * VertexBufferObject vbo(true);
 * 
 * // Case 6
 * VertexBufferObject vbo(m);
 * vbo.Pre();
 * loop(){
 * 	vbo.Draw();
 * }
 * vbo.Post();
 *
 *****************************/
#endif
