
#ifndef __PLATFORM_H
#define __PLATFORM_H

#include "GLContextHandler.h"
void AddGame(GLContextHandler* g);
struct GameObj{
	GameObj(GLContextHandler* g){
		AddGame(g);
	}
};

#define GAME(glch) GameObj glch##GameObj(new glch);


#endif

