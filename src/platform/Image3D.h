
#ifndef __IMAGE3D_H
#define __IMAGE3D_H

#include "GLContext.h"
#include "types.h"

struct Image3D {
	virtual ~Image3D() {}

	virtual bool Valid()  const { return false; }
	virtual u16  Width()  const { return 0; }
	virtual u16  Height() const { return 0; }
	virtual u16  Depth()  const { return 0; }
	virtual u16  BPP()    const { return 0; }

	virtual const void*  Data()       const { return 0; }
	virtual u32    DataBytes()  const { return Width()*Height()*Depth()*BPP()/8; }
	virtual GLenum DataType()   const { return GL_UNSIGNED_BYTE; }
	virtual GLenum DataFormat() const {
		switch(BPP()){
			case 32: return GL_RGBA;
			case 24: return GL_RGB;
			default: return 0;
		}
	}

	virtual void Print(std::ostream& os) const {
		os << Width() << "x" << Height() << "x" << Depth() << "x" << BPP();
	}

	friend std::ostream& operator<< (std::ostream& os, const Image3D& img) {
		img.Print(os);
		return os;
	}
};

#endif

