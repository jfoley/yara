#include "SDLImage.h"
#include "util/strings.h"
#include "backend.h"
#include "Assert.h"

#include <algorithm>
using std::swap;

void SDLImage::Init(){
	fname="";
	valid=false;
}

void SDLImage::Free(){
	if(Valid())
		data.clear();
	Init();
}

void SDLImage::perror(string err) const { std::cerr << (START_BLUE "SDLImage(\"" + fname + "\") " START_RED)    << err << (END_COLOR "\n"); }
void SDLImage::pwarn(string err)  const { std::cerr << (START_BLUE "SDLImage(\"" + fname + "\") " START_YELLOW) << err << (END_COLOR "\n"); }

static bool isBinaryPower(u16 n){
	u16 ref=1;
	while(ref){
		if(ref==n)
			return true;
		ref<<=1;
	}
	return false;
}

SDLImage& SDLImage::operator= (const SDLImage& sdl){
	if(&sdl==this)
		return *this;
	
	Free();
	if(!sdl.Valid())
		return *this;

	fname=sdl.fname;
	data.clear();
	data=sdl.data;

	return *this;
}

void SDLImage::Print(std::ostream& os) const{
	os << "SDLImage(\"" + FileName() + "\"): ";
	Image3D::Print(os);
}

SDLImage::~SDLImage(){
	if(Valid()){
		data.clear();
		Init();
	}
}

bool SDLImage::FromSurface(SDL_Surface *surface) {
	//--- query SDL format info
	SDL_PixelFormat *format = surface->format;
	//show(int(format->BitsPerPixel));
	//show(int(format->BytesPerPixel));
	//show(format->Rmask);
	//show(format->Gmask);
	//show(format->Bmask);
	//show(format->Amask);

	if(!(format->BitsPerPixel == 24 || format->BitsPerPixel == 32)) {
		cout << "SDLImage::FromSurface does not support " << format->BitsPerPixel << "\n";
		return false;
	}

	if(format->Amask)
		bpp = 32;
	else
		bpp = 24;

	width = surface->w;
	height = surface->h;

	//show(width);
	//show(height);
	//show(Width());
	//show(Height());
	//show(Depth());
	//show(this->BPP());

	SDL_LockSurface(surface);
	u8 *byte = (u8*) surface->pixels;
	data.reserve(width*height*format->BitsPerPixel/8);
	uloop(i, width*height*format->BitsPerPixel/8) {
		data.push_back(byte[i]);
	}
	SDL_UnlockSurface(surface);
	SDL_FreeSurface(surface);

	if(!isBinaryPower(Width()) || !isBinaryPower(Height())){
		pwarn(util::stringf("Image dimentions are not powers of 2: %dx%d", Width(), Height()));
	}
	/*if(Width()!=Height()){
		pwarn(util::stringf("Image dimentions are not equal: %dx%d", Width(), Height()));
	}*/

	valid=true;
	return true;
}

bool SDLImage::Load(string filename){
	if(filename=="")
		return false;
	Free();

	fname=filename;

	SDL_Surface *surface = IMG_Load(fname.c_str());
	if(!surface) {
		perror("Couldn't load file with SDL_Image");
		return false;
	}

	return FromSurface(surface);
}

bool SDLImage::Load(const void *ptr, u32 size, const char *extension) {
	SDL_RWops *memfile = SDL_RWFromConstMem(ptr, size);
	//show(extension);
	
	SDL_Surface *surface = IMG_LoadTyped_RW(memfile, false, (char*) extension);
	if(!surface) {
		cerr << "Failed to load texture from data in memory\n";
		return false;
	}

	bool result = FromSurface(surface);
	SDL_FreeRW(memfile);
	return result;
}

