
#include "Intel.h"
GAME(Intel);

#include "math/seq.h"
#include "gob/GameObject.h"
#include "gob/Grid.h"
#include "gob/Cube.h"
#include "platform/GLContext.h"
#include "platform/Mesh.h"
#include "platform/OBJModel.h"
#include "platform/Texture.h"
#include "platform/StoredMesh.h"
#include "platform/SkyBox.h"
#include "input/YaraDevice.h"

namespace {
bool useYara=true;

}

void VBOTest();

class SpaceShip: public GameObject{
	static OBJModel model;
	public:
	SpaceShip(){
		once(){
			model.Load("res/gfx/models/enemy1.obj");
		}
		nextRing=0;
	}
	virtual void Draw(const Camera& cam){
		GLSaveStack(){
			using namespace GLContext;
			Transform(pos,dir);
			Transform(v3r(), Orientation(AXIS_X, AXIS_Y)); // Kludge to reorient ship
			model.SlowDraw();
		}
	}


	u32 nextRing;
};
OBJModel SpaceShip::model;



/*
class Laser: public GameObject{
	public:
		
		void Draw(const Camera& cam);

		void PhysUpdate(real dt);
		bool Alive() const { return ttl>0; }
		real ttl;
		real velocity;
};

void Laser::PhysUpdate(real dt){
	real displace=velocity*dt;
	ttl-=displace;
	pos+=dir.GetForward() * displace;
}

void Laser::Draw(const Camera& cam){
	static v3r v[]={
		v3r( 0,  0,  0), // 0 Tip
		v3r(-1,-.1, .1), // 1
		v3r(-1,-.1,-.1), // 2
		v3r(-1, .1,-.1), // 3
		v3r(-1, .1, .1), // 4
		v3r(-4,  0,  0), // 5 Tail
	};
	static color c[]={
		color(1,0,0,1),
		color(1,.1,0,1),
		color(1,.1,0,1),
		color(1,.1,0,1),
		color(1,.1,0,1),
		color(1,.3,0,1),
	};
	static u32 i[]={
		0,1,2,
		0,2,3,
		0,3,4,
		0,4,1,
		5,1,2,
		5,2,3,
		5,3,4,
		5,4,1,
	};
	static VBO beam;
	once(){
		static Mesh mesh(GL_TRIANGLES);
		mesh.nVertices=lengthof(v);
		mesh.nIndices=lengthof(i);
		mesh.vertices=v;
		mesh.colors=c;
		mesh.indices=i;
		beam.Store(mesh);
	}

	GLSaveStack(){
		GLContext::Transform(pos, dir);
		beam.Draw();
	}
}

*/
/*
static vector<Laser*> lasers;

void FireLaser(){
	loop(i,2){
		Laser* laser=new Laser;
		laser->pos=cam.pos - cam.dir.GetUp() + cam.dir.GetRight() * (i==0 ? 1 : -1);
		laser->dir=cam.dir;
		laser->ttl=1000;
		laser->velocity=50;
		lasers.push_back(laser);
	}
}

void UpdateLasers(){
	loop(i, lasers.size()){
		lasers[i]->PhysUpdate(.1);
		lasers[i]->Draw(cam);
	}
	loop(i, lasers.size()){
		if(!lasers[i]->Alive()){
			delete lasers[i];
			lasers.erase(lasers.begin()+i);
			i=0;
		}
	}
}
*/




void Intel::BeforeLoop(){

	loop(i, lengthof(keys))
		keys[i]=false;

	gfxrunning=true;
	fps_preferred=30.0f;
	SetWindow(1024*3/4,600*3/4,false);

	picio.calibration=Calibration(-0x200, 0x200, -1, 1);
	picioupdater=InputDeviceUpdater(&picio);
	picioupdater.Start();


if(!picio.Valid())
printf("NO YARA DEVICE\n");
picio.calibration=Calibration(0,1024,-1,1);

}

void Intel::AfterLoop(){
	picioupdater.StopAndJoin();
	picio.Close();
}


void Intel::Resize( int width, int height ){
	float ratio = (float) width / (float) height;
	glShadeModel(GL_SMOOTH);
//	glCullFace(GL_BACK);
//	glFrontFace(GL_CCW);
//	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_SMOOTH);

	glClearColor(0, 0, 0, 0);

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective( 60.0, ratio, 1.0, 1024.0 * 2);
}

#include "gob/Rod.h"



class HomoToroid: public GameObject {

	static StoredMesh* bright;
	static StoredMesh* dark;

	static StoredMesh* CreateToroid(real R, real r, int nSectors=20, int nSegments=3, bool dim=false){
		StoredMesh* result=new StoredMesh;

		real du=2*PI/nSectors;
		real dv=2*PI/nSegments;
		loop(i, nSectors){
			loop(j, nSegments){
				loop(k, 4){
					real u=(i + ((k == 1 || k == 2) ? 1 : 0 ))*du;
					real v=(j + ((k == 2 || k == 3) ? 1 : 0 ))*dv;
					result->normals.push_back(v3r(
								cos(v)*cos(u),
								cos(v)*sin(u),
								sin(v)
								));

					result->colors.push_back(Crayon::Opaque(color(
									(cos(u+v + 2*PI/3*0)+1)/2,
									(cos(u+v + 2*PI/3*1)+1)/2,
									(cos(u+v + 2*PI/3*2)+1)/2)/(dim ? 4 : 1))
							);
					result->vertices.push_back(v3r(
								(R+r*cos(v))*cos(u),
								(R+r*cos(v))*sin(u),
								r*sin(v)
								));
				}
			}
		}

		result->Finalize(GL_QUADS);


		return result;
	}


	public:
	bool dim;

		HomoToroid(){
			if(!bright)
				bright=CreateToroid(20, 1, 20, 3);
			if(!dark)
				dark=CreateToroid(20, 1, 20, 3, true);

			dim=false;
		}

		virtual void Draw(const Camera& cam){
			using namespace GLContext;
			GLSaveStack(){
				Transform(pos, dir);
				(dim ? dark : bright) -> vbo.Draw();
			}
		}

		real FindIntersection(const v3r& A, const v3r& B) const {
			v3r N=dir.GetForward();
			real t=v3r::DotProduct(N, pos-A) / v3r::DotProduct(N, B-A);
			return t;
		}
		v3r FindIntersectionPt(const v3r& A, const v3r& B) const {
			return A + FindIntersection(A,B)*(B-A);
		}
		real FindIntersectionDist(const v3r& A, const v3r& B) const {
			real t=FindIntersection(A,B);
			if(t<0 || t>1)
				return -1;
			return ( (A+(B-A)*t) - pos).MagnitudeSquared();
		}
};
StoredMesh* HomoToroid::dark=0;
StoredMesh* HomoToroid::bright=0;


typedef v3r (*Curve)(real t);


namespace {
real rrr(real t){
	return t*t*(1+2*t);
}

v3r Track(real t){
	real R=400;
	real a=cos(8*PI*t);
	real T=GetTime()/25;
	return v3r(
			R*(a)*cos(t*2*PI+T),
			R*(a)*sin(t*2*PI+T),
			R*rrr(t)*rrr(1-t)*sin(t*2*PI+T/3)*8
			);
}

v3r CurveTangent(Curve f, real t){
	real dt=1e-3;
	return (f(t+dt)-f(t-dt));
}

real ArcLength(Curve f){
	real nSeg=1000;
	real s=0;
	loop(i, nSeg){
		real t=real(i)/nSeg;
		real dt=1.0/nSeg;
		s+=(f(t+dt)-f(t)).Magnitude();
	}
	return s;
}


void DrawCurve(Curve f, u32 nSegments, real radius){
	real dt=1.0/nSegments;
	loop(i, nSegments+1){
		real t=dt*i;
		DrawRod(f(t), f(t+dt), radius);
	}
}
}
namespace{
	Camera cam;
	Curve track;
	vector<HomoToroid> ring;
	vector<SpaceShip> player;


	void CreateTrack(Curve f, int nRings){
		track=f;
		ring.resize(0);
		loop(i, nRings)
			ring.push_back(HomoToroid());
	}

	void DrawTrack(){
		DrawCurve(track, 1000, .2);
	}
	void DrawRings(){
		using namespace GLContext;

		loop(i,ring.size()){
			real t=real(i)/ring.size();
			v3r pos=Track(t);
			v3r tgt=CurveTangent(Track, t);

			HomoToroid& x=ring[i];
			x.pos=pos;
			x.dir=Orientation(tgt, AXIS_Z);
			x.Draw(cam);
		}

	}
}


void Follow(const GameObject& leader, GameObject& other, real dist, real rate){
	other.acc=leader.pos-leader.dir.F()*dist-other.pos;
	other.pos=LERP(other.pos, leader.pos-leader.dir.F()*dist, rate);
	other.PhysUpdate(.01);

	other.dir.Set(
				LERP((other.dir.F()), (leader.pos-other.pos), rate/10),
				LERP((other.dir.U()), (leader.dir.U()), rate)
			);
}

void DrawInputBar(v2r offset, real value){
				using namespace GLContext;
	GLContext::Color(Crayon::Yellow);
	GLPrimitive(GL_QUADS){
		loop(i,4){
			Vertex(cam.pos + cam.dir.F()*2 + cam.dir.U()*((i==2 || i==3)/6.0+offset[1]) + cam.dir.R()*((i==1 || i==2)*value + offset[0]));
		}
	}
}

void Intel::Redraw(){


	GLContext::Color(Crayon::Black);
	cam.Clear();

using namespace GLContext;

cam.Look();
loop(i,4){
DrawInputBar(v2r(1,.2*(i+1)),Calibration(-1,1,1,0)((i==1 ? 1 : -1)*picio.GetCalibratedAnalog(i)));
}





	//static SkyBox sb;
//	sb.Draw(cam);


	GLContext::Light light0;
	light0.diffuse=light0.ambient=color(.5,.5,.5,1);
	light0.pos=v3r(-10,20,20);

	GLContext::Material mat0;
	mat0.ambient=
	mat0.diffuse=color(.5,.5,.5,1);
	mat0.specular=Crayon::White;
	mat0.shininess=0.5;

	light0.Apply();
	mat0.Apply();

	once_every(90){
		cout << "Track Length: " << ArcLength(Track) << "\n";
	}


	once(){
		CreateTrack(Track, 100);

		int N=30;
		loop(i, N){
			player.push_back(SpaceShip());
			real s=i/(N+0.0);
			player[i].pos=Track(s);
			player[i].dir=Orientation(CurveTangent(Track, s), AXIS_Z);
			player[i].nextRing=s*100+1;;
		}
	}


	loop(i, ring.size()){
		int j=i-player[0].nextRing;
		bool dim=j<0 || j>=1;
		ring[i].dim=dim;
	}

	GLContext::Color(Crayon::Red);
	DrawTrack();
	DrawRings();


real ThrottleLeft        ; 
real ThrottleRight       ; 
real Velocity            ; 
real Yaw                 ; 
real ThrottleDifferential; 
real ThrottleCommon      ; 


if(picio.Valid() && useYara){
	ThrottleLeft         = picio.GetCalibratedAnalog(0)*10;
	ThrottleRight        = -picio.GetCalibratedAnalog(1)*10;
	Velocity             = ((picio.GetCalibratedAnalog(3)/.8+1)/2 + (GetKey(KEY_T)-GetKey(KEY_G)) + .4)/2.0;

//	Velocity             = ((GetKey(KEY_T)-GetKey(KEY_G)) + .4);
	Yaw                  = -picio.GetCalibratedAnalog(2)*30 + (GetKey(KEY_F)-GetKey(KEY_H))*10;
	ThrottleDifferential = (ThrottleRight - ThrottleLeft)/2*3 + (GetKey(KEY_D) - GetKey(KEY_A))*10;
	ThrottleCommon       = (ThrottleRight + ThrottleLeft)/2*3 + (GetKey(KEY_W) - GetKey(KEY_S))*10;
}
else{
	ThrottleLeft         = 0; //picio.GetCalibratedAnalog(0)*10;
	ThrottleRight        = 0; //-picio.GetCalibratedAnalog(1)*10;
	Velocity             = ((GetKey(KEY_T)-GetKey(KEY_G)) + .4);
	Yaw                  = (GetKey(KEY_F)-GetKey(KEY_H))*10;
	ThrottleDifferential = (GetKey(KEY_D) - GetKey(KEY_A))*10;
	ThrottleCommon       = (GetKey(KEY_W) - GetKey(KEY_S))*10;
}


	//static real nextVelocity=.2;
//	v3r lastpos=player.pos;

//	nextVelocity=LERP(nextVelocity, .2, .01);

	loop(i, player.size()){

		if(!GetKey(KEY_R)){
			if(i==0){
				player[i].dir.Increment(
						-Yaw, // dYaw
						-ThrottleCommon, // dPitch
						ThrottleDifferential, // dRoll
						Velocity/50 // dR
						);
			}
			else
				Velocity=.4;

			player[i].vel=Velocity*player[i].dir.GetForward();


			real dist=ring[player[i].nextRing].FindIntersectionDist(player[i].pos, player[i].pos+player[i].vel*10);
			if(dist>=0){
				player[i].nextRing++;
				player[i].nextRing%=ring.size();
//				nextVelocity=MAX(MIN(20/sqrt(dist+1), .5), .2);

				player[i].dir.Set(LERP((ring[player[i].nextRing].pos-player[i].pos), player[i].dir.GetForward(), .5), player[i].dir.GetUp());
//				show(nextVelocity);

			}
else
				player[i].dir.Set(LERP((ring[player[i].nextRing].pos-player[i].pos), player[i].dir.GetForward(), .999), player[i].dir.GetUp());




			player[i].PhysUpdate(10);
		}

	}




	/*
	real dist=ring[player.nextRing].FindIntersectionDist(lastpos, player.pos);
	if(dist>=0){
		player.nextRing++;
		player.nextRing%=ring.size();
		nextVelocity=MAX(MIN(20/sqrt(dist+1), .5), .2);

		player.dir.Set(LERP((ring[player.nextRing].pos-player.pos), player.dir.GetForward(), .5), player.dir.GetUp());
		show(dist);
		show(nextVelocity);

	}

	Follow(player,cam, 8, .2);

	*/

	cam.Follow(player[0], 8, .2);
	GLCapability(GL_LIGHTING, GL_LIGHT0){
		loop(i, player.size())
			player[i].Draw(cam);
	}
	
	
//	UpdateLasers();

	once_every(90){
		printf("fps=%f, %3.2f %% err\n", fps_actual, (fps_preferred-fps_actual)/fps_preferred*100);
	}
}





void Intel::KeyDn(KeyCode key){ 
	if(key==KEY_ESCAPE)
		Quit();
	if(key==KEY_F4)
		ToggleFullScreen();
	if(key==KEY_P)
		picio.Close();

	if(key==KEY_SPACE){
		//FireLaser();
	}
if(key==KEY_P)
useYara=!useYara;
	key-=KEY_A;
	if(key<=KEY_Z)
		keys[key]=true;
}

void Intel::KeyUp(KeyCode key){ 
	key-=KEY_A;
	if(key<=KEY_Z)
		keys[key]=false;
}

namespace {
void VBOTest(){
	using namespace GLContext;
	using namespace Crayon;
	/*   v6 +------+ v7
	 *     /|     /|
	 * v2 +------+ v3
	 *   v4 +----|-+ v5
	 *    |/     |/
	 * v0 +------+ v1
	 */

	static v3r v[]={
		v3r(0,0,0), 
		v3r(1,0,0), 
		v3r(0,1,0),
		v3r(1,1,0),
		v3r(0,0,1),
		v3r(1,0,1),
		v3r(0,1,1),
		v3r(1,1,1)
	};

	static color c[]={
		Black,
		Blue,
		Green,
		Cyan,
		Red,
		Magenta,
		Yellow,
		White,
	};

	static color c2[]={
		Black,
		Blue,
		Black,
		Blue,
		Black,
		Blue,
		Black,
		Blue,
	};

	static u32 ii[]={
		0,1,3,2,
		4,5,7,6,
		1,3,7,5,
		0,4,6,2,
		2,3,7,6,
		0,1,5,4,
	};

	static Mesh m(GL_QUADS);
	static Mesh m2(GL_QUADS);

	static real r=0;
	static VBO mycube;


	once() {
		m.nVertices=lengthof(v);
		m.nIndices=lengthof(ii);
		m.vertices=v;
		m.colors=c;
		m.indices=ii;
		m2.nVertices=lengthof(v);
		m2.nIndices=lengthof(ii);
		m2.vertices=v;
		m2.colors=c2;
		m2.indices=ii;
		mycube.Store(m);
	}

	r+=PI/180;

	loop(ii,51){
		int i=ii-25;
		GLSaveStack(){
			Translate(10,i,4);
			Rotate(r*i/2+r,1,i,sin(r));
			mycube.Draw();
			Translate(0,0,i*.2);
			m2.SlowDraw();
		}
	}
}
}
