
#ifndef __TESTGAME_H
#define __TESTGAME_H

#include "platform/platform.h"
#include "platform/GLContextHandler.h"
#include "gob/SpatialHash.h"
#include "input/Joystick.h"
#include "input/YaraDevice.h"

class Intel: public GLContextHandler{
	public:
		void Resize(int w, int h);

		void BeforeLoop();
		void AfterLoop();
		void Redraw();

		string GetName() const { return "Intel Cornell Cup"; }

		void Quit(){ gfxrunning=false; }
		void KeyDn(KeyCode k);
		void KeyUp(KeyCode k);

	private:
	
		YaraDevice picio;
		InputDeviceUpdater picioupdater;

		bool GetKey(KeyCode k){
			k-=KEY_A;
			return k<=KEY_Z ? keys[k] : false;
		}
		bool keys[KEY_Z-KEY_A+1];
};


#endif
