
#include "Texture.h"

void Texture::Init(){
	tex=0;
	width=0;
	height=0;
	stored=false;
	bound=false;
}

Texture::Texture(const Image3D& img){
	Init();
	Store(img);
}

Texture::Texture(u16 w, u16 h, u16 d, const void* data, GLenum format, GLenum dtype){
	Init();
	Store(w,h,d,data,format,dtype);
}

Texture::~Texture(){
	Free();
}

void Texture::Free(){
	if(stored)
		DelTex();
	Init();
}

void Texture::Store(u16 w, u16 h, u16 d, const void* data, GLenum format, GLenum dtype){
	if(!(w && h && d && data))
		return;

	width = w;
	height = h;

	this->~Texture();

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	GenTex();
	BindTex();


	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, w, h, d, 0, format, dtype, data);

	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,  
			GL_MODULATE
			);// GL_DECAL);

	BindTex();

	stored=true;
	bound=true;
}

void Texture::Store(const Image3D& img){
	Store(img.Width(), img.Height(), img.Depth(), img.Data(), img.DataFormat(), img.DataType());
}

void Texture::Pre(){
	if(!stored)
		return;

	glEnable(GL_TEXTURE_3D);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,  
			GL_MODULATE
			);// GL_DECAL);

	BindTex();

	stored=true;
	bound=true;
}

void Texture::Post(){
	if(!stored)
		return;

	bound=false;
	glDisable(GL_TEXTURE_3D);
}

bool Texture::Valid() const {
	return stored;
}

void Texture::GenTex() {
	glGenTextures(1, &tex);
}

void Texture::DelTex() {
	glDeleteTextures(1, &tex);
	tex=0;
}

void Texture::BindTex() {
	glBindTexture(GL_TEXTURE_3D, tex);
}

