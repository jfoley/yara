
#include "Mesh.h"
#include <cstring>
#include "Assert.h"

#ifdef REAL_IS_DOUBLE
#	define GL_REAL GL_DOUBLE
#else
#	define GL_REAL GL_FLOAT
#endif

void glNormalPointer(GLint dummy, GLenum type, GLsizei stride, const GLvoid* pointer){
	glNormalPointer(type,stride,pointer);
}

////////////////////////////////////////////
// Mesh
Mesh::Mesh(GLenum _mode){
	mode=_mode;

	nVertices=0;
	nIndices=0;

	vertices=0;
	normals=0;
	colors=0;

	texcoords1=0;
	texcoords2=0;
	texcoords3=0;

	indices=0;
}

void Mesh::SlowDraw() const {
	VertexBufferObject vbo(*this);
	vbo.Draw();
}

VertexBufferObject* Mesh::GetVBO(bool fallback) const {
	return new VertexBufferObject(*this, fallback);
}

std::ostream& operator<< (std::ostream& os, const Mesh& m){
	os << "{";
	os << "Mode:" << m.mode << ", ";
	os << "nVer:" << m.nVertices << ", ";
	os << "nInd:" << m.nIndices << ", ";
	os << "data:{";
	if(m.vertices  ) os << "v,";
	if(m.normals   ) os << "n,";
	if(m.colors    ) os << "c,";
	if(m.texcoords1) os << "t1,";
	if(m.texcoords2) os << "t2,";
	if(m.texcoords3) os << "t3,";
	if(m.indices   ) os << "i,";
	os << "}}";
	return os;
}

////////////////////////////////////////////
// VertexBufferObject
bool VertexBufferObject::Test(){
	return GLEW_ARB_vertex_buffer_object;
}

void VertexBufferObject::Init(){
	stored=false;
	bound=false;

	iCount=0;
	vCount=0;

	iSize=0;
	vSize=0;
	nSize=0;
	cSize=0;
	tSize=0;

	iOffset=0;
	vOffset=0;
	nOffset=0;
	cOffset=0;
	tOffset=0;
	
	totalSize=0;

	vertexbuf=0;
	indexbuf=0;

	if(!Test())
		fallback=true;

	mesh=0;
}

VertexBufferObject::VertexBufferObject(bool fb) {
	Init();
	fallback=fb;
}

VertexBufferObject::VertexBufferObject(const Mesh& m, bool fb) {
	Init();
	fallback=fb;
	Store(m);
}

VertexBufferObject::~VertexBufferObject() {
	Free();
}


void VertexBufferObject::Store(const Mesh& m){
	if(stored)
		Free();

	bool fb=fallback;
	Init();
	fallback|=fb;

	if(!m.nVertices)
		return;

	mode=m.mode;

	vCount=m.nVertices;
	iCount=m.nIndices;

	if(m.vertices  ) vSize = vCount * sizeof(m.vertices[0]);
	if(m.normals   ) nSize = vCount * sizeof(m.normals [0]);
	if(m.colors    ) cSize = vCount * sizeof(m.colors  [0]);
	if(m.texcoords1) tSize = vCount * sizeof(m.texcoords1[0]);
	if(m.texcoords2) tSize = vCount * sizeof(m.texcoords2[0]);
	if(m.texcoords3) tSize = vCount * sizeof(m.texcoords3[0]);

	vOffset   = 0;
	nOffset   = vSize;
	cOffset   = vSize + nSize;
	tOffset   = vSize + nSize + cSize;
	totalSize = vSize + nSize + cSize + tSize;

	iOffset=0;
	iSize = iCount * sizeof(m.indices[0]);

	if(!fallback){
		// Store vertex data
		glGenBuffersARB(1, &vertexbuf);
		glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertexbuf);
		glBufferDataARB(GL_ARRAY_BUFFER_ARB, totalSize, 0, GL_STATIC_DRAW_ARB);
		glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, vOffset, vSize, m.vertices);
		glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, nOffset, nSize, m.normals);
		glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, cOffset, cSize, m.colors);
		if(m.texcoords1) glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, tOffset, tSize, m.texcoords1);
		if(m.texcoords2) glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, tOffset, tSize, m.texcoords2);
		if(m.texcoords3) glBufferSubDataARB(GL_ARRAY_BUFFER_ARB, tOffset, tSize, m.texcoords3);

		// Store index data
		if(iSize){
			glGenBuffersARB(1, &indexbuf);
			glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, indexbuf);
			glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, iSize, 0, GL_STATIC_DRAW_ARB);
			glBufferSubDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB, iOffset, iSize, m.indices);
		}

		glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
		glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
	}
	else{
		mesh=&m;
	}

	stored=true;
}

void VertexBufferObject::Free(){
	if(stored){
		if(bound)
			Post();
		stored=false;

		if(!fallback){
			if(vertexbuf) glDeleteBuffersARB(1, &vertexbuf);
			if(indexbuf)  glDeleteBuffersARB(1, &indexbuf);
		}
	}

	Init();
}

void VertexBufferObject::Pre(){
	if(!stored)
		return;
	if(!bound){
		bound=true;

		if(!fallback){
			// Bind vertex buffer
			glBindBufferARB(GL_ARRAY_BUFFER_ARB, vertexbuf);
		}

		// Enable use of vertex data
		if(vSize) glEnableClientState(GL_VERTEX_ARRAY);
		if(nSize) glEnableClientState(GL_NORMAL_ARRAY);
		if(cSize) glEnableClientState(GL_COLOR_ARRAY);
		if(tSize) glEnableClientState(GL_TEXTURE_COORD_ARRAY);

		// Indicate data offsets
		if(!fallback){
			if(vSize) glVertexPointer  (vSize/(vCount*sizeof(real)), GL_REAL, 0, (GLvoid*) (ptrdiff_t) vOffset);
			if(nSize) glNormalPointer  (nSize/(vCount*sizeof(real)), GL_REAL, 0, (GLvoid*) (ptrdiff_t) nOffset);
			if(cSize) glColorPointer   (cSize/(vCount*sizeof(float)), GL_FLOAT, 0, (GLvoid*) (ptrdiff_t) cOffset);
			if(tSize) glTexCoordPointer(tSize/(vCount*sizeof(real)), GL_REAL, 0, (GLvoid*) (ptrdiff_t) tOffset);
		}
		else{
			if(vSize) glVertexPointer  (vSize/(vCount*sizeof(real)), GL_REAL, 0, mesh->vertices);
			if(nSize) glNormalPointer  (nSize/(vCount*sizeof(real)), GL_REAL, 0, mesh->normals);
			if(cSize) glColorPointer   (cSize/(vCount*sizeof(float)), GL_FLOAT, 0, mesh->colors);
			if(tSize && mesh->texcoords1) glTexCoordPointer(tSize/(vCount*sizeof(real)), GL_REAL, 0, mesh->texcoords1);
			if(tSize && mesh->texcoords2) glTexCoordPointer(tSize/(vCount*sizeof(real)), GL_REAL, 0, mesh->texcoords2);
			if(tSize && mesh->texcoords3) glTexCoordPointer(tSize/(vCount*sizeof(real)), GL_REAL, 0, mesh->texcoords3);
		}

		// If there is index data, use it
		if(iSize){
			if(!fallback)
				glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, indexbuf);
			glEnableClientState(GL_INDEX_ARRAY);
			if(!fallback) {
				// STUPID? this generates an error and works fine without
				//glIndexPointer(GL_UNSIGNED_INT, 0, 0);
			} else {
				glIndexPointer(GL_UNSIGNED_INT, 0, mesh->indices);
			}
		}
	}
}


void VertexBufferObject::Post(){
	if(bound){
		bound=false;

		// Disable ALL client states
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_INDEX_ARRAY);

		if(!fallback){
			// Unbind buffers
			glBindBufferARB(GL_ARRAY_BUFFER_ARB, 0);
			glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, 0);
		}
	}
}



void VertexBufferObject::Draw(){
	if(bound)
		RawDraw();
	else{
		Pre();
		RawDraw();
		Post();
	}
}

void VertexBufferObject::RawDraw(){
	if(!stored || !bound){
		show(stored);
		show(bound);
		Assert(0);
	}
	if(!iSize)
		glDrawArrays(mode, 0, vCount);
	else
		glDrawElements(mode, iCount, GL_UNSIGNED_INT, fallback ? mesh->indices : 0);
}
