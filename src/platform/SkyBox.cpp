#include "SkyBox.h"
#include "SDLImage.h"
#include "platform/GLContext.h"
#include "Assert.h"

SkyBox::SkyBox(string path, real s, bool b) {
	bottom = b;
	size = s;
	Load(path);
}

void SkyBox::Load(string path) {
	Image3D *img = new SDLImage(path);
	Assert(img->Valid());
	texture.Store(*img);

	static v3r v[]={
		v3r(0,0,0),
		v3r(0,1,0),
		v3r(0,0,1),
		v3r(0,1,1),
		v3r(1,0,0),
		v3r(1,1,0),
		v3r(1,0,1),
		v3r(1,1,1)
	};

	static v3r vv[]={
		v[2],v[6],v[4],v[0],
		v[3],v[7],v[6],v[2],
		v[6],v[7],v[5],v[4],
		v[7],v[3],v[1],v[5],
		v[3],v[2],v[0],v[1],
		v[0],v[4],v[5],v[1],
	};
	static v2r tt[]={
		v2r(.000,.66666666),v2r(.250,.66666666),v2r(.250,.33333333),v2r(.000, .33333333),
		v2r(.000,1.0000000),v2r(.250,1.0000000),v2r(.250,.66666666),v2r(.000, .66666666),
		v2r(.250,.66666666),v2r(.500,.66666666),v2r(.500,.33333333),v2r(.250, .33333333),
		v2r(.500,.66666666),v2r(.750,.66666666),v2r(.750,.33333333),v2r(.500, .33333333),
		v2r(.750,.66666666),v2r(1.00,.66666666),v2r(1.00,.33333333),v2r(.750, .33333333),
		v2r(.000,.33333333),v2r(.250,.33333333),v2r(.250,.00000000),v2r(.000, .00000000),
	};

	mesh = Mesh(GL_QUADS);

	u32 n = lengthof(vv);
	if(!bottom) { // don't draw last one, this is ugly hack
		n-=4;
	}

	mesh.nVertices=n;
	mesh.vertices=vv;
	mesh.texcoords2=tt;

	vbo.Store(mesh);
}

void SkyBox::Draw(const Camera &cam) {
	using namespace GLContext;

	texture.Pre();
	GLSaveStack(){
		Translate(cam.pos);
		Scale(size);
		Translate(-.5,-.5,-.5);
		vbo.Draw();
	}
	texture.Post();
}


