

#ifndef __GLWRAPPER_H
#define __GLWRAPPER_H

#include "backend.h"
#include <GL/glew.h>
//#include <GL/gl.h>
//#include <GL/glu.h>
#include "types.h"
#if YARA_WINDOWS
	#include <glut.h>
#else
	#include <GL/glut.h>
#endif
#include "math/seq.h"
#include "math/Orientation.h"

namespace GLContext{
	bool PushMatrix   ();
	bool PopMatrix    ();
	void LoadIdentity ();
	void Vertex    (const v3r& s);
	void Normal    (const v3r& s);
	void TexCoord  (const v2r& s);
	void Color     (const color& s);
	void Translate (const v3r& s);
	void Scale     (const v3r& s);
	void Rotate    (real r, const v3r& axis);
	void Transform (const v3r& pos, const Orientation& dir);
	void Transform (const seq<4,v4r>& mat);


	inline void Rotate   (real r, real x, real y, real z) { Rotate(r, v3r(x,y,z)); }
	inline void Color    (real r, real g, real b, real a=1){ Color(color(r,g,b,a)); }
	inline void Vertex   (real x, real y, real z){ Vertex(v3r(x,y,z)); }
	inline void Normal   (real x, real y, real z){ Normal(v3r(x,y,z)); }
	inline void Translate(real x, real y, real z){ Translate(v3r(x,y,z)); }
	inline void Scale    (real x, real y, real z){ Scale(v3r(x,y,z)); }
	inline void Scale    (real s){ Scale(s,s,s); }
	inline void TexCoord (real s, real t){ TexCoord(v2r(s,t)); }

	inline bool Begin(GLenum mode) { glBegin(mode); return true; }
	inline bool End() { glEnd(); return false; }

	bool Enable(GLenum cap);
	bool Disable(GLenum cap);

	bool Enable(const GLenum* cap);
	bool Disable(const GLenum* cap);

	struct Light{
		Light(u8 n=0);

		u8 light; // 0 through 7
		v3f pos; // Light position
		float homo; // Homogeneous coordinate of light position


		// Color Components
		color ambient;
		color diffuse;
		color specular;

		// Spotlight parameters
		struct {
			v3f direction; 
			float exponent; // Cosine Exponent
			float cutoff;   // Cutoff angle (in radians {range [0 - PI/2] and PI})
		} spot;

		// Attenuation Coefficients
		v3f attenuation; // [0]=constant, [1]=linear, [2]=quadratic

		bool Enable()  const { glEnable  (GL_LIGHT0+light); return true; }
		bool Disable() const { glDisable (GL_LIGHT0+light); return false; }
		void Apply() const;
	};


	struct Material{
		// Color components
		color ambient;
		color diffuse;
		color emission;
		color specular;

		// Specular coefficient
		float shininess;

		Material();
		void Apply() const;
	};

	struct Fog {
		enum FogType {
			Exp    = GL_EXP,
			Exp2   = GL_EXP2,
			Linear = GL_LINEAR,
		};

		FogType mode;
		color fogColor;

		float expDensity;

		float linStart;
		float linEnd;

		bool Enable() const { glEnable(GL_FOG); return true; }
		bool Disable() const { glDisable(GL_FOG); return false; }
		void Apply() const;
	};
}


#define GLCapability(...) \
	for(const GLenum __caps[]={__VA_ARGS__, 0}, *__cond=GLContext::Enable(__caps) ? __caps : 0; __cond; __cond=GLContext::Disable(__caps) ? __caps : 0)

#define GLSaveStack() for(bool __gldraw=GLContext::PushMatrix(); __gldraw; __gldraw=GLContext::PopMatrix())

#define GLPrimitive(MODE) for(int __gldraw=GLContext::Begin(MODE); __gldraw; __gldraw=GLContext::End())

namespace GL = GLContext;

#endif
