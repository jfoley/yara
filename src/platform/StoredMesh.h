#ifndef __STOREDMESH_H
#define __STOREDMESH_H

#include "platform/Mesh.h"

class StoredMesh {
	public:
		StoredMesh() { Reset(0, 0); }
		void Reset(u32 numVertices = 0, u32 numIndices = 0);

		/*void UseVertices();
		void UseNormals();
		void UseColors();
		void UseIndices();
		void UseTexCoords2();*/
		void Finalize(GLenum primitiveType);

		Mesh m;
		VBO vbo;
		u32 nVertices;
		u32 nIndices;
		vector<v3r>   vertices;
		vector<v3r>   normals;
		vector<v3r>   texcoords3;
		vector<v2r>   texcoords2;
		vector<v1r>  texcoords1;
		vector<color> colors;
		vector<u32>   indices;
};



#endif

