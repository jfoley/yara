#ifndef __HEIGHTMAP_H
#define __HEIGHTMAP_H

#include "types.h"
#include "macros.h"
#include "math/geom.h"
#include "Assert.h"


class TerrainFunc {
	public:
		virtual ~TerrainFunc() {}
		virtual real operator()(real h, real x, real y) const = 0;
};

class HeightMap {
	public:
		/// mildly nonsense default constructor
		HeightMap() : worldSize(1), dataSize(10) { }
		/// Generate a heightmap of a given world size, resolution ( up to 128 with current vbo interface and an initial height
		HeightMap(real wSize, u32 resolution, real initial=0);
		
		//--- Building Functions
		void Sample(const HeightMap &other) { Sample(other, v2r(0,0), other.worldSize); }
		void Sample(const HeightMap &other, v2r offset, real size);
		void PerfectSample(const HeightMap &other, v2i offset);
		void MakeSeamless();
		void AddFunc(const TerrainFunc &func);
		void SetMagnitude(const real f);
		real MinHeight() const;
		real MaxHeight() const;

		//--- Finalize
		void CalculateNormals();

		//--- For accessing data, CalculateNormals first
		v3r ApproximateNormal(const v2r &pos) const;
		real ApproximateHeight(const v2r &pos) const;
		v3r Approximate(const v2r& pos) const {
			return v3r(pos) + AXIS_Z*ApproximateHeight(pos);
		}

		//--- Exporting to StoredMesh
		void ExportNormals(vector<v3r> *dest) const;
		void ExportVertices(vector<v3r> *dest, v2r offset=v2r(0,0)) const;
		void ExportTexCoords(vector<v2r> *dest) const;
		void ExportIndices(vector<u32> *dest) const;
		u32 NumVertices() const;
		u32 NumIndices() const;

		//--- for setting things based on Roads
		real& Get(i32 x, i32 y);
		real Get(i32 x, i32 y) const;
		real Unit() const;

		//--- Drawing
		void DrawPoints() const;

	private:
		u32 Index(i32 x, i32 y) const;
		v3r GetPoint(i32 x, i32 y) const;
		v3r GetNormal(i32 x, i32 y) const;
		
		/// Size of the World this heightmap represents
		real worldSize;
		/// Number of datapoints only a u32 to reflect VBOs
		u32 dataSize;
		/// The height data
		vector<real> data;

		vector<v3r> normals;
};


#endif

