
#ifndef __TESTGAME_H
#define __TESTGAME_H

#include "platform/platform.h"
#include "platform/GLContext.h"
#include "platform/GLContextHandler.h"

class ScreenSaver: public GLContextHandler{

	public:
		void Resize(int w, int h);

		void Redraw();
		void BeforeLoop();
		string GetName() const { return "ScreenSaver"; }

		void Quit(){ gfxrunning=false; }
		void KeyDn(KeyCode key){
			if(key==KEY_ESCAPE)
				Quit();
			if(key==KEY_F4)
				ToggleFullScreen();
		}
		bool done;
};

#endif
