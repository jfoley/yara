
#include "ScreenSaver.h"

GAME(ScreenSaver);

// The following nonsense is mostly stolen from the interbutt.


void ScreenSaver::Resize( int width, int height ){
	float ratio = (float) width / (float) height;
	glShadeModel( GL_SMOOTH );
	glCullFace( GL_BACK );
	glFrontFace( GL_CCW );
	glEnable( GL_CULL_FACE );
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_LINE_SMOOTH );
	glClearColor( 0, 0, 0, 0 );
	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );
	gluPerspective( 60.0, ratio, 1.0, 1024.0 );
	Redraw();
}

#include "math/seq.h"
#include "macros.h"
#include "math/const.h"

template <unsigned int N, typename T>
seq<N,T> rol(const seq<N,T>& s){
	seq<N,T> o;
	loop(i,N)
		o.data[i]=s.data[(i+1)%N];
	return o;
}

void ScreenSaver::BeforeLoop(){
	gfxrunning=true;
	fps_preferred=30.0f;

	show(true);
	//show(GetCurrentScreenHeight());
	show(fps_preferred);

	//SetWindow(800,600);
	SetWindow(GetCurrentScreenWidth(),GetCurrentScreenHeight(),true);
}

void draw_grid(const v3r& center){
	int n=15;
	real gridspacing=10.0;
	glLineWidth(1.5);
	glPushMatrix();
	glScalef(gridspacing,gridspacing,gridspacing);
	glBegin(GL_LINES);
	loop2(i,j,n,n){
		int ii=i-n/2;
		int jj=j-n/2;
		v3r a(ii,jj,-n);
		v3r b(ii,jj, n);


		real d=1/v3r(ii,jj,1).MagnitudeSquared();
		glColor3f(d,d,d);
		loop(k,3){
			using namespace GLContext;
			Vertex(a+center);
			Vertex(b+center);
//			glVertex3fv((a+center).data);
//			glVertex3fv((b+center).data);
			a=rol<3,real>(a);
			b=rol<3,real>(b);
		}
	}

	glEnd();
	glPopMatrix();
}


void ScreenSaver::Redraw(){
	/* Our angle of rotation. */
	static float angle = 0.0f;

	static GLfloat v0[] = { -1.0f, -1.0f,  1.0f };
	static GLfloat v1[] = {  1.0f, -1.0f,  1.0f };
	static GLfloat v2[] = {  1.0f,  1.0f,  1.0f };
	static GLfloat v3[] = { -1.0f,  1.0f,  1.0f };
	static GLfloat v4[] = { -1.0f, -1.0f, -1.0f };
	static GLfloat v5[] = {  1.0f, -1.0f, -1.0f };
	static GLfloat v6[] = {  1.0f,  1.0f, -1.0f };
	static GLfloat v7[] = { -1.0f,  1.0f, -1.0f };
	static GLubyte red[]    = { 255,   0,   0, 255 };
	static GLubyte green[]  = {   0, 255,   0, 255 };
	static GLubyte blue[]   = {   0,   0, 255, 255 };
	static GLubyte white[]  = { 255, 255, 255, 255 };
	static GLubyte yellow[] = {   0, 255, 255, 255 };
	static GLubyte black[]  = {   0,   0,   0, 255 };
	static GLubyte orange[] = { 255, 255,   0, 255 };
	static GLubyte purple[] = { 255,   0, 255,   0 };

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity( );




gluLookAt(
		10*cos(angle),10*sin(angle),2,
		0,0,0,
		0,sin(angle*sqrt(2.0)/2),cos(angle*math::E)/2);


draw_grid(v3r());

			angle+=0.02;

	glBegin( GL_TRIANGLES );

	glColor4ubv( red );
	glVertex3fv( v0 );
	glColor4ubv( green );
	glVertex3fv( v1 );
	glColor4ubv( blue );
	glVertex3fv( v2 );

	glColor4ubv( red );
	glVertex3fv( v0 );
	glColor4ubv( blue );
	glVertex3fv( v2 );
	glColor4ubv( white );
	glVertex3fv( v3 );

	glColor4ubv( green );
	glVertex3fv( v1 );
	glColor4ubv( black );
	glVertex3fv( v5 );
	glColor4ubv( orange );
	glVertex3fv( v6 );

	glColor4ubv( green );
	glVertex3fv( v1 );
	glColor4ubv( orange );
	glVertex3fv( v6 );
	glColor4ubv( blue );
	glVertex3fv( v2 );

	glColor4ubv( black );
	glVertex3fv( v5 );
	glColor4ubv( yellow );
	glVertex3fv( v4 );
	glColor4ubv( purple );
	glVertex3fv( v7 );

	glColor4ubv( black );
	glVertex3fv( v5 );
	glColor4ubv( purple );
	glVertex3fv( v7 );
	glColor4ubv( orange );
	glVertex3fv( v6 );

	glColor4ubv( yellow );
	glVertex3fv( v4 );
	glColor4ubv( red );
	glVertex3fv( v0 );
	glColor4ubv( white );
	glVertex3fv( v3 );

	glColor4ubv( yellow );
	glVertex3fv( v4 );
	glColor4ubv( white );
	glVertex3fv( v3 );
	glColor4ubv( purple );
	glVertex3fv( v7 );

	glColor4ubv( white );
	glVertex3fv( v3 );
	glColor4ubv( blue );
	glVertex3fv( v2 );
	glColor4ubv( orange );
	glVertex3fv( v6 );

	glColor4ubv( white );
	glVertex3fv( v3 );
	glColor4ubv( orange );
	glVertex3fv( v6 );
	glColor4ubv( purple );
	glVertex3fv( v7 );

	glColor4ubv( green );
	glVertex3fv( v1 );
	glColor4ubv( red );
	glVertex3fv( v0 );
	glColor4ubv( yellow );
	glVertex3fv( v4 );

	glColor4ubv( green );
	glVertex3fv( v1 );
	glColor4ubv( yellow );
	glVertex3fv( v4 );
	glColor4ubv( black );
	glVertex3fv( v5 );
	glEnd( );

	{
		static int c=0;
		c++;
		if(c>=30){
			printf("fps=%f, %3.2f %% err\n", fps_actual, (fps_preferred-fps_actual)/fps_preferred*100);
			c=0;
		}
	}
}

