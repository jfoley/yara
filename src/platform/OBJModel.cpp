
#include "util/parse.h"
#include "util/strings.h"
#include "OBJModel.h"

using namespace std;
using namespace util;


namespace util {
	vector<string> flatten(const vector< vector<string> >& vv){
		vector<string> v;
		loop(i, vv.size())
			loop(j, vv[i].size())
			v.push_back(vv[i][j]);
		return v;
	}
	vector<string> split(const vector<string>& v, char c){
		vector< vector<string> > vv;
		loop(i, v.size())
			vv.push_back(split(v[i],c));
		return flatten(vv);
	}
	vector<string> split(const string& s, char c0, char c1){
		return split(split(s,c0),c1);
	}
}

vector<u32> objslash(const string& s){
	unsigned v=split(s,' ').size();
	vector<string> l=split(s,'/',' ');
	vector<u32> values;
	loop(i, l.size()){
		Option<u32> x=parse<u32>(l[i]);
		if(x.Valid())
			values.push_back(u32(x));
		else
			values.push_back(0);
		loop(j, 3-l.size()/v)
			values.push_back(0);
	}
	return values;
}

const Mesh* OBJModel::PolyGroup::GetMesh() const {
	if(!v.size())
		return 0;
	if(t.size()!=0 && t.size()!=v.size())
		return 0;
	if(n.size()!=0 && n.size()!=v.size())
		return 0;

	Mesh* mesh=new Mesh(mode);
	mesh->nVertices  = v.size();
	mesh->vertices   = v.size() ? (v3r*) &v[0] : 0;
	mesh->texcoords2 = t.size() ? (v2r*) &t[0] : 0;
	mesh->normals    = n.size() ? (v3r*) &n[0] : 0;
	return mesh;
}

void OBJModel::PolyGroup::SlowDraw() const {
	if(!v.size())
		return;
	const Mesh* mesh=GetMesh();
	if(!mesh)
		return;
	mesh->SlowDraw();
	delete mesh;
}

void OBJModel::LoadOBJModel(OBJModel* objmodel, istream& f, ostream& log){
	if(!f.good())
		return;

	vector<v3r> v_raw;
	vector<v2r> t_raw;
	vector<v3r> n_raw;

	vector<PolyGroup>& groups=objmodel->polygroups;
	PolyGroup begin;
	groups.push_back(begin);

	while(f.good()){
		string line;
		getline(f,line);
		line=compactSpaces(line);

		string cmd=before(line, ' ');
		string arg=after(line, ' ');

		PolyGroup& grp=groups[groups.size()-1];

		match(string, cmd){
			with("#"){
				log << "Comment:\t" << arg << "\n";
				break;
			}
			with("o"){
				log << "Object:\t" << arg << "\n";
				PolyGroup g;
				g.name=arg;
				groups.push_back(g);
				break;
			}
			with("g"){
				log << "Group:\t" << arg << "\n";
				PolyGroup g;
				g.name=arg;
				groups.push_back(g);
				break;
			}
			with("v"){
				v3r v=parse<v3r>(arg);
				log << "Vertex:\t" << v << "\n";
				v_raw.push_back(v);
				break;
			}
			with("vt"){
				v3r vt=parse<v3r>(arg);
				log << "TexCoord:\t" << vt << "\n";
				t_raw.push_back(vt);
				break;
			}
			with("vn"){
				v3r vn=parse<v3r>(arg);
				log << "Normal:\t" << vn << "\n";
				n_raw.push_back(vn);
				break;
			}
			with("f"){
				log << "Face:\t" << arg << "\n";
				vector<u32> face=objslash(arg);

				unsigned mode=face.size()/3;
				grp.mode=(
						mode==3 ? GL_TRIANGLES :
						mode==4 ? GL_QUADS :
						GL_LINE_STRIP
						);

				loop(i, face.size()/3){
					if(face[3*i+0] && face[3*i+0]<=v_raw.size()) grp.v.push_back(v_raw[face[3*i+0]-1]);
					if(face[3*i+1] && face[3*i+1]<=t_raw.size()) grp.t.push_back(t_raw[face[3*i+1]-1]);
					if(face[3*i+2] && face[3*i+2]<=n_raw.size()) grp.n.push_back(n_raw[face[3*i+2]-1]);
				}
				break;
			}
			default(){
				log << "Unknown:\t" << line << "\n";
			}
		}
	}
}

void OBJModel::LoadOBJModel(OBJModel* objmodel, string filename, ostream& log){
	ifstream f(filename.c_str());
	if(!f.good()){
		f.close();
		return;
	}
	LoadOBJModel(objmodel, f, log);
	f.close();
}


void OBJModel::SlowDraw() const {
	loop(i, NGroups())
		operator[](i).SlowDraw();
}
