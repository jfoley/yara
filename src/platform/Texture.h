
#ifndef __TEXTURE_H
#define __TEXTURE_H

#include "Image3D.h"

class Texture {
	public:
		Texture(const Image3D& img);
		Texture(u16 w=0, u16 h=0, u16 d=0, const void* data=0, GLenum format=GL_RGB, GLenum dtype=GL_UNSIGNED_BYTE);
		~Texture();

		bool Valid() const;
		void Store(const Image3D& img);
		void Store(u16 w=0, u16 h=0, u16 d=0, const void* data=0, GLenum format=GL_RGB, GLenum dtype=GL_UNSIGNED_BYTE);
		void Free();

		void Pre();
		void Post();

		GLuint GetID() const { return tex; }
		
		u16 width;
		u16 height;

	private:
		GLuint tex;
		bool stored;
		bool bound;

		void Init();
		void GenTex();
		void DelTex();
		void BindTex();
		void UnBindTex();
};

#endif
