#ifndef __HOUSE_H
#define __HOUSE_H

#include "gob/GameObject.h"
#include "World.h"

class House : public GameObject {
	public:
		House(u32 s);
		virtual ~House() { }
		void Generate();
		virtual void Draw(const GameObject &cam);
		void SetOnWorld(const World& world);

		color wallColor;
		color roofColor;

		bool chimney;
		u32 seed;
		real width;
		real height;
		real roofSize;
		
};


#endif

