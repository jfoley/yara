#include "Model.h"
#include "Assert.h"
#include "platform/SDLImage.h"
#include "util/parse.h"
#include "filesystem/Path.h"
using namespace GLContext;

#include <aiMatrix4x4.h>

namespace {
	//--- this owns all aiScene data
	Assimp::Importer assimp;
	
	void GetBoundingBoxR(const aiScene *scene, const aiNode *node, aiVector3D *min, aiVector3D *max, aiMatrix4x4 *matrix) {
		// determine current matrix & save previous
		aiMatrix4x4 prevMatrix = *matrix;
		*matrix *= node->mTransformation;

		// update min and max as necessary
		loop(mesh_id, node->mNumMeshes) {
			const aiMesh *mesh = scene->mMeshes[node->mMeshes[mesh_id]];
			loop(v_id, mesh->mNumVertices) {
				aiVector3D cur = mesh->mVertices[v_id];
				cur *= *matrix;
				//aiTransformVecByMatrix4(&cur, matrix);
				
				if(cur.x < min->x) min->x = cur.x;
				if(cur.y < min->y) min->y = cur.y;
				if(cur.z < min->z) min->z = cur.z;
				
				if(cur.x > max->x) max->x = cur.x;
				if(cur.y > max->y) max->y = cur.y;
				if(cur.z > max->z) max->z = cur.z;
			}
		}

		// recurse to children
		loop(child_id, node->mNumChildren) {
			GetBoundingBoxR(scene, node->mChildren[child_id], min, max, matrix);
		}

		// unroll transformations
		*matrix = prevMatrix;
	}

	void GetBoundingBox(const aiScene *scene, v3r &min, v3r &max) {
		aiMatrix4x4 transform;
		//aiIdentityMatrix4(&transform);

#define ALMOST_INFINITY 1e10f
		aiVector3D minimum, maximum;
		minimum.x = minimum.y = minimum.z = ALMOST_INFINITY;
		maximum.x = maximum.y = maximum.z = -ALMOST_INFINITY;

		GetBoundingBoxR(scene, scene->mRootNode, &minimum, &maximum, &transform);

		min[0] = minimum.x;
		min[1] = minimum.y;
		min[2] = minimum.z;
		max[0] = maximum.x;
		max[1] = maximum.y;
		max[2] = maximum.z;
	}
}

Model::Model() {
	Clear();
}

void Model::Clear() {
	data = 0; //ASSIMP data is owned by the library, no delete needed
	lastTex = 0;
	scale = 1.f;

	loop(i, textures.size())
		delete textures[i];
	textures.clear();
}

bool Model::Load(const string &fileName, float scl) {
	Clear();
	scale = scl;

	show(Path(fileName));
	Assert(Path(fileName).Exists());

	basePath = Path(fileName).GetDir();
	Assert(Path(basePath).Directory());

	data = assimp.ReadFile(fileName.c_str(),
//aiProcessPreset_TargetRealtime_Quality);
			aiProcess_Triangulate |
			aiProcess_JoinIdenticalVertices |
			aiProcess_RemoveRedundantMaterials |
			aiProcess_SortByPType
			);

	if(!data || !data->mRootNode) {
		show("Loading Model Failed\n");
		Clear();
		return false;
	}

	v3r min;
	v3r max;
	GetBoundingBox(data, min, max);

	min *= scale;
	max *= scale;

	size = max-min;

	SetPosition(pos);

	//--- extract styles
	FetchTextures();

	return Valid();
}

void Model::SetPosition(const v3r pt) {
	pos = pt;
	pos[2] += size[2]/2.5;
}

void Model::Draw(const GameObject &cam) {
	if(!Valid())
		return;
	if(!data || !data->mRootNode)
		return;

	/*
	show(data);
	show(data->mRootNode);
	show(data->mRootNode->mNumChildren);
	*/
	GLSaveStack() {
		//GLContext::Translate(pos);

		GLContext::Transform(pos, dir);
		GLContext::Transform(v3r(), Orientation(AXIS_Y, -AXIS_Z));
		lastTex = 0;
		Render(data, data->mRootNode);
		if(lastTex) {
			lastTex->Post();
			lastTex = 0;
		}
	}

}

bool Model::FetchTextures() {
	//--- load textures embedded in file
	loop(i, data->mNumTextures) {
		aiTexture* texture = data->mTextures[i];

		//--- calculate embedded data size
		if(texture->mHeight != 0) {
			show("TODO");
			Assert(0);
			return false;
		} else {
			SDLImage img;
			img.Load(texture->pcData, texture->mWidth, texture->achFormatHint);
			Assert(img.Valid());
			textures.push_back(new Texture(img));
		}
	}

	//show(data->mNumMaterials);

	loop(i, data->mNumMaterials) {
		aiMaterial *mat = data->mMaterials[i];
		aiString path;
		
		int texIndex = 0;
		aiReturn hasTexture = AI_SUCCESS;
		while(hasTexture == AI_SUCCESS) {
			hasTexture = mat->GetTexture(aiTextureType_DIFFUSE, texIndex++, &path);

			if(path.data[0] == '*')
				continue;
			const string name(path.data);
			if(name == "")
				continue;
			if(loaded_textures.find(name) != loaded_textures.end())
				continue;

			//show(name);
			//show(path.data);

			Path p;
			if(Path(name).Exists()) {
				p = name;
			} else {
				p = basePath;
				p += name;
			}
			//show(p);

			if(!p.Exists() || p.Directory()) {
				cerr << "Couldn't find texture \"" << name << "\"\n";
				Assert(0);
			}

			SDLImage img(p);
			Assert(img.Valid());
			Texture *tex = new Texture(img);
			Assert(tex->Valid());
			loaded_textures[name] = tex;
		}
	}
	
	return true;
}

v4f ConvertColor(const aiColor4D *input) {
	return v4f(input->r, input->b, input->g, input->a);
}

void Model::ApplyMaterial(const aiMaterial *material) {
	aiString path;
	if(AI_SUCCESS == material->GetTexture(aiTextureType_DIFFUSE, 0, &path)) {
		Texture *next = 0;
		
		if(path.data[0] == '*') { 
			//--- embedded texture
			int id = util::parse<int>(string(&path.data[1])).GetOrElse(-1);
			if(id >= 0 && id < (int) textures.size()) {
				next = textures.at(id);
			}
		} else {
			if(loaded_textures.find(path.data) != loaded_textures.end()) {
				next = loaded_textures[path.data];
			}
		}

		//--- switch textures
		if(next) {
			Assert(next->Valid());

			if(lastTex != next) {
				if(lastTex) lastTex->Post();

				next->Pre();
				lastTex = next;
			}
		}
	}

	Assert(material);
	//TODO apply material...

	aiColor4D tmp;
	v4f c;

	c = v4f(0.8f, 0.8f, 0.8f, 0.0f);
	if(AI_SUCCESS == aiGetMaterialColor(material, AI_MATKEY_COLOR_DIFFUSE, &tmp))
		c = ConvertColor(&tmp);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c.data);

	c = v4f(0.0f, 0.0f, 0.0f, 0.0f);
	if(AI_SUCCESS == aiGetMaterialColor(material, AI_MATKEY_COLOR_SPECULAR, &tmp))
		c = ConvertColor(&tmp);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, c.data);

	c = v4f(0.2f, 0.2f, 0.2f, 2.0f);
	//if(AI_SUCCESS == aiGetMaterialColor(material, AI_MATKEY_COLOR_AMBIENT, &tmp))
		//c = ConvertColor(&tmp);

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, c.data);

	c = v4f(0.0f, 0.0f, 0.0f, 0.0f);
	if(AI_SUCCESS == aiGetMaterialColor(material, AI_MATKEY_COLOR_EMISSIVE, &tmp))
		c = ConvertColor(&tmp);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, c.data);

	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 0);

}

void Model::Render(const aiScene *scene, const aiNode *node) {
	aiMatrix4x4 m;
	m.Scaling(aiVector3D(scale, scale, scale), m);
	m *= node->mTransformation;

	m.Transpose();
	GLSaveStack() {
		glMultMatrixf((float*) &m);

		//--- draw this
		loop(mesh_id, node->mNumMeshes) {
			const aiMesh *mesh = scene->mMeshes[node->mMeshes[mesh_id]];
			const aiMaterial *material = scene->mMaterials[mesh->mMaterialIndex];

			//--- set texture and lighting values
			ApplyMaterial(material);

			if(mesh->mNormals == NULL)
				glDisable(GL_LIGHTING);
			else
				glEnable(GL_LIGHTING);

			if(mesh->mColors[0] != NULL)
				glEnable(GL_COLOR_MATERIAL);
			else
				glDisable(GL_COLOR_MATERIAL);

			loop(face_id, mesh->mNumFaces) {
				const aiFace *face = &mesh->mFaces[face_id];
				GLenum face_mode;

				switch(face->mNumIndices) {
					case 0: continue;
					case 1: face_mode = GL_POINTS; break;
					case 2: face_mode = GL_LINES; break;
					case 3: face_mode = GL_TRIANGLES; break;
					default: face_mode = GL_POLYGON; break;
				}
				/*
					 if(face->mNumIndices != 3)
					 show(face->mNumIndices);
					 */

				GLPrimitive(face_mode) {
					loop(index_id, face->mNumIndices) {
						int index = face->mIndices[index_id];
						if(mesh->mColors[0] != NULL) {
							const aiColor4D *color = &mesh->mColors[0][index];
							glColor4f(color->r, color->g, color->b, color->a);
						}
						if(mesh->HasTextureCoords(0)) {
							glTexCoord3f(mesh->mTextureCoords[0][index].x, 1 - mesh->mTextureCoords[0][index].y, 0);
							//Assert(lastTex);
							//Assert(lastTex->Valid());
						}
						if(mesh->mNormals != NULL) {
							glNormal3fv(&mesh->mNormals[index].x);
						}
						glVertex3fv(&mesh->mVertices[index].x);
					}
				}
			}

		}

		//--- recursively draw children with this transpose
		loop(child_id, node->mNumChildren) {
			Render(scene, node->mChildren[child_id]);
		}
	}
}
