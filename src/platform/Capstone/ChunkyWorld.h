#ifndef __CHUNKYWORLD_H
#define __CHUNKYWORLD_H

#include "platform/HeightMap.h"
#include "platform/Texture.h"

struct WorldChunk;

class ChunkyWorld {
	public:
		ChunkyWorld(real wSize=1, u32 numChunks=1, u32 chunkRes=10);
		~ChunkyWorld();

		void InitFromTerrain(const HeightMap &);

		void Draw(const v2r &position) const;

	private:
		Texture* GenerateTexture(u32 resolution, const HeightMap &portion, v2r pos, real max, real min);
		
		int GetIndex(i32 x, i32 y) const;
		WorldChunk* GetChunk(i32 x, i32 y) const;

		real worldSize;
		real chunkSize;
		u32 numChunks;
		u32 chunkRes;
		vector<WorldChunk*> data;
};



#endif

