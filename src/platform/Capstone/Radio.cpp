#include "Radio.h"
#include "filesystem/Path.h"
#include "filesystem/FileSystem.h"
#include "Capstone.h"
#include <algorithm>
#include "util/strings.h"
#include "Assert.h"

#define NUM_CHANNELS 4u

bool operator==(const SongData &lhs, const SongData &rhs) {
	if(lhs.size() != rhs.size())
		return false;

	uloop(i, lhs.size()) {
		if(lhs[i].size() != rhs[i].size()) {
			return false;
		}

		uloop(j, lhs[i].size()) {
			string left = Path(lhs[i][j]).Name();
			string right = Path(rhs[i][j]).Name();
			if(left != right) {
				return false;
			}
		}
	}
	return true;
}

void ShowSongData(const SongData &sd) {
	uloop(ch_id, sd.size()) {
		string channelStr = util::stringf("channel%d", ch_id+1);
		uloop(s_id, sd[ch_id].size()) {
			cout << channelStr << "/" << Path(sd[ch_id][s_id]).Name() << "\n";
		}
	}
}

vector<string> StringsFromPaths(const vector<Path> &paths) {
	vector<string> result;
	result.reserve(paths.size());

	uloop(i, paths.size())
		result.push_back(paths[i]);

	std::sort(result.begin(), result.end());

	return result;
}

// TODO, do this a better way
bool IsMusicFile(string path) {
	using namespace util;

	static const char *valid [] = {
		"mp3",
		"m4a",
		"ogg",
		"wav",
		"aif",
		"aiff",
		0
	};

	string extension = afterLast(path, '.');

	for(u32 i=0; valid[i] != 0; i++) {
		if(extension == valid[i])
			return true;
	}
	return false;
}

vector<Path> GetChildrenIf(string input, bool isDir) {
	vector<Path> children = Path(input).List();
	vector<Path> results;
	uloop(i, children.size()) {
		if(children[i].Directory() == isDir) {
				results.push_back(children[i]);
		}
	}
	return results;
}

vector<Path> GetAllMountedMedia(string mountPath) {
	vector<Path> possibleMedia = GetChildrenIf(mountPath, true);
	return possibleMedia;
}

void SongDataFromDirectory(string dir, SongData &songdata) {
	dir = Path(dir);
	using namespace util;

	//--- make return parameter big enough
	if(songdata.size() != NUM_CHANNELS)
		songdata.resize(NUM_CHANNELS);
	
	vector<string> folders = StringsFromPaths(GetChildrenIf(dir, true));

	i32 channelId = -1;
	uloop(f_id, folders.size()) {
		Path folder(folders[f_id]);

		if(contains(toLower(folder.Name()), "channel")) {
			channelId = ((channelId+1) % NUM_CHANNELS);
		}

		vector<string> songs = StringsFromPaths(GetChildrenIf(folder, false));

		uloop(s_id, songs.size()) {
			string &song = songs[s_id];

			if(IsMusicFile(song)) {
				songdata[channelId].push_back(song);
			}
		}
	}
}

bool SongDataUseless(SongData &songdata) {
	if(songdata.size() != NUM_CHANNELS)
		return true;

	bool useful = true;
	uloop(i, NUM_CHANNELS) {
		useful &= (songdata[i].size() != 0);
	}
	return !useful;
}

Radio::Radio(SoundServer *p) : soundServer(p) {
	power = false;
	currentChannel = 0;
	currentFile.resize(NUM_CHANNELS, 0);
	currentSound.resize(NUM_CHANNELS, -1);

	Assert(IsMusicFile("lol.mp3"));
	Assert(IsMusicFile("lol.m4a"));
	Assert(!IsMusicFile("lol.txt"));
	
	const Param &config = GetConfig();
	if(!Path(config.musicCachePath).Exists())
		MakeDir(config.musicCachePath);

	//SetChannel(0);

	SetInterval(1);
	Start();
}

void Radio::ReloadSongData() {
	const Param &config = GetConfig();

	SongData fromCache;
	SongDataFromDirectory(config.musicCachePath, fromCache);

	SongData fromMedia;
	vector<Path> media = GetAllMountedMedia(config.mountPath);
	uloop(drive_id, media.size()) {
		SongDataFromDirectory(media[drive_id], fromMedia);
	}

	if(SongDataUseless(fromMedia)) {
		if(config.printMediaInfo) {
			cout << "Radio Song Data on media seems useless.\n";
		}
		songData = fromCache;
		return;
	}

	//show("fromMedia");
	//ShowSongData(fromMedia);
	//show("fromCache");
	//ShowSongData(fromCache);
	
	if(fromCache == fromMedia) {
		if(config.printMediaInfo) {
			cout << "Radio Song Data on media seems equivalent to that in the cache.\n";
		}
		songData = fromCache;
		return;
	}

	//--- move old cache to temporary location so we can restore on an error
	string tmp = UniqueSubDir(".", "tmpMediaCache");
	Assert(MoveFile(config.musicCachePath, tmp));
	
	if(config.printMediaInfo) {
		show(fromMedia.size());
	}

	//--- copy fromMedia into fromCache
	try {
		if(!MakeDir(config.musicCachePath))
			throw "Couldn't make new music cache\n";

		uloop(ch_id, NUM_CHANNELS) {
			string channelStr = util::stringf("channel%d", ch_id+1);
			Path channelFolder = Path(config.musicCachePath) + channelStr;
			
			if(!MakeDir(channelFolder))
				throw "Couldn't make channel folder...";

			uloop(s_id, fromMedia[ch_id].size()) {
				Path src(fromMedia[ch_id][s_id]);
				Path dest(config.musicCachePath);
				Path target = dest + channelStr;

				if(!CopyFile(src, target)) {
					throw "Couldn't copy file...";
				}
			}
		}

		//--- remove backup instead of restoring here
		Assert(RemoveAll(tmp));

		//--- Invalidate current Song, next song will be from new set on flash drive
		currentFile.clear();
		currentFile.resize(NUM_CHANNELS, 0);

		songData = fromMedia;

	} catch (const char *failure) {
		if(config.printMediaInfo) {
			cerr << failure << "\n";
		}

		//--- restore previous state
		RemoveAll(config.musicCachePath);
		Assert(MoveFile(tmp, config.musicCachePath));
		
		songData = fromCache;
	}
}

string Radio::GetNextSong() {
	ReloadSongData();

	if(SongDataUseless(songData))
		return "";

	u32 numSongs = songData[currentChannel].size();
	CurrentFile() = (CurrentFile() + 1) % numSongs;

	return songData[currentChannel][CurrentFile()];
}

string Radio::GetCurrentSong() {
	if(songData.size() && songData[currentChannel].size()) {
		return songData[currentChannel][CurrentFile()];
	} else {
		return "";
	}
}

void Radio::SetChannel(u32 i) {
	u32 oldChannel = currentChannel;

	power = true;
	ReloadSongData();

	ScopeLock lock(soundServer->mutex);
	
	//--- pause old song
	soundServer->KillSound(CurrentSound());

	//--- change channel
	currentChannel = i % NUM_CHANNELS;
	
	//--- start new song or unpause old song
	if(CurrentSound() == -1 || oldChannel == currentChannel)
		CurrentSound() = soundServer->PlaySound(GetNextSong());
	else
		CurrentSound() = soundServer->PlaySound(GetCurrentSong());
	
	show(GetCurrentSong());
}

void Radio::SetPower(bool on) {
	power = on;
	if(!power) {
		ScopeLock lock(soundServer->mutex);
		soundServer->KillSound(CurrentSound());
	}
}

void Radio::Rep() {
	ScopeLock lock(soundServer->mutex);
	if(power && !soundServer->HasSound(CurrentSound())) {
		CurrentSound() = soundServer->PlaySound(GetNextSong());
	}
}



