#include "ChunkyWorld.h"
#include "platform/Texture.h"
#include "platform/StoredMesh.h"
#include "math/Util.h"
#include "Assert.h"
#include "platform/GLContext.h"
#include "math/Random.h"

struct WorldChunk {
	public:
		WorldChunk() : texture(0) { }
		
		~WorldChunk() { 
			if(texture) delete texture; 
		}
		
		void Draw(v3r off) {
			if(texture) {texture->Pre();}
			GLSaveStack() {
				GLContext::Translate(off);
				mesh.vbo.Draw();
			}
			if(texture) {texture->Post();}
		}

		Texture *texture;
		StoredMesh mesh;
};

ChunkyWorld::ChunkyWorld(real ws, u32 nc, u32 cr) {
	worldSize = ws;
	numChunks = nc;
	chunkRes = cr;
	chunkSize = worldSize / numChunks;
}

ChunkyWorld::~ChunkyWorld() {
	for(u32 i=0; i<data.size(); i++) {
		delete data[i];
	}
	data.clear();
}

color TerrainGradient(real t) {
#if 0
	// Raw Noise
	return color(t,t,t,1);
#else
	// Mountain Gradient
	color c;
	if(t < .75)
		c=LERP(color(0,.4,0,1), color(.5,1,.5,1), (t/2));
	else	 
		c=color(.5,1,.5,1);

	// Add in some noise
	static Random r;
	c+=color(real(r)-.5,real(r)-.5,real(r)-.5,0)*.1;

	return c;
#endif
}

Texture* ChunkyWorld::GenerateTexture(u32 detail, const HeightMap &terrain, v2r offset, real max, real min) {
	Texture *tex = new Texture();
	if(!tex)
		return 0;

	vector<u32> data;
	data.resize(SQUARE(detail), 0xff003300);
	loop2(x,y,detail,detail) {
		u32 value = 0xffff00ff;
		v2r pos(chunkSize*RATIO(x, detail), chunkSize*RATIO(y, detail));
		
		real z = terrain.ApproximateHeight(pos);
		real t = (z - min) / (max - min);
		value = Crayon::ColorToInt(TerrainGradient(t));

		data[x+(y*detail)] = value;
	}

	tex->Store(detail, detail, 1, &data[0], GL_RGBA, GL_UNSIGNED_BYTE);
	Assert(tex->Valid());
	
	return tex;
}

void ChunkyWorld::InitFromTerrain(const HeightMap &terrain) {
	this->~ChunkyWorld();
	data.resize(SQUARE(numChunks));

	const real max = terrain.MaxHeight();
	const real min = terrain.MinHeight();
	
	loop2(x,y,numChunks,numChunks) {
		data[GetIndex(x,y)] = new WorldChunk();
		WorldChunk &cur = *GetChunk(x,y);

		const v2i index = v2i(x,y);
		const v2r pos = index*chunkSize;

		//--- get height data
		HeightMap map = HeightMap(chunkSize, chunkRes+1, 0);
		map.Sample(terrain, pos, chunkSize );
	
		//--- generate texture
		cur.texture = GenerateTexture(chunkRes, map, pos, max, min);

		//--- generate VBO
		cur.mesh.Reset(map.NumVertices(), map.NumIndices());

		map.ExportVertices(&cur.mesh.vertices);
		map.ExportNormals(&cur.mesh.normals);
		map.ExportIndices(&cur.mesh.indices);
		map.ExportTexCoords(&cur.mesh.texcoords2);

		cur.mesh.Finalize(GL_QUADS);
	}
}


int ChunkyWorld::GetIndex(i32 x, i32 y) const {
	x = Wrap(x, numChunks);
	y = Wrap(y, numChunks);
	return x+(y*numChunks);
}

WorldChunk* ChunkyWorld::GetChunk(i32 x, i32 y) const {
	return data[GetIndex(x,y)];
}


void ChunkyWorld::Draw(const v2r &position) const {
	v3r origin = (position/worldSize);
	origin[0] = floor(origin[0]);
	origin[1] = floor(origin[1]);
	origin[2] = 0;
	origin *= worldSize;

	v2i index = Wrap(position, worldSize)/chunkSize;

	const int pad = 1;
	for(int x=index[0]-pad; x<=index[0]+pad; x++) {
		for(int y=index[1]-pad; y<=index[1]+pad; y++) {
			v3r pos ( x*chunkSize, y*chunkSize, 0);
			GetChunk(x,y)->Draw(pos + origin);
		}
	}
}

