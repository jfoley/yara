#ifndef __WORLD_H
#define __WORLD_H

#include "gob/Camera.h"

class WorldData;

class World {
	public:
		World();
		~World();

		/// Generate or regenerate world and road data
		void Generate();
		void UpdateConfig();
		
		
		void Pre() const;
		void Post() const;
		
		void Draw(const v2r &pos) const;
		
		real GetHeight(const v2r &pos) const;
		v3r GetNormal(const v2r &pos) const;
		v3r PointOnTerrain(const v3r &pos) const;

		//--- World parameters
		real worldSize;

		//--- Terrain only parameters
		u32 terrainSeed;
		u32 terrainOctaves;
		u32 terrainDetail;
		u16 terrainTexDetail;

	private:
		WorldData *data;
};




#endif

