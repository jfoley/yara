#include "DeviceManager.h"
#include "Capstone.h"
#include "input/YaraDevice.h"
#include "input/Joystick.h"
#include "concur/Lockable.h"

#define GET_ANALOG(name) \
	yDev.GetCalibratedAnalog(GetConfig().analog.name)

#define GET_JOYSTICK(axis) \
	jDev.GetCalibratedAnalog(GetConfig().joystick.axis)

#define GET_DIGITAL(name) \
	yDev.GetDigital(GetConfig().digital.name)

struct DeviceThread : public RepeaterThread {
	DeviceThread(DeviceManager *m, Radio *r) : manager(m), radio(r) {
		SetInterval(0);
	}
	void Rep() {
		//show("Rep");
		{
			ScopeLock lock(mutex);
			if(yDev.Valid()) yDev.Update(); else yDev.Reconnect();
			if(jDev.Valid()) jDev.Update(); else jDev.Reconnect();
		}

		//show(yDev.Valid());

		if(yDev.Valid()) {
			TimeValue now;
			if(real(now - lastHornPress) > GetConfig().digital.delay) {
				bool horn = GET_DIGITAL(horn);

				//--- fire off sounds from horn
				if(horn) {
					static int which = 0;
					const char *horns[] = { "res/sfx/simple.wav", "res/sfx/ooga.wav" };

					which = (which+1) % lengthof(horns);
					ScopeLock lock(radio->soundServer->mutex);
					radio->soundServer->PlaySound(horns[which]);

					show(lastHornPress);

					lastHornPress.Now();
				}
			}

			if(real(now - lastRadioPress) > GetConfig().digital.delay) {
				bool radio0 = GET_DIGITAL(radioStation0);
				bool radio1 = GET_DIGITAL(radioStation1);
				bool radio2 = GET_DIGITAL(radioStation2);
				bool radio3 = GET_DIGITAL(radioStation3);
				bool radioPower = GET_DIGITAL(radioPower);

				//--- Update Radio
				if(radio0)
					radio->SetChannel(0);
				else if(radio1)
					radio->SetChannel(1);
				else if(radio2)
					radio->SetChannel(2);
				else if(radio3)
					radio->SetChannel(3);

				if(radioPower) {
					show("TogglePower!\n");
					radio->TogglePower();
				}

				if(radio0 || radio1 || radio2 || radio3 || radioPower) {
					lastRadioPress.Now();
				}
			}

			//--- check lights
			manager->lights = GET_DIGITAL(lights);

			Calibration wheel(100,936,1,-1);
			Calibration gas(0,1023,0,2);
			Calibration brake(0,1023,0,1.5);
			Calibration joystick(-32768, 32767, 2, -2);

			real accel = gas(GET_ANALOG(gas)) + joystick(GET_JOYSTICK(y));
			bool speed = accel > GetConfig().vibrate.accel;

			//--- set vibrate based on gas
			yDev.SetVibrate(speed);
			yDev.SetFan(speed ? 3 : 0);

			manager->wheel = wheel(GET_ANALOG(wheel)) + joystick(GET_JOYSTICK(x))/3;
			manager->gas = accel + joystick(GET_JOYSTICK(y));
			manager->brake = brake(GET_ANALOG(brake));
		} else {
			yDev.Reconnect();
			manager->wheel = 0;
			manager->gas = 0;
			manager->brake = 0;
		}


		//Nap(.01);
	}
	Lockable mutex;
	DeviceManager *manager;
	Radio *radio;
	TimeValue lastHornPress;
	TimeValue lastRadioPress;
	YaraDevice yDev;
	Joystick   jDev;
};


DeviceManager::DeviceManager(Radio *r) {
	radio = r;
	thread = 0;
	thread = new DeviceThread(this, radio);
	
	Assert(radio);
	Assert(thread);

	lights = false;
	wheel = 0;
	gas = 0;
	brake = 0;

	thread->Start();
}

DeviceManager::~DeviceManager() {
	thread->StopAndJoin();
}

void DeviceManager::SetFan(u8 spd) {
	ScopeLock lock(thread->mutex);
	thread->yDev.SetFan(spd);
}

void DeviceManager::SetVibrate(bool on) {
	ScopeLock lock(thread->mutex);
	thread->yDev.SetVibrate(on);
}


