#include "House.h"
#include "math/Random.h"
#include "platform/GLContext.h"

namespace {

void DrawUnitCube() {
	using namespace GLContext;
	static const v3r pts[] = {
		v3r(0.,0.,0.),
		v3r(1.,0.,0.),
		v3r(1.,1.,0.),
		v3r(0.,1.,0.),
		v3r(0.,0.,1.),
		v3r(1.,0.,1.),
		v3r(1.,1.,1.),
		v3r(0.,1.,1.),
	};

	static const v3r normals[] = {
		v3r(1,0,0),
		v3r(-1,0,0),
		v3r(0,1,0),
		v3r(0,-1,0),
		v3r(0,0,1),
		v3r(0,0,-1),
	};

#define GLQUAD(a,b,c,d,n) \
	Normal(normals[n]); \
	Vertex(pts[a]); \
	Normal(normals[n]); \
	Vertex(pts[b]); \
	Normal(normals[n]); \
	Vertex(pts[c]); \
	Normal(normals[n]); \
	Vertex(pts[d]) 

	GLPrimitive(GL_QUADS) {
		// no need to draw bottom
		//GLQUAD(0,1,2,3, 5);
		GLQUAD(4,5,6,7, 4);
		GLQUAD(0,1,5,4, 3);
		GLQUAD(1,2,6,5, 0);
		GLQUAD(2,3,7,6, 2);
		GLQUAD(3,0,4,7, 1);
	}
}

void DrawUnitPyramid() {
	using namespace GLContext;
	static const v3r pts[] = {
		v3r(0.,0.,0.),
		v3r(1.,0.,0.),
		v3r(1.,1.,0.),
		v3r(0.,1.,0.),
		v3r(.5,.5,1.),
	};

	static const v3r normals[] = {
		v3r(0,0,1),
		v3r(1,0,1),
		v3r(-1,0,1),
		v3r(0,1,1),
		v3r(0,-1,1),
	};

#define GLTRI(a,b,c,n) \
	Normal(normals[n]); \
	Vertex(pts[a]); \
	Normal(normals[n]); \
	Vertex(pts[b]); \
	Normal(normals[n]); \
	Vertex(pts[c])

	GLPrimitive(GL_TRIANGLES) {
		GLTRI(0,1,2, 0);
		GLTRI(0,2,3, 0);
		GLTRI(0,4,3, 2);
		GLTRI(0,1,4, 4);
		GLTRI(1,2,4, 1);
		GLTRI(4,2,3, 3);
	}
}


real randInRange(Random &r, real a, real b) {
	return (b-a)*real(r) + a;
}

}

House::House(u32 s) {
	seed = s;
	mass = 1000;
	width = 1;
	height = 1;
	chimney = false;
	Generate();
}

void House::Generate() {
	Random r(seed);

	width = randInRange(r, 10,22);
	height = randInRange(r,10,22);
	roofSize = randInRange(r,3,10);
	chimney = r;

	wallColor = color(
			randInRange(r,.1,.7),
			randInRange(r,.1,.7),
			randInRange(r,.1,.7),
			1);
	real grey = randInRange(r,.0,.2);
	roofColor = color(grey, grey, grey, 1);

	radius = v2r(width,height).Magnitude();
	//show(width);
	//show(height);
	//show(radius);
}

void House::Draw(const GameObject &cam) {
	//DrawSphere();
	using namespace GLContext;

	GLContext::Material s;
	s.emission=s.ambient=s.diffuse=wallColor;
	s.Apply();

	GLSaveStack() {
		Translate(pos);
		Scale(width, height, -(radius*2));
		GLSaveStack() {
		Translate(-.5,-.5,0);
		DrawUnitCube();
		}
	}
	
	s.emission=s.ambient=s.diffuse=roofColor;
	s.Apply();

	GLSaveStack() {
		Translate(pos);
		Scale(width+1, height+1, roofSize);
		GLSaveStack() {
			Translate(-.5, -.5, 0);
			DrawUnitPyramid();
		}
	}
}

void House::SetOnWorld(const World &world) {
	real avg = (
			world.GetHeight(pos) +
			world.GetHeight(pos + AXIS_X*radius) + 
			world.GetHeight(pos - AXIS_X*radius) +
			world.GetHeight(pos + AXIS_Y*radius) +
			world.GetHeight(pos - AXIS_Y*radius) ) / 5;

	pos[2] = avg+radius/3;
	vel[2] = 0;
	acc[2] = 0;
}

