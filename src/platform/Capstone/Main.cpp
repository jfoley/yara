#include "Main.h"
#include "Capstone.h"
#include "physics/Collision.h"
#include "Vehicle.h"
#include "math/Util.h"
#include "math/Random.h"
#include "physics/PhysicsLoop.h"
#include "Model.h"
#include "concur/Chrono.h"
#include "House.h"
GAME(Capstone);

const real vZoom = .02;
const real MinZoom = 0.0001;
const real MaxZoom = 2;

bool ShouldSimplify = false;

// Naming a variable {anonymous}::time caused ambiguity with time.h method time_t time(time_t)
// Ugly hack to fix for now
#define time __THIS_IS_THE_TIME__

namespace {
	Param config;
	vector<bool> keyState;

	vector<GameObject*> stuff;

	SoundServer *audio;
	Radio *radio;
	SkyBox *sky;
	DeviceManager *devices;
	Vehicle *vehicle;

	Model carModel;

	Camera cam;
	
	real dt;
	real time;

	u32 cameraType;
	real zoomValue;
	real zoomSpeed;
	World world;

	PhysicsLoop physics;
	bool drawWorldNormals=false;
	bool transparentGround=false;
	int povCam=0;
	bool tetherToGround=false;

	void OnConfig() {
		ReloadConfig();
		world.UpdateConfig();
		time = 0;
		cameraType = config.camera.type;
		world.Generate();
	}
	void ChangeNumOctaves(int dx) {
		world.terrainOctaves = CLAMP( i32(world.terrainOctaves) + dx, 0, 30);
		world.Generate();
	}


};

const Param& GetConfig() { return config; }
void ReloadConfig() { config.LoadConfig(config.fileName); }


void Capstone::Resize( int width, int height ){
	float ratio = (float) width / (float) height;
	glShadeModel(GL_SMOOTH);
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);

	glLightModeli (GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
	glLightModeli (GL_LIGHT_MODEL_TWO_SIDE,     GL_TRUE);

	glClearColor(0, .3, .1, 0);

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective( 60.0, ratio, 1.0, config.terrain.size*10 );
}

void Capstone::BeforeLoop(){
	ReloadConfig();

	int argc=0;
	glutInit(&argc, 0);

	keyState.resize(1<<bitsizeof(KeyCode), false);
	gfxrunning=true;
	fps_preferred=60.0f;
	SetWindow(config.window.Width,config.window.Height,config.fullScreen);
	// start GLEW with SetWindow

	OnConfig();
	zoomValue = 1.0;
	zoomSpeed = 0;
	audio = 0;
	radio = 0;
	sky = 0;
	devices = 0;
	vehicle = 0;

	audio = new SoundServer();
	radio = new Radio(audio);
	sky = new SkyBox(config.world.skybox);
	if(!config.no_devices) {
		devices = new DeviceManager(radio);
	}
	vehicle = new Vehicle();

	cam.pos = v3r(-config.terrain.size-2,-config.terrain.size-2,10);
	cam.dir = Orientation(v3r(1,1,0), v3r(0,0,1));

//	physics.Start();
	
	carModel.pos = world.PointOnTerrain(v2r(0,0));
	carModel.Load(config.vehicle.model, config.vehicle.model_scale);
	
//show(carModel.Valid());
//show(carModel.pos);

	//--- SetWindow again to deal with any FBO's we've generated
	SetWindow(config.window.Width,config.window.Height,config.fullScreen);
}

void Capstone::AfterLoop(){
	if(radio)	delete radio;
	if(audio)	delete audio;
	if(sky) delete sky;
	if(devices) delete devices;
	if(vehicle) delete vehicle;

	loop(i, stuff.size())
		delete stuff[i];
	stuff.clear();
}

v3r ComputeNormal(const World& w, const v2r& p){
	real eps=.0001;
	v3r a=w.PointOnTerrain(p + v2r(0,eps));
	v3r b=w.PointOnTerrain(p);
	v3r c=w.PointOnTerrain(p + v2r(eps,0));
	return v3r::Normal(a,b,c).Normalize();
}

bool UpdateVehicle(real dt){
	vehicle->SetAcc(config.physics.gravity * AXIS_Z);


	const v3r &forward = vehicle->GetPOV().dir.F();

	bool movingForward = v3r::DotProduct(forward, vehicle->vel) > 0;

	//--- accelerate
	real amt = keyState[KEY_W] - keyState[KEY_S];
	if(devices)
		amt += devices->gas - devices->brake;
	if(movingForward) {
		amt = CLAMP(amt, -1, 1) * config.vehicle.accel;
	} else {
		amt = CLAMP(amt, 0, 1) * config.vehicle.accel;
	}
	vehicle->AccumAcc(forward * amt);

	//--- turn

	amt = keyState[KEY_A] - keyState[KEY_D];
	if(devices)
		amt += devices->wheel;
	vehicle->AccumTurn(amt/3);

	if(tetherToGround){
		loop(i, 4) { // ew
			GameObject& G=*vehicle->children[i];
			G.onGround=true;
			G.pos=world.PointOnTerrain(v2r(G.pos));
			G.pos[2]+=G.radius;
		}
	} else {
		// Collision detection
		loop(i, vehicle->children.size()){
			GameObject& G=*vehicle->children[i];
			HandleCollision(world, G, dt);
		}
	}

	if( ABS(v2r(vehicle->vel).Magnitude()) < config.vehicle.min_speed)
		vehicle->vel = v3r(0,0,0);
	if( ABS(v2r(vehicle->acc).Magnitude()) < config.vehicle.min_accel)
		vehicle->acc = v3r(0,0,0);

	// Kinematics
	vehicle->PhysUpdate(dt);

	return true;
}

void SetCamera() {
	const real radius = config.terrain.size*.5;
	zoomValue = CLAMP(zoomValue+zoomSpeed,MinZoom,MaxZoom);
	
	switch(cameraType) {
		
		//--- rotate around
		default: case 0:
		{
			cam.pos = v3r(radius*zoomValue*cos(time), radius*zoomValue*sin(time), 100);
			v3r target = v3r(0,0,0);
			v3r dir = target - cam.pos;
			cam.dir = Orientation(dir, v3r(0,0,1));
		}
		break;

		case 1:
		{
			static v2r pos;
			static v2r dir;

			pos=(v2r(cos(time), sin(time))+v2r(1,1))*50;
			dir=v2r(-sin(time), cos(time));

#if 1 // Single pt
			cam.pos=v3r(pos)+AXIS_Z*(world.GetHeight(pos)+.5);
			cam.dir=Orientation(world.GetNormal(pos), dir, Orientation::UPWARD, Orientation::FORWARD);
#else // Tripod with damping shocks

			v2r perp=v2r(dir[1],-dir[0])*3;

			v3r fl=world.PointOnTerrain(pos-perp);
			v3r fr=world.PointOnTerrain(pos+perp);
			v3r bc=world.PointOnTerrain(pos-3*dir);;

			static v3r nrm=AXIS_Z;
			nrm*=4;
			nrm+=v3r::CrossProduct(fr-fl, (fr+fl)/2 - bc);
			nrm/=5;

			cam.pos=v3r(pos)+zoomValue*AXIS_Z*(world.GetHeight(pos)+.6);
			cam.dir=Orientation(nrm, dir, Orientation::UPWARD, Orientation::FORWARD);

			if(world.PointIsOnRoad(cam.pos)) {
				cout << "On Road!\n";
			}

#endif

		}
		break;
		
		//--- top view
		case 2:
		{
			cam.pos = v3r(vehicle->pos[0],vehicle->pos[1],config.terrain.size*zoomValue);
			cam.dir = Orientation(v3r(0,0,-1), v3r(0,1,0));
		}
		break;

		case 3:
		{
			// POV camera
			cam.Follow(vehicle->GetPOV(), povCam==0 ? 10 : 0, .1);
		}
		break;
	}

	cam.pos[2]=MAX(cam.pos[2], world.GetHeight(v2r(cam.pos))+1);
}

void DrawSunAndSky() {
	/* Lighting and Representative Sphere*/
	// Light
	GLContext::Light l0(0);
	l0.diffuse=l0.ambient=color(.6,.9,1,1);
	l0.specular=color(1,1,.7,1);
	l0.pos=1500*v3r(cos(time/1000), 1, sin(time/1000)) + vehicle->pos;
	//l0.pos=v3r(1,sin(time*PI*2)/4,1+cos(time) + cos(time*PI*2)/4)*500;
	l0.Apply();
	l0.Enable();

	// Sphere
	GLContext::Material s;
	s.emission=s.ambient=s.diffuse=color(1,.9,.7,1);
	s.Apply();

	GameObject G;
	G.pos=l0.pos;
	G.radius=20;
	G.Draw(cam);

	sky->SetSize(config.terrain.size * 2);
	sky->Draw(cam);
}

void DrawVehicle() {
	GLContext::Material mVehicle;
	mVehicle.diffuse=mVehicle.ambient=color(.9,.9,.2,1);
	mVehicle.specular=color(1,1,1,1);
	mVehicle.shininess=0.2;
	mVehicle.Apply();

	//if(cameraType != 3 || povCam!=2)
	if(carModel.Valid()) {
		carModel.pos=vehicle->pos;
		carModel.dir=vehicle->dir;
		carModel.Draw(cam);
	} else {
		vehicle->Draw(cam);
	}
}

void DrawNormals() {
	if(!drawWorldNormals)
		return;

	/* Draw Surface Normals */
	GLContext::Material m;
	m.diffuse=m.ambient=color(0,0,1,1);
	m.specular=color(1,1,1,1);
	m.shininess=0.2;
	m.Apply();

	v2r p(vehicle->pos);

	int N=21;

	// Using GetNormal
	loop2(i,j,N,N){
		v2r r=p+v2r(i-N/2,j-N/2);
		v3r w=world.PointOnTerrain(r);
		v3r n=world.GetNormal(r);
		n.Normalize();
		DrawRod(w, w+n, .03);
	}


	m.diffuse=m.ambient=color(1,0,0,1);
	m.Apply();

	// Using CrossProduct
	loop2(i,j,N,N){
		v2r r=p+v2r(i-N/2,j-N/2);
		v3r w=world.PointOnTerrain(r);
		v3r n=ComputeNormal(world, r);
		DrawRod(w, w+n, .03);
	}
}

void DrawTerrain() {
	/* Landscape material */
	static real transparency=1;
	transparency=LERP(transparency, (transparentGround ? .5 : 1), .025);
	GLContext::Material m;
	m.ambient=m.diffuse=color(.5,.5,.5,transparency);
	m.Apply();


	world.Pre();
	world.Draw(vehicle->pos);
	world.Post();
}

void UpdateObjects(v3r pos, v3r forward) {
	real maxDistSq = SQUARE(config.objects.maxDist);
	loop(i, stuff.size()) {
		if(stuff[i] != 0) {
			real distanceSq = (pos - stuff[i]->pos).MagnitudeSquared();
			if(distanceSq > maxDistSq) {
				delete stuff[i];
				stuff[i] = 0;
			}
		}
		if(stuff[i] == 0) {
			stuff.erase(stuff.begin() + i);
			i--;
		}
	}

	loop(i, stuff.size()) {
		GameObject *obj = stuff[i];
		House *house = dynamic_cast<House*>(obj);
		if(house) {
			house->SetOnWorld(world);
		} else {
			HandleCollision(world, *obj, dt);
		}
		
		obj->PhysUpdate(dt);
		obj->vel *= exp(-dt*GetConfig().vehicle.friction);
	}

	const Orientation dir(AXIS_Z, forward, Orientation::UPWARD, Orientation::FORWARD);
	const v3r &fwd = dir.F();
	const v3r &right = dir.R();
	while(stuff.size() < config.objects.count) {
		//--- get random, acceptable distance
		Random r;
		real theta = 2*PI*real(r);
		real dist = real(r) * (config.objects.maxDist - config.objects.minDist) + config.objects.minDist;
		GameObject *p = new House(u32(r));
		
		p->pos = pos + (fwd*cos(theta) + right*sin(theta))*dist;
		p->vel = v3r();
		p->acc = (config.physics.gravity * AXIS_Z);
		stuff.push_back(p);
	}

	loop(i, stuff.size()) {
		for(int j=i+1; j<int(stuff.size()); j++) {
			HandleCollision(*stuff[i], *stuff[j], dt);
		}
		loop(j, vehicle->children.size()) {
			HandleCollision(*stuff[i], *vehicle->children[j], dt);
		}
	}
}

void DrawObjects() {
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	loop(i, stuff.size()) {
		real a=i*2*PI/stuff.size();

		GLContext::Material m;
		m.ambient=m.diffuse=color(
				(1+cos(a+0*PI/3))/2,
				(1+cos(a+2*PI/3))/2,
				(1+cos(a+4*PI/3))/2,
				.4);
		m.Apply();

		if(stuff[i])
			stuff[i]->Draw(cam);
	}
}

void Capstone::Redraw(){
	using namespace GLContext;

	dt = 1./fps_preferred;
	time += dt;

	UpdateVehicle(dt);
	SetCamera();
	UpdateObjects(vehicle->GetPOV().pos, vehicle->GetPOV().dir.F());

	cam.Clear();
	cam.Look();

	DrawVehicle();
	DrawSunAndSky();
	DrawNormals();
	DrawTerrain();
	DrawObjects();


	
	once_every(30){
		cout << "Vehicle Position " << vehicle->pos << "\n";
		printf("fps=%f, %3.2f %% err", fps_actual, (fps_preferred-fps_actual)/fps_preferred*100);
		printf(" sounds=%d", audio->NumSounds());
		printf("\n");
	}
}


void Capstone::KeyDn(KeyCode key){ 
	if(key==KEY_ESCAPE)
		Quit();
	if(key==KEY_F4) // F4
		ToggleFullScreen();
	if(key >= KEY_0 && key <= KEY_9) {
		cameraType = key - KEY_0;
		show(cameraType);
	}
	if(key == KEY_TAB) {
		world.terrainSeed++;
		world.Generate();
	}
	if(key==KEY_K) {
		zoomSpeed = -vZoom;
	}
	if(key==KEY_J) {
		zoomSpeed = vZoom;
	}
	if(key==KEY_F5) {
		OnConfig();
	}
	if(key==KEY_LSHIFT) {
		ChangeNumOctaves(+1);
	} else if(key == KEY_RSHIFT) {
		ChangeNumOctaves(-1);
	}

	if(key==KEY_T)
		transparentGround=!transparentGround;

	if(key==KEY_F){
		povCam++;
		povCam%=3;
	}
	if(key==KEY_G){
		tetherToGround=!tetherToGround;

	}

	if(key==KEY_N){
		drawWorldNormals=!drawWorldNormals;
	}

	keyState[key] = true;
}

void Capstone::KeyUp(KeyCode key){ 
	if(key == KEY_K || key == KEY_J) {
		zoomSpeed = 0;
	}
	
	if(key == KEY_SPACE) {
		ShouldSimplify = !ShouldSimplify;
		world.Generate();
	}
  
	keyState[key] = false;
}

