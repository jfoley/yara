
#ifndef __VEHICLE_H
#define __VEHICLE_H

#include "Capstone.h" // for GetConfig() access
#include "gob/GameObject.h"
#include "physics/Linkage.h"
#include "physics/SpringLinkage.h"


SpringLinkage* QuickSpring(GameObject* A, GameObject* B, real k, real c=0, real m=0){
	SpringLinkage* l=new SpringLinkage;
	l->x0=(B->pos-A->pos).Magnitude();
	l->v0=(B->vel-A->vel).Magnitude();
	l->a0=(B->acc-A->acc).Magnitude();

	l->k=k;
	l->c=c;
	l->m=m;

	l->gob[0]=A;
	l->gob[1]=B;

	return l;
}

#define MAXIMUM_VEHICLE_VELOCITY 30 // ~67 mph

v3r clamp(const v3r& v, real lo, real hi){
	real mag=v.Magnitude();
	if(mag==0)
		return v;
	if(mag < lo)
		return v*(lo/mag);
	if(mag > hi)
		return v*(hi/mag);
	return v;
}



class Vehicle: public GameObject{
	real length;
	real width;
	real height;

	public:
		Vehicle(){
			// Reasonable car dimentions
			mass=2000;
			length=5.0;
			width=2.8;
			height=1.4;

			pos=v3r(0,0,50);
			dir=Orientation(AXIS_X, AXIS_Z);

			Construct();
		}

		void SetPosition(const v3r& base){
			if(dir.U()[2]<=0)
				dir.Set(dir.F(), AXIS_Z);
			
			v3r init[]={
				dir.R()*-width/2+dir.F()*-length/2,
				dir.R()*+width/2+dir.F()*-length/2,
				dir.R()*+width/2+dir.F()*+length/2,
				dir.R()*-width/2+dir.F()*+length/2,
				dir.U()*height,
			};
			loop(i,children.size())
				children[i]->pos=init[i] + base;
		}

		void Construct(){
			
			const int N=5;

			loop(i,N){
				GameObject* G = new GameObject;
				G->radius=i<N-1 ? .5 : height;
				G->stiffness=0.1;
				G->mass=mass/N;
				G->onGround=false;
				children.push_back(G);
			}

			mass=0;

			SetPosition(pos);

			// Bottom Square
			loop(i,4){
				joints.push_back(QuickSpring(
							children[(i+0)%4],
							children[(i+1)%4],
							50000, 1000));
			}

			// Criss-Cross
			loop(i,2){
				joints.push_back(QuickSpring(
							children[(i+0)%4],
							children[(i+2)%4],
							50000, 1000));
			}

			// Roof
			loop(i,4){
				joints.push_back(QuickSpring(
							children[i],
							children[4],
							500000, 1000));
			}

		}

		void Destruct(){
			// Disconnect joints
			loop(i, joints.size())
				delete joints[i];
			joints.resize(0);

			// Delete child nodes
			loop(i, children.size())
				delete children[i];
			children.resize(0);
		}



		const GameObject& GetPOV() const { return *children[4]; }
		GameObject& GetPOV() { return *children[4]; }


		virtual void PhysUpdate(real dt){
			// Apply Joints
			loop(i, joints.size())
				joints[i]->Apply();

			// Component Kinematics
			loop(i, children.size()){
				children[i]->PhysUpdate(dt);
				if(children[i]->onGround)
					children[i]->vel*=exp(-dt * GetConfig().vehicle.friction); // Velocity decay
				children[i]->vel=clamp(children[i]->vel,0,GetConfig().vehicle.maxSpeed);
			}

			// Tire grip hack for forward motion only
			loop(i, children.size()){
				if(children[i]->onGround){
					v3r& v=children[i]->vel;
					v=LERP(v, v3r::Projection(v, dir.F()),.5);
				}
			}

			pos=COMpos();
			vel=COMvel();
			acc=COMacc();
			dir=Orientation(
					(children[2]->pos + children[3]->pos) - (children[0]->pos + children[1]->pos), // Forward
					v3r::CrossProduct(children[0]->pos - children[1]->pos, children[2]->pos - children[1]->pos) // Up
					);

			GetPOV().dir=dir;

			if(GetPOV().pos[2] < pos[2])
				SetPosition(pos);
		}

		void SetAcc(const v3r& a=v3r(0)){
			loop(i, children.size())
					children[i]->acc=a;
		}

		void AccumAcc(const v3r& a){
			loop(i, children.size())
				if(children[i]->onGround)
				children[i]->acc+=a;
		}

		void AccumTurn(real left){
			for(int i=2; i<=3; i++)
				children[i]->pos+=GetPOV().dir.R() * left * v3r::DotProduct(vel, dir.F())/50;
		}

		virtual void Draw(GameObject& cam){
			loop(i, children.size())
				children[i]->Draw(cam);

			loop(i, joints.size())
				joints[i]->Draw(cam);
		}

		virtual ~Vehicle(){
			Destruct();
		}
};

#endif
