#ifndef __MODEL_H
#define __MODEL_H

#include "gob/GameObject.h"
#include <assimp.hpp>
#include <aiScene.h>
#include <aiPostProcess.h>
#include "platform/GLContext.h"
#include "platform/Texture.h"
#include <map>

/// AssImp models
class Model : public GameObject {
	public:
		Model();
		bool Valid() const { return data != 0 && data->mRootNode != 0; }
		void Clear();
		bool Load(const string &fileName, float scale=1.0f);
		void Draw(const GameObject &cam);
		void SetPosition(v3r pt);

	private:
		void Render(const aiScene *, const aiNode *);
		void ApplyMaterial(const aiMaterial *mtl);
		bool FetchTextures();

		v3r size;
		float scale;
		string basePath;
		Texture *lastTex;
		const aiScene *data;
		vector<Texture*> textures;
		std::map<string, Texture*> loaded_textures;
};



#endif


