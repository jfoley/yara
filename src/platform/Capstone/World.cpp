#include "World.h"
#include "Capstone.h"
#include "PerlinTerrain.h"
#include "math/Random.h"
#include "platform/GLContext.h"
#include "platform/Texture.h"
#include "ChunkyWorld.h"


class WorldData {
	public:
		//--- Collision data, much higher res than terrain data itself
		HeightMap collisionData;
		//--- chunked world data
		ChunkyWorld worldData;
	private:

};

World::World() {
	data = 0;

	worldSize = 1024;

	terrainSeed = 0;
	terrainOctaves = 7;
}

World::~World() {
	if(data)
		delete data;
}

void World::UpdateConfig() {
	const Param& config = GetConfig();
	worldSize = config.terrain.size;

	terrainOctaves = config.terrain.octaves;

	//show(worldSize);

	//show(terrainSeed);
	//show(terrainOctaves);
}

void World::Generate() {
	if(data) {
		delete data;
		data = 0;
	}

	data = new WorldData();
	const Param &config = GetConfig();

	//--- generate terrain & collision data
	data->collisionData = PerlinTerrain(terrainSeed, worldSize, config.collision.detail, terrainOctaves);
	
	//--- ensure no seams have appeared
	data->collisionData.MakeSeamless();
	data->collisionData.CalculateNormals();
	
	//--- generate chunks from collision data to draw
	u32 terrainChunks = config.terrain.chunks;
	u32 textureDetail = config.terrain.texdetail;
	data->worldData = ChunkyWorld(worldSize, terrainChunks, textureDetail);
	data->worldData.InitFromTerrain(data->collisionData);
}

void World::Pre() const {
	if(!data) return;
}

void World::Post() const {
	if(!data) return;
}

void World::Draw(const v2r &pos) const {
	if(!data) return;
	
	data->worldData.Draw(pos);
	
//#define ANGRY 1
#if ANGRY
	glDisable(GL_LIGHTING);
	glPointSize(10);
	GLContext::Color(1,1,1,1);
	data->collisionData.DrawPoints();
	glEnable(GL_LIGHTING);
#endif
}

real World::GetHeight(const v2r &pos) const {
	if(!data)
		return 0;

	return data->collisionData.ApproximateHeight(pos);
}

v3r World::GetNormal(const v2r &pos) const {
	if(!data)
		return AXIS_Z;

	return data->collisionData.ApproximateNormal(pos);
}

v3r World::PointOnTerrain(const v3r &pt) const {
	if(!data)
		return pt;

	return v3r( pt[0], pt[1], GetHeight(v2r(pt[0], pt[1])) );
}


