
#ifndef __CAPSTONE_MAIN_H
#define __CAPSTONE_MAIN_H

#include "platform/platform.h"
#include "platform/GLContextHandler.h"
#include "platform/SkyBox.h"
#include "audio/SoundServer.h"
#include "gob/Camera.h"
#include "World.h"
#include "Param.h"
#include "Radio.h"
#include "DeviceManager.h"

class Capstone: public GLContextHandler{
	public:
		void Resize(int w, int h);

		void BeforeLoop();

		void AfterLoop();
		void Redraw();

		string GetName() const { return "Capstone"; }

		void Quit(){ gfxrunning=false; }
		void KeyDn(KeyCode key);
		void KeyUp(KeyCode key);
		bool done;
};

#endif
