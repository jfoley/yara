#ifndef __DEVICEMANAGER_H
#define __DEVICEMANAGER_H

#include "Radio.h"

struct DeviceThread;

class DeviceManager {
	public:
		DeviceManager(Radio *r);
		~DeviceManager();

		//--- As output device
		void SetFan(u8 fanSpeed);
		void SetVibrate(bool on);

		volatile bool lights;
		volatile real wheel;
		volatile real gas;
		volatile real brake;
	
	private:
		DeviceThread *thread;
		Radio *radio;
};



#endif

