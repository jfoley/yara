#ifndef __CAPSTONE_H
#define __CAPSTONE_H

#include "Param.h"

const Param& GetConfig();
void ReloadConfig();
extern bool ShouldSimplify;

#endif

