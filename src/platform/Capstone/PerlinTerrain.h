#ifndef __PERLINTERRAIN_H
#define __PERLINTERRAIN_H

#include "platform/HeightMap.h"

HeightMap PerlinTerrain(u32 seed, real size, u32 detail, u32 numOctaves);

#endif

