#ifndef __RADIO_H
#define __RADIO_H

#include "macros.h"
#include "audio/SoundServer.h"

typedef vector< vector<string> > SongData;

class Radio : public RepeaterThread {
	public:
		Radio(SoundServer*);
		virtual ~Radio() { }
		virtual void Rep();

		string GetNextSong();
		string GetCurrentSong();
		void SetChannel(u32 i);
		
		void SetPower(bool on);
		void TogglePower() { SetPower(!power); }
		
		SoundServer *soundServer;

	private:
		void ReloadSongData();
		int& CurrentSound() { return currentSound[currentChannel]; }
		u32& CurrentFile() { return currentFile[currentChannel]; }

		bool power;
		u32  currentChannel;
		vector<u32> currentFile;
		vector<int> currentSound;
		SongData songData;
};





#endif

