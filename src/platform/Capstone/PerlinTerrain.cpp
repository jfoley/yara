
#include "PerlinTerrain.h"
#include "math/Perlin.h"

class PerlinTF : public TerrainFunc {
	public:
		PerlinTF(u32 seed, real o, real a)
		  : noise(o, seed), octave(o), amplitude(a) { }

		real eval(real in, real x, real y) const;
		virtual real operator()(real h, real x, real y) const { return eval(h,x,y); }
	private:
		Perlin noise;
		real octave;
		real amplitude;
};

real PerlinTF::eval(real h, real x, real y) const {
	//--- won't wrap without 0-1 of HeightMap scaling to the 0-Period of noise
	const real T = noise.GetPeriod();
	const real ix = x*octave*T;
	const real iy = y*octave*T;

	const real z = noise(ix, iy, 1);
	return (amplitude*z) + h;
}

HeightMap PerlinTerrain(u32 seed, real size, u32 detail, u32 numOctaves) {
	HeightMap result(size, detail, 0);

	real amplitude = 8.0;
	real period = 1;
	loop(i, numOctaves) {
		PerlinTF noise(seed, period, amplitude);
		result.AddFunc(noise);
		
		//--- rather than as a function of i
		period    *= 2;
		amplitude = 16 / period;
	}
	if(numOctaves) {
		result.SetMagnitude(9*numOctaves);
		//show(result.MaxHeight());
		//show(result.MinHeight());
	}

	return result;
}

