
#include <cstdio>
#include <cstdlib>

#include "platform.h"
#include "util/Arguments.h"
#include "util/parse.h"
#include "filesystem/FileSystem.h"
#include "filesystem/Path.h"

#include "macros.h"
#include <vector>
typedef std::vector<GLContextHandler*> gamelist;
gamelist* games=0;

void AddGame(GLContextHandler* g){
	if(!games)
		games=new gamelist;
	games->push_back(g);
}

#include <iostream>
using namespace std;

int main(int argc, char* argv[]){
	GLContextHandler* game=0;

	ArgumentParser argParse;
	argParse.DefineOption("gameId", "i", "The id of the game to select, skips menu.");
	argParse.DefineOption("directory", "d", "The root directory of the game.");
	Option<Arguments> results = argParse.Parse(argc,argv);

	if(!results.Valid()) {
		std::cout << "\n";
		argParse.PrintUsage(std::cout, argv[0]);
		return -1;
	}

	Arguments args(results);
	unsigned autoselect = util::parse<unsigned>( args.GetOption("gameId").GetOrElse("0"));
	ChangeDir(args.GetOption("directory").GetOrElse("."));

	show(Path("."));

	if(games){
		if(games->size()==1){
			game=(*games)[0];
			cout << "Selecting default game.\n";
		}
		else{
			if(autoselect>0 && autoselect<=games->size()){
				game=(*games)[autoselect-1];
			}
			else{
				loop(i,games->size()){
					cout << (i+1) << ".\t"<< (*games)[i]->GetName() << "\n";
				}
				unsigned int i=0;
				while(i<1 || i>games->size()){
					cout << "Please select a game (1.." << games->size() << "): ";
					cin >> i;
					game=(*games)[i-1];
				}
			}
		}
		cout << "Loading game: " << game->GetName() << "\n";
	}
	int result;
	if(game){
		loop(i,games->size()){
			if((*games)[i]!=game)
				delete (*games)[i];
		}
		delete games;
		result=game->Run(argc,argv);
		delete game;
	}
	else{
		cout << "No games available.\n";
		result=-1;
	}
	return result;
}









