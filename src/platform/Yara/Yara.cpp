
#include "Yara.h"
GAME(Yara);

#include "platform/GLContext.h"
#include <cstdio>
#include "input/YaraDevice.h"

#include "types.h"

#include "TGATexture.h"
#include "math/Random.h"
#include "macros.h"


#include "gob/GameObject.h"
#include "gob/SpatialHash.h"

class MarioTree: public GameObject{
	public:
		void Draw(const Camera& cam);
};

void MarioTree::Draw(const Camera& cam){
	static float hit;
	static bool hitt;

	glPushMatrix();
	glTranslatef(pos[0],pos[1],pos[2]);
	{
		static int treecalllist=0;

		if(!treecalllist){
			treecalllist=glGenLists(1);
			if(!treecalllist)
				return;

			glNewList(treecalllist, GL_COMPILE_AND_EXECUTE);

			GLfloat radius=.1;
			GLfloat height=1;
			GLfloat di=.5;

			GLfloat black[]={0,0,0,1};
			GLfloat brown[]={.5,.25,.125,1};
			glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, black);
			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, brown);

			glBegin(GL_QUADS);
			for(GLfloat i=0.0; i<=3.14159*2; i+=di){
				glNormal3f(cos(i   ), sin(i   ),0); glVertex3f(radius*cos(i   ), radius*sin(i   ), -1);
				glNormal3f(cos(i+di), sin(i+di),0); glVertex3f(radius*cos(i+di), radius*sin(i+di), -1);
				glNormal3f(cos(i+di), sin(i+di),0); glVertex3f(radius*cos(i+di), radius*sin(i+di), height);
				glNormal3f(cos(i   ), sin(i   ),0); glVertex3f(radius*cos(i   ), radius*sin(i)   , height);
			}
			glEnd();


			glEndList();
		}
		if(treecalllist)
			glCallList(treecalllist);
	}
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, 
			( hitt ? 
			  v4f(0,0,1).data :
			  LERP(v4f(1,0,0) , v4f(0,.7,0), 1-hit).data
			)			
			);
	glTranslatef(0,0,1.5);
	glScalef(1,1,2);
	glutSolidSphere(.4, 20, 20);
	glPopMatrix();
}


class Sun: public GameObject {
	public:
	Sun(double t){ pos=v3r(200,200*sin(t),200*cos(t)); }
	void Draw(const Camera& cam)
	{
		glPushMatrix();
		glTranslatef(pos[0],pos[1],pos[2]);

		glLightModeli (GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
		glLightModeli (GL_LIGHT_MODEL_TWO_SIDE,     GL_TRUE);

		glLightModelfv(GL_LIGHT_MODEL_AMBIENT, v4f(.1,.1,.1,1).data);
		glLightfv(GL_LIGHT0, GL_AMBIENT,  v4f(.1,.1,.1, 1).data);
		glLightfv(GL_LIGHT0, GL_DIFFUSE,  v4f( 1, 1,.8, 1).data);
		glLightfv(GL_LIGHT0, GL_SPECULAR, v4f( 1, 1,.8, 1).data);
		glLightfv(GL_LIGHT0, GL_POSITION, v4f( 0, 0, 0, 1).data);
	
		glEnable(GL_LIGHTING);
		glMaterialfv(GL_FRONT, GL_EMISSION, v4f(1,1,.5,1).data);
		glutSolidSphere(14.0, 50,50);
		glDisable(GL_LIGHTING);

		glPopMatrix();
	}
};



void drawwater();
void drawlandscape();

namespace glbl
{
	double time=0;
	string resourceDirectory;
	Random uniformpdf(9);
	
	GLuint textureGrass;
	GLuint textureWater;
};


YaraDevice picio;
InputDeviceUpdater idu(&picio);

static bool keys[256]={false};

void Yara::Resize( int width, int height ){
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_SMOOTH);

	glClearColor(0, 0, 0, 0);

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60, width*1.0/height, .1, 500);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

bool Yara::Init(int& argc, char* argv[]){
	glutInit(&argc,argv);
	return true;
}


void Yara::BeforeLoop(){
	gfxrunning=true;
	fps_preferred=30.0f;
	SetWindow(800,450,false);
	picio.calibration=Calibration(-512,511, -1,1);
	idu.Start();

	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
	glLineWidth(1.5);
	glbl::textureWater=LoadTGATexture(TGAImg(string("src/platform/Yara/res/textures/water.tga").c_str()));
	glbl::textureGrass=LoadTGATexture(TGAImg(string("src/platform/Yara/res/textures/grass.tga").c_str()));
}


void Yara::AfterLoop(){}

void Yara::KeyDn(KeyCode key){ 
	if(key==KEY_ESCAPE)
		Quit();
	if(key==KEY_F4) // F4
		ToggleFullScreen();
	keys[key - KEY_A + 'a']=true;
}

void Yara::KeyUp(KeyCode key){ 
	keys[key - KEY_A + 'a']=false;
}


float randf(){return glbl::uniformpdf;}


static Camera camera;


void Yara::Redraw(){
	glClearColor (.05, 0.4*cos(glbl::time)+.3, 0.4*cos(glbl::time)+.4+.2, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
		glLoadIdentity();
	v3r c=camera.pos;
	v3r d=v3r(c)+camera.dir.GetForward();
	gluLookAt(
			c[0],c[1],c[2],
			d[0],d[1],d[2],
			0,0,1);

	Sun s(glbl::time);
	s.Draw(camera);

	drawwater();
	drawlandscape();

	GLfloat dt=0.01;
	glbl::time+=dt;
	static GLfloat p[4]={0};

	p[3]=keys['w']-keys['s']+picio.GetCalibratedAnalog(0); // speed
	p[2]=keys['d']-keys['a']+picio.GetCalibratedAnalog(1); // yaw
	p[1]=keys['e']-keys['q']+picio.GetCalibratedAnalog(2); // height
	p[0]=LERP(p[0],keys['r']-keys['f']+picio.GetCalibratedAnalog(3),.5); // pitch

	camera.pos+=camera.dir.GetForward()*p[3];
	camera.pos[2]=LERP(camera.pos[2],p[1]*10+4,.5);
	camera.dir=Orientation(camera.dir.GetForward()*v3r(1,1,0)+v3r(0,0,p[0]),v3r(0,0,1));
	camera.dir.Increment(-p[2]*dt*10,0,0);

	once_every(30){
		printf("fps=%f, %3.2f %% err\n", fps_actual, (fps_preferred-fps_actual)/fps_preferred*100);
	}
}


void drawwater(){
	glPushMatrix();
	glTranslatef(0,fmod(glbl::time,1),-.01+sin(glbl::time*5)/2);
	
	v4f watercolor(1,1,1);
	watercolor*=(cos(glbl::time)+1.2)/2.2;

	GLfloat s=500;

	glEnable(GL_TEXTURE_2D);
	glColor3fv(watercolor.data);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glBindTexture(GL_TEXTURE_2D, glbl::textureWater);
	
	glBegin(GL_QUADS);
	{
		glTexCoord2f( s,-s); glNormal3f(0,0,1); glVertex3f(s,-s,0);
		glTexCoord2f( s, s); glNormal3f(0,0,1); glVertex3f(s,s,0);
		glTexCoord2f(-s, s); glNormal3f(0,0,1); glVertex3f(-s,s,0);
		glTexCoord2f(-s,-s); glNormal3f(0,0,1); glVertex3f(-s,-s,0);
	}
	glEnd();
	
	glDisable(GL_TEXTURE_2D);
	glFlush();
	glPopMatrix();
}

void drawlandscape(){
	static const int w=201;
	static int done=0;
	static GLfloat a[w][w]={{0}};
	static v4f n[w][w];
	if(!done){
		// Random Data
		for(int i=0; i<w-1; i++)
			for(int j=0; j<w-1; j++)
				a[i][j]+=(randf()-.5);
		// Smooth Data
		for(int c=0; c<10; c++){
			for(int i=1; i<w-1; i++){
				for(int j=1; j<w-1; j++){
					a[i][j]+=a[i+1][j]+a[i][j+1]+a[i-1][j]+a[i][j-1];
					a[i][j]/=4;
				}
			}
		}
		// Calculate Normals
		for(int i=0; i<w-1; i++){
			for(int j=0; j<w-1; j++){
				n[i][j]=v4f::CrossProduct(v4f(1,0,a[i+1][j]-a[i][j]), v4f(0,1,a[i][j+1]-a[i][j]));
			}
		}
		done=1;
	}


	static SpatialHash sh(10, PI/2, 200);

	once(){
		for(int i=0; i<300; i++){
			int x=200*randf();
			int y=200*randf();
			if(a[x][y]<0){
				i--;
				continue;
			}
			MarioTree* tree=new MarioTree;
			tree->pos=v4f(x,y,a[x][y]);
			sh.Insert(tree);
		}
	}

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_TEXTURE_2D);
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, v4f(1,1,1).data);
	glBindTexture(GL_TEXTURE_2D, glbl::textureGrass);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glBegin(GL_QUADS);
	glColor3f(1,.5,0);
	for(int i=0; i<w-1; i++){
		for(int j=0; j<w-1; j++){
			glTexCoord2f(0,0); glNormal3fv((GLfloat*)&n[i+0][j+0]); glVertex3f(i+0,j+0, a[i+0][j+0]);
			glTexCoord2f(0,1); glNormal3fv((GLfloat*)&n[i+0][j+1]); glVertex3f(i+0,j+1, a[i+0][j+1]);
			glTexCoord2f(1,1); glNormal3fv((GLfloat*)&n[i+1][j+1]); glVertex3f(i+1,j+1, a[i+1][j+1]);
			glTexCoord2f(1,0); glNormal3fv((GLfloat*)&n[i+1][j+0]); glVertex3f(i+1,j+0, a[i+1][j+0]);
		}
	}	
	glEnd();
	glFlush();
	glDisable(GL_TEXTURE_2D);

	sh.Draw(camera);

	glDisable(GL_LIGHTING);
}







