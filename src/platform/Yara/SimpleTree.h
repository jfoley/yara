// SimpleTree.h

#ifndef __SIMPLETREE_H
#define __SIMPLETREE_H

#include "macros.h"
#define LIN LERP
#include "SpatialObject.h"

class Tree: public SpatialObject{
	public:
	float hit;
	bool hitt;
	Tree(const v4f& pos){
		position=pos;
		hit=0;
		hitt=false;
	} 
	
	virtual void draw(){
		glPushMatrix();
		glTranslatef(position[0],position[1],position[2]);
		drawTree();

		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, 
				
				( hitt ? 
				  v4f(0,0,1).data :
					LIN(v4f(1,0,0) , v4f(0,.7,0), 1-hit).data
				)			
				);
		glTranslatef(0,0,1.5);
		glScalef(1,1,2);
		glutSolidSphere(.4, 20, 20);

		glPopMatrix();
	}

	private:
	static void drawTree(){
		static int treecalllist=0;

		if(!treecalllist){
			treecalllist=glGenLists(1);
			if(!treecalllist)
				return;

			glNewList(treecalllist, GL_COMPILE_AND_EXECUTE);

			GLfloat radius=.1;
			GLfloat height=1;
			GLfloat di=.5;

			GLfloat black[]={0,0,0,1};
			GLfloat brown[]={.5,.25,.125,1};
			glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, black);
			glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, brown);

			glBegin(GL_QUADS);
			for(GLfloat i=0.0; i<=3.14159*2; i+=di){
				glNormal3f(cos(i   ), sin(i   ),0); glVertex3f(radius*cos(i   ), radius*sin(i   ), -1);
				glNormal3f(cos(i+di), sin(i+di),0); glVertex3f(radius*cos(i+di), radius*sin(i+di), -1);
				glNormal3f(cos(i+di), sin(i+di),0); glVertex3f(radius*cos(i+di), radius*sin(i+di), height);
				glNormal3f(cos(i   ), sin(i   ),0); glVertex3f(radius*cos(i   ), radius*sin(i)   , height);
			}
			glEnd();


			glEndList();
		}
		if(treecalllist)
			glCallList(treecalllist);
	}
};


#endif
