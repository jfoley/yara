// TGATexture.h
// TGAImg.h

#ifndef __TGAIMG_H
#define __TGAIMG_H

#include "types.h"
#include "math/seq.h"
class TGAImg
{
	public: // Every member is public on purpose.
		v4f* data;
		u16 width;
		u16 height;

		TGAImg(const char* fname=NULL) : data(0) { init(fname);	}
		~TGAImg(){ init(); }
		bool ready() const { return data; }
		void init(const char* fname=NULL);
};

// TGAImg.cpp

#pragma pack(push)
#pragma pack(1)
struct TGAHeader
{
	u8 idLength;
	u8 colorMapType;
	u8 imageType;

	// Color Map Spefication
	u16 colorMapOffset;
	u16 colorMapLength;
	u8 colorMapDepth;

	// Image Specification
	u16 originx;
	u16 originy;
	u16 width;
	u16 height;
	u8 depth;
	u8 imageDescriptor;
};
#pragma pack(pop)

void TGAImg::init(const char* fname)
{
	if(ready())
	{
		delete[] data;
		data=0;
	}
	if(!fname)
		return;
	FILE* f=fopen(fname, "rb");
	if(f<=0)
		return;
	TGAHeader header;
	int r=fread(&header, sizeof(TGAHeader), 1, f);
	r++;

	if(header.imageType!=2)
	{
		//DEBUG(0, "TGA Image is not a true-color image.");
		return;
	}
	/*
	   printf("Color Map Type = %d\n", header.colorMapType);
	   printf("Image Type     = %d\n", header.imageType);
	   printf("CMap Offset    = %d\n", header.colorMapOffset);
	   printf("CMap Length    = %d\n", header.colorMapLength);
	   printf("CMap Depth     = %d\n", header.colorMapDepth);
	   printf("XY Offsets     = %d,%d\n", header.originx, header.originy);
	   printf("(%d,%d)\n", header.width, header.height);
	   printf("Pixel Depth    = %d\n", header.depth);
	   // */
	width=header.width;
	height=header.height;
	int depth=header.depth/8;
	if(depth!=4 && depth!=3)
	{
		//DEBUG(0, "Non 24-bit/32-bit *.tga file.");
		return;
	}

	if(depth==4)
	{
		//DEBUG(0, "TODO: 32-bit *.tga files not implemented yet.");
		return;
	}
	// Allocate memory to store byte values, and vector4f's
	u8* raw_data=new u8[width*height*depth];
	data=new v4f[width*height];

	// Set file position to the first byte of the color data and read all of it into raw_data
	fseek(f, sizeof(TGAHeader)+header.idLength+header.colorMapLength*header.colorMapDepth/8, SEEK_SET);
	int a=fread(raw_data, depth, width*height, f);
	a++;

	// Store the color information as vector4f's
	if(depth==3)
		for(int index=0; index<width*height; index++)
			data[index]=v4f(
					raw_data[index*depth+2]/255.0,
					raw_data[index*depth+1]/255.0,
					raw_data[index*depth+0]/255.0,
					1.0
					);

	// Clean up
	delete[] raw_data;
	fclose(f);
}

#endif

#ifndef __TGATEXTURE_H
#define __TGATEXTURE_H


GLuint LoadTGATexture(const TGAImg& img);
void UnloadTGATexture(GLuint texName);

void UnloadTGATexture(GLuint texName)
{
	if(!texName)
		return;
	glDeleteTextures(1, &texName);
}

GLuint LoadTGATexture(const TGAImg& img){
	if(!img.ready())
		return 0;


	GLuint texName;
	glGenTextures(1, &texName);

	if(texName<=0)
		return 0;

	glBindTexture(GL_TEXTURE_2D, texName);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.width, img.height, 0, GL_RGBA, GL_FLOAT, img.data);
	return texName;
}
#endif
