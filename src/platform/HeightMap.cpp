#include "HeightMap.h"
#include "Assert.h"
#include "math/Util.h"

HeightMap::HeightMap(real wSize, u32 resolution, real initial) {
	worldSize = wSize;
	//dataSize-1 * dataSize-1 must fit in a u32 for VBO/VA
	dataSize = CLAMP(resolution, 1u, 65535u);

	const i32 len = SQUARE(dataSize);
	data.resize(len);
	loop(i,len) { data[i] = initial; }
}

void HeightMap::SetMagnitude(const real f) {
	real max = 1;
	real min = 0;
	loop(i,SQUARE(dataSize)) {
		min = MIN(data[i], min);
		max = MAX(data[i], max);
	}

	real amplitude = max - min;
	real offset = 0;
	if(min < 0)
		offset -= min;
	//show(amplitude);
	//show(offset);
	loop(i, SQUARE(dataSize)) {
		data[i] += offset;
		data[i] *= f/amplitude;
	}
}

real HeightMap::MinHeight() const {
	real min = 0;
	loop(i,SQUARE(dataSize)) {
		min = MIN(data[i], min);
	}
	return min;
}

real HeightMap::MaxHeight() const {
	real max = 0;
	loop(i,SQUARE(dataSize)) {
		max = MAX(data[i], max);
	}
	return max;
}

//--- apply function to set of points, add result to heightmap
void HeightMap::AddFunc(const TerrainFunc &func) {
	loop2(x,y,dataSize,dataSize) {
		real xi = RATIO(x,dataSize-1);
		real yi = RATIO(y,dataSize-1);
		Get(x,y) = func(Get(x,y), xi, yi);
	}
}

void HeightMap::MakeSeamless() {
	loop(x,dataSize)
		Get(x,dataSize) = Get(x,0);
	loop(y,dataSize)
		Get(dataSize,y) = Get(0,y);
}

void HeightMap::Sample(const HeightMap &other, v2r offset, real size) {
	normals.resize(NumVertices());
	loop2(x,y,dataSize,dataSize) {
		v2r p = offset + size*v2r(RATIO(x,dataSize-1), RATIO(y,dataSize-1) );

		u32 id = Index(x,y);
		data[id] = other.ApproximateHeight(p);
		normals[id] = other.ApproximateNormal(p);
	}
}

void HeightMap::PerfectSample(const HeightMap &other, v2i offset) {
	//--- make room for normals
	normals.resize(NumVertices());
	loop2(x,y,dataSize,dataSize) {
		i32 index = Index(x,y);
		data[index] = other.Get(x+offset[0], y+offset[1]);
		normals[index] = other.GetNormal(x+offset[0], y+offset[1]);
	}
}

real HeightMap::ApproximateHeight(const v2r &p) const {
	v2r wrapped = Wrap(p, worldSize);
	v2r pos = wrapped/Unit();

	i32 x = floor(pos[0]);
	i32 y = floor(pos[1]);

	real dx = pos[0] - x;
	real dy = pos[1] - y;

	real h0 = Get(x, y);
	real h1 = Get(x+1, y);
	real h2 = Get(x, y+1);
	real h3 = Get(x+1, y+1);

	real h01 = LERP(h0, h1, dx);
	real h23 = LERP(h2, h3, dx);
 
	return LERP(h01,h23,dy);
}

v3r HeightMap::ApproximateNormal(const v2r &p) const {
	v2r wrapped = Wrap(p, worldSize);
	v2r pos = wrapped/Unit();

	i32 x = pos[0];
	i32 y = pos[1];

	real dx = pos[0] - x;
	real dy = pos[1] - y;

	v3r n0 = GetNormal(x, y);
	v3r n1 = GetNormal(x+1, y);
	v3r n2 = GetNormal(x, y+1);
	v3r n3 = GetNormal(x+1, y+1);

	v3r n01 = LERP(n0, n1, dx);
	v3r n23 = LERP(n2, n3, dx);

	return LERP(n01,n23,dy);
}

void HeightMap::CalculateNormals() {
	normals.resize(NumVertices());

	loop2(x,y,dataSize,dataSize) {
		normals[Index(x,y)] = v3r(-Get(x+1,y)+Get(x-1,y), -Get(x,y+1)+Get(x,y-1), 2*Unit()).Normalize();
	}
}

//---- Exporting to StorageMesh
void HeightMap::ExportNormals(vector<v3r> *dest) const {
	Assert(normals.size());

	if(!dest) return;
	vector<v3r> &out = *dest;

	if(out.size() != NumVertices())
		out.resize(NumVertices());

	Assert(out.size() == NumVertices());

	loop2(x,y,dataSize,dataSize) {
		out[Index(x,y)] = normals[Index(x,y)];
	}
}

void HeightMap::ExportVertices(vector<v3r> *dest, v2r offset) const {
	if(!dest) return;
	vector<v3r> &out = *dest;

	if(out.size() != NumVertices())
		out.resize(NumVertices());

	loop2(x,y,dataSize,dataSize) {
		//int rx = x;
		//int ry = y;
		//if(x == int(dataSize-1))	rx = 0;
		//if(y == int(dataSize-1)) ry = 0;

		u32 id = Index(x,y);
		out[id] = GetPoint(x,y) + v3r(offset);
		//out[id][2] = Get(rx,ry);
	}
}

void HeightMap::ExportTexCoords(vector<v2r> *dest) const {
	if(!dest) return;
	vector<v2r> &out = *dest;

	if(out.size() != NumVertices())
		out.resize(NumVertices());

	loop2(x,y,dataSize,dataSize) {
		u32 id = Index(x,y);
		out[id] = v2r(RATIO(x, dataSize-1), RATIO(y, dataSize-1));
	}
}

void HeightMap::ExportIndices(vector<u32> *dest) const {
	if(!dest) return;
	vector<u32> &out = *dest;

	if(out.size() != NumIndices())
		out.resize(NumIndices());

	Assert(out.size() == NumIndices());

	u32 i=0;
	u32 max = 0;
	loop2(x,y,dataSize-1,dataSize-1) {
#if 0 // CW
		out[i++] = Index(x,y);
		out[i++] = Index(x,y+1);
		out[i++] = Index(x+1,y+1);
		out[i++] = Index(x+1,y);
#else // CCW
		out[i++] = Index(x,y);
		out[i++] = Index(x+1,y);
		out[i++] = Index(x+1,y+1);
		out[i++] = Index(x,y+1);
#endif
		max = MAX(max, Index(x+1,y+1));
	}
}

u32 HeightMap::NumVertices() const {
	return SQUARE(dataSize);
}

u32 HeightMap::NumIndices() const {
	return SQUARE(dataSize-1)*4;
}

real& HeightMap::Get(i32 x, i32 y) {
	return data[Index(x,y)];
}

real HeightMap::Get(i32 x, i32 y) const {
	return data[Index(x,y)];
}

real HeightMap::Unit() const {
	return RATIO(worldSize,dataSize-1);
}

v3r HeightMap::GetPoint(i32 x, i32 y) const {
	real xc = x*Unit();
	real yc = y*Unit();
	real zc = data[Index(x,y)];
	return v3r(xc,yc,zc);
}

v3r HeightMap::GetNormal(i32 x, i32 y) const {
	return normals[Index(x,y)];
}

u32 HeightMap::Index(i32 x, i32 y) const {
	x = Wrap(x, dataSize);
	y = Wrap(y, dataSize);
	return x + y*dataSize;
}

#include "platform/GLContext.h"

void HeightMap::DrawPoints() const {
	using namespace GLContext;
	GLPrimitive(GL_POINTS) {
		loop2(x,y,dataSize,dataSize) {
			Vertex(GetPoint(x,y));
		}
	}
}

