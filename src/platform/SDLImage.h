
#ifndef __SDLIMAGE_H
#define __SDLIMAGE_H

#include "Image3D.h"

class SDLImage: public Image3D{
	public: 
		SDLImage(string filename=""){ Init(); Load(filename); }
		SDLImage& operator= (const SDLImage& sdl);
		bool Load(string filename);
		bool Load(const void* mem, u32 size, const char *extension);
		void Free();
		string FileName() const { return fname; }

		// Inherited Members
		~SDLImage();
		bool Valid() const { return valid; }
		u16 Width()  const { return width; }
		u16 Height() const { return height; }
		u16 Depth()  const { return 1; }
		u16 BPP()    const { return bpp; }
		// GLenum DataFormat() const;
		// GLenum DataType() const;
		// u32 DataBytes() const;
		const void* Data() const { return &data[0]; }
	
		void Print(std::ostream& os) const;

	private:
		bool FromSurface(SDL_Surface *data);
		void Init();
		void perror(string err) const;
		void pwarn(string err)  const;
	
		string fname;
		bool valid;
		u32 bpp;
		u32 width;
		u32 height;
		vector<u8> data;
};


#endif
