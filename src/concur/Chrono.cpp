#include "backend.h"
#include "Chrono.h"

#include <ctime>

#if YARA_LINUX
#define RESOLUTION 1000000000LL
#endif

static u64 GetResolution() {
#if YARA_WINDOWS
	LARGE_INTEGER frequency = {0};
	if(!QueryPerformanceFrequency(&frequency)) {
		return 1000;
	}
	return frequency;
#else
	return RESOLUTION;
#endif
}

static u64 GetTicks(){
#if YARA_WINDOWS
	static u64 lastTime = 0;
	LARGE_INTEGER time = {0};
	if(!QueryPerformanceCounter(&time)) {
		return lastTime;
	}
	lastTime = u64(time.QuadPart);
	return lastTime;
#else
	timespec t;
	clock_gettime(CLOCK_MONOTONIC, &t);
	return (t.tv_sec*RESOLUTION+t.tv_nsec);
#endif
}

void Nap(real s){
#if YARA_LINUX
	static struct timespec t;
	t.tv_sec=int(s);
	t.tv_nsec=int((s-int(s))*1e9);
	nanosleep(&t,&t);
#else
	// since no option to Sleep(t < 1ms), block if we can't
	real ms = s*1000;
	if (ms > 1.0) {
		Sleep((DWORD) ms);
	} else {
		real now = GetTicks();
		while(GetTicks() - now < s)
			;
	}
#endif
}

void TimeValue::Now() {
	ticks = GetTicks();
}

real TimeValue::Seconds() const {
	return real(ticks)/real(GetResolution());
}


