
#include "Lockable.h"

Lockable::Lockable() {
	mutex = SDL_CreateMutex();
}

Lockable::~Lockable() {
	SDL_DestroyMutex(mutex);
}

bool Lockable::Lock() const {
	SDL_LockMutex(mutex);
	return true;
}

bool Lockable::Unlock() const {
	SDL_UnlockMutex(mutex);
	return true;
}

