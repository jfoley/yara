#include "Thread.h"
#include "Chrono.h"
#include "types.h"
#include "macros.h"

//--- private to file
namespace {
	//--- magic callbacks
	int _run(void* handle) {
		Thread* t=(Thread*)handle;
		t->state=Thread::RUNNING;
		t->Run();
		t->state=Thread::INIT;
		return 0;
	}
}


bool Thread::Running() const { 
	return state!=INIT; 
}

Thread::Thread(){ 
	state=INIT; 
}

Thread::~Thread(){
	while(state==FORKING) {
		ForceYield();
	}
	if(state==RUNNING)
		Join();
}

bool Thread::Start(){
	if(state==INIT) {
		state=FORKING;
		thread = SDL_CreateThread(_run, this);
		if(thread == 0) {
			state=INIT;
			return false;
		}
		while(state==FORKING) {
			ForceYield();
		}
	}
	return true; 
}

bool Thread::Join(){
	//--- make sure threads are in a good state
	while(state==FORKING) {
		ForceYield();
	}
	
	if(state==RUNNING) {
		SDL_WaitThread(thread, 0);
		return true;
	} else {
		return false;
	}
}

