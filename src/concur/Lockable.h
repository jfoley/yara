// Lockable.h

#ifndef __LOCKABLE_H
#define __LOCKABLE_H

#include "concur.h"
#include "Assert.h"

/// A mutex and associated methods
class Lockable{
	public:
		Lockable();
		~Lockable();

		bool Lock()    const;
		bool Unlock()  const; 

	private:
		Mutex_t mutex;
};

/// A scope-lifetime mutex lock, fashioned after boost's scope_lock
class ScopeLock {
	public:
		ScopeLock(const Lockable &t);
		~ScopeLock();
	private:
		const Lockable &target;
};

//TODO replace assert with something better
inline ScopeLock::ScopeLock(const Lockable &t) : target(t) {
	Assert(target.Lock());
}

//TODO replace assert with something better
inline ScopeLock::~ScopeLock() {
	Assert(target.Unlock());
}

#endif

