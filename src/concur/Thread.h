
#ifndef __THREAD_H
#define __THREAD_H

#include "concur.h"

class Thread{
	public:
		Thread();
		virtual ~Thread();
		virtual void Run()=0;
		bool Start();
		bool Join();
		bool Running() const;
	
		enum ThreadState{ INIT, FORKING, RUNNING, } state;
	private:
		Thread_t thread;
};

#endif
