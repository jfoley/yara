#ifndef __CHRONO_H
#define __CHRONO_H

#include "backend.h"
#include "util/Compare.h"
#include "types.h"

void Nap(real s);

#if YARA_LINUX
	#include <sched.h>
	inline void ForceYield() { sched_yield(); }
#elif YARA_WINDOWS
	inline void ForceYield() { Sleep(0); }
#endif

class TimeValue {
	public:
		//--- compare operators
		ComparableTo(TimeValue);
		real CompareTo(const TimeValue& t) const { return (*this-t); }

		TimeValue() { Now(); }
		TimeValue(u64 n) : ticks(n) { }
		
		//--- update internal value to now
		void Now();

		//--- really basic math operations
		TimeValue operator-(const TimeValue &other) const { return ticks - other.ticks; }
		TimeValue operator+(const TimeValue &other) const { return ticks + other.ticks; }
		

		//--- floating point repr
		real Seconds() const;

		//--- casting to a double returns a floating point value in seconds
		operator real() const { return Seconds(); }

	private:
		u64 ticks;
};

inline real GetTime(){
	return TimeValue().Seconds();
}

class Stopwatch {
	public:
		ComparableTo(Stopwatch);
		Stopwatch() { Start(); }
		TimeValue Start() { stop.Now(); start.Now(); return 0; }
		TimeValue Stop()  { stop.Now(); return Duration(); }
		real Duration() const { return stop-start; }
		real CompareTo(const Stopwatch& t) const { return Duration()-t.Duration(); }
	private:
		TimeValue start;
		TimeValue stop;
};

#endif
