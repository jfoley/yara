#ifndef __CONCUR_H
#define __CONCUR_H

namespace concur {
	bool Init();
};

#include "backend.h"

// wow, SDL isn't even consistently capitalized O.o
#define Mutex_t    SDL_mutex*
#define Thread_t   SDL_Thread*

#endif

