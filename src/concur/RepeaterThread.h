
#ifndef __REPEATERTHREAD_H
#define __REPEATERTHREAD_H

#include "Thread.h"
#include "Chrono.h"

class RepeaterThread: public Thread{
	public:
		virtual void Rep() = 0;
		void Run() {
			while(running){
				Stopwatch sw;
				sw.Start();
				Rep();
				if(interval<0)
					break;
				sw.Stop();
				Nap(interval-sw.Duration());
			}
		}
		virtual ~RepeaterThread() {
			Stop();
			Join();
		}

		void SetInterval(real i){ running=true; interval=i; }
		real GetInterval() const { return interval; }

		void Stop() { running=false; }
		void StopAndJoin() { Stop(); Join(); }

	private:
		bool running;
		real interval;
};

#endif
