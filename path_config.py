import sys
from make.Backend import *

cfg_include_paths = []
cfg_lib_paths = []

if onWindows():
	# the r's prevent \x from escaping
	
	base_path = "C:\\yara\\"
	
	arch = 'x86'
	if getMachineType() == 64:
		arch = 'x64'
	
	cfg_include_paths += [
		base_path + "SDL-1.2.15\\include",
		base_path + "SDL_mixer-1.2.12\\include",
		base_path + "glew-1.7.0\\include",
		base_path + "glut-3.7.6-bin",
		]
	
	cfg_lib_paths += [
		base_path + "SDL-1.2.15\\lib\\"+arch,
		base_path + "SDL_mixer-1.2.12\\lib\\"+arch,
		base_path + "glew-1.7.0\\lib",
		base_path + "glut-3.7.6-bin",
		]
	
