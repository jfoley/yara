#include "Tool.h"

static const string HeaderInclude("header_include");
static const string ImplInclude("impl_include");

// C++ Style definitions
static const string DefaultMethodName("Defaults");
static const string LoadMethodName("LoadConfig");

struct ConfigToolInfo {
	string confName;   //Example.conf
	string baseName;   //path/Example
	string implName;   //path/Example.cpp
	string headerName; //path/Example.h
	string relBaseName; //Example
	string relImplName; //Example.cpp
	string relHeaderName; //Example.h
	vector<string> headerIncludes;
	vector<string> implIncludes;
	vector<ConfigVar> defines;
};

static bool __ByName(const ConfigVar& a, const ConfigVar& b) { return a.name < b.name; }

void writeStruct(std::ostream &os, Indentation &indent, const vector<ConfigVar> &input) {
	vector<ConfigVar> vars(input);
	//--- sort so that scopes are in order
	std::sort(vars.begin(), vars.end(), __ByName);
	
	using FileFormat::ScopeSep;

	vector<string> cur_path;

	loop(i, vars.size()) {
		const string &name = vars[i].name;
		const string &type = vars[i].type;

		string varname = ifContainsAfterLast(name, ScopeSep);
		
		//--- split scope path
		vector<string> path = split(name, ScopeSep);
		
		int path_len     = int(path.size());
		int cur_path_len = int(cur_path.size());

		//--- count elements of path that are the same as the previous
		int same;
		for(same = 0; same < cur_path_len && same < path_len; same++) {
			if(cur_path[same] != path[same]) break;
		}

		//--- close all open scopes that don't match
		for(int j = cur_path_len-1; j >= same; j--) {
			const string &toClose = cur_path[j];
			indent.Less();
			os << indent << "} " << toClose << ";\n";
		}

		cur_path.resize(same);

		//--- open all new scopes that don't match
		for(int j = same; j < path_len-1; j++) {
			const string &toOpen = path[j];
			cur_path.push_back(toOpen);
			os << indent << "struct " << toOpen << "_s {\n";
			indent.More();
		}

		//--- output the actual variable
		os << indent << type << " " << varname << ";\n";
	}

	rloop(i, cur_path.size()) {
		const string &toClose = cur_path[i];
		indent.Less();
		os << indent << "} " << toClose << ";\n";
	}
}

bool writeHeader(ConfigToolInfo *info) {
	std::ofstream file;
	file.open( info->headerName.c_str() );
	if(!file.good()) return false;

	string guard = MakeIncludeGuard(info->headerName);
	
	file << "#ifndef " << guard << "\n";
	file << "#define " << guard << "\n";
	file << "\n";
	file << "#include \"types.h\" // for string\n"; 
	file << "\n";
	loop(i, info->headerIncludes.size()) {
		const string& str = info->headerIncludes[i];
		file << "#include " << str << "\n";
	}

	file << "\n";

	Indentation indent(1);

	string structName(info->relBaseName);

	file << "struct " << structName << " {\n";
	file << indent << "/// Constructor\n";
	file << indent << structName << "() { " << DefaultMethodName << "(); }\n";
	file << indent << "/// Reset to Defaults\n";
	file << indent << "void " << DefaultMethodName << "();\n";
	file << indent << "/// Load values present in file fileName\n";
	file << indent << "bool " << LoadMethodName << "(string fileName);\n\n\n";

	file << indent << "/// Values\n";

	writeStruct(file, indent, info->defines);


	//--- terminate file
	file << "}; // " << structName << "\n";
	file << "\n#endif\n\n";

	return true;
}

bool writeImpl(ConfigToolInfo *info) {
	std::ofstream file;
	file.open(info->implName.c_str());
	if(!file.good()) return false;

	file << "#include \"" << info->relHeaderName << "\"\n\n";

	if(info->implIncludes.size()) {
		file << "// manually specified includes:\n";
	}
	loop(i, info->implIncludes.size()) {
		const string& str = info->implIncludes[i];
		file << "#include " << str << "\n";
	}

	file << "\n// algorithm specific includes:\n";

	file << "#include \"macros.h\" // for loop\n";
	file << "#include \"util/parse.h\"\n"; 
	file << "#include \"util/ConfigParser.h\"\n";
	file << "#include <cstdio>\n";
	file << "#include \"Assert.h\"\n";
	file << "using namespace util;\n";

	file << "\n";
	
	string structName(info->relBaseName);

	file << "void " << structName << "::" << DefaultMethodName << "() {\n";
	loop(i, info->defines.size()) {
		const ConfigVar &c = info->defines[i];
		file << "\t" << c.name << " = parse< " << c.type << " >(";
		
		string val = c.value;
#define QUOTE_CHAR '"'
		if(contains(val, QUOTE_CHAR)) {
			val = beforeLast(after(val, QUOTE_CHAR), QUOTE_CHAR);
		}
		file << "\"" << val << "\");\n";
	}
	file << "}\n\n";

	file << "bool " << structName << "::" << LoadMethodName << "(string fileName) {\n";
	file << "\tConfigFileData data;\n";
	file << "\tif(!data.Parse(fileName)) return false;\n";
	file << "\n\tloop(i, data.defines.size()) {\n";
	file << "\t\tconst string& name  = data.defines[i].name;\n";
	file << "\t\tconst string& type  = data.defines[i].type;\n";
	file << "\t\tconst string& value = data.defines[i].value;\n";
	
	file << "\n";

	//--- blame dave, if trick so I can just generate else ifs
	file << "\t\tif(0) {\n";
	loop(i, info->defines.size()) {
		const string& name  = info->defines[i].name;
		const string& type  = info->defines[i].type;
		
		file << "\t\t} else if(name == \"" << name << "\") {\n";
		file << "\t\t\tAssert(type==\"" << type << "\");\n";
		file << "\t\t\t" << name << "= parse< " << type << " >(value);\n";
	}
	file << "\t\t} else {\n";
	file << "\t\t\tprintf(\"Unexpected key %s in file %s\\n\", name.c_str(), fileName.c_str());\n";
	file << "\t\t\tAssert(0);\n";
	file << "\t\t} // end if catastrophe\n";
	file << "\t} //end loop\n";
	file << "return true;\n";
	file << "} //end func\n";

	return true;
}

int main(int argc, const char **argv) {
	ArgumentParser arg_parse;

	arg_parse.DefineParameter("configFile", "The configuration file to process");

	Option<Arguments> result = arg_parse.Parse(argc, argv);
	if(!result.Valid()) {
		std::cout << "\n";
		arg_parse.PrintUsage(std::cout, argv[0]);
		return -1;
	}
	Arguments args(result);

	ConfigToolInfo info;
	info.confName = args.GetParameter("configFile");
	info.baseName = before(info.confName, '.'); // strip extension
	info.headerName = info.baseName + HeaderExt;
	info.implName = info.baseName + ImplExt;
	info.relBaseName   = ifContainsAfterLast(info.baseName, PathSep);
	info.relHeaderName = ifContainsAfterLast(info.headerName, PathSep);
	info.relImplName   = ifContainsAfterLast(info.implName, PathSep);

	ConfigFileData filedata;
	if(!filedata.Parse(info.confName)) {
		printf("%s: *** file %s had no useful data.\n", argv[0], info.confName.c_str());
		return -2;
	}

	//--- take in our defines
	info.defines = filedata.defines;
	//--- parse special directives
	loop(i, filedata.directives.size()) {
		Line line = filedata.directives[i];
		string data = line.data;
		u32 lineNumber = line.number;
		string directive = toLower(before(data, ' '));
		string argument = after(data, ' ');

		if(directive == HeaderInclude) {
			info.headerIncludes.push_back(argument);
		} else if(directive == ImplInclude) {
			info.implIncludes.push_back(argument);
		} else {
			printf("%s:%d: *** Warning, unhandled special directive \"%s\"\n", argv[0], lineNumber, data.c_str());
		}
	}

	if(!writeHeader(&info)) {
		printf("%s: *** Couldn't write header file %s\n", argv[0], info.headerName.c_str());
		return -3;
	}
	
	if(!writeImpl(&info)) {
		printf("%s: *** Couldn't write implementation file %s\n", argv[0], info.implName.c_str());
		return -4;
	}

}

