#include "Tool.h"

static const string EnumExt(".enum");
static const string memberName("value");
//--- @ directives
static const string StorageType("storage");
static const string ErrorValue("error");
static const string Include("include");

//--- because I don't think enums ought to start with zero
const int DefaultStartValue = 1;
const int DefaultErrorValue = 0;

//--- ASSUMPTION all given values ought to fit in int
struct EnumValue {
	string name;
	int    value;
};

struct EnumToolInfo {
	string inputFile;
	string outputFile;
	string className;
	string storageType;
	string startValue;
	string errorValue;
	vector<EnumValue> values;
	vector<string> includes;
};

string makeSetExpression(vector<int> &values, string parameter) {
	std::sort(values.begin(), values.end());
	const char *x = parameter.c_str();
	
	string expr = "";

	const int minimum = values[0];
	const int none = minimum-1;

	int start = none;
	int last = none;

	int len = int(values.size());
	for(int i=0; i<len; i++) {
		int current = values[i];

		if(start == none) {
			start = current;
			last  = start;
			continue;
		}
		// gap found
		if(current != last+1) {
			if(expr != "") expr += " | ";
			if(start == last) {
				expr += stringf("(%s==%d)", x, start);
			} else {
				expr += stringf("(%s>=%d && %s<=%d)", x, start, x, last);
			}
			start = current;
			last  = start;
			continue;
		} else {
			last = current;
		}
	}
	//--- close expression
	if(expr != "") expr += " | ";
	if(start == last) {
		expr += stringf("(%s==%d)", x, start);
	} else {
		expr += stringf("(%s>=%d && %s<=%d)", x, start, x, last);
	}

	return expr;
}

bool parseInput(EnumToolInfo *info) {
	FileData raw;
	
	if(!raw.Parse(info->inputFile)) return false;

	using namespace FileFormat;
	//--- parse lines of file
	int index = DefaultStartValue;
	int error = DefaultErrorValue;
	loop(i, raw.lines.size()) {
		Line line = raw.lines[i];
		string text = line.data;

		if(contains(text, Assignment)) {
			//--- split strings
			string newValue = after(text, Assignment);
			text = before(text, Assignment);

			//--- parse text after "=" to determine new index
			Option<int> newIndex = parse<int>(newValue);
			if(!newIndex.Valid()) {
				printf("%s:%d: *** Syntax error, can't parse index for line: \"%s\"\n", raw.fileName, line.number, text.c_str());
				return false;
			}
			
			index = newIndex;
		}
		
		EnumValue current;
		current.name  = compactSpaces(text);
		current.value = index;

		if(index == error) {
			error++;
		}
		
		info->values.push_back(current);
		
		//--- increment index
		index++;
	}

	//--- determine appropriate errorValue
	info->errorValue = stringf("%d", error);	
	
	//--- parse special directives
	loop(i, raw.directives.size()) {
		Line line = raw.directives[i];
		string data    = compactSpaces(line.data);
		string command = before(data, ' ');
		string args    = after(data, ' ');

		if(command == StorageType) {
			info->storageType = args;
		} else if(command == Include) {
			info->includes.push_back(args);
		} else {
			printf("%s:%d: *** Unknown directive \"%s\"\n", raw.fileName, line.number, command.c_str());
		}
	}


	return info->values.size() != 0;
}

bool writeOutput(EnumToolInfo *info) {
	std::ofstream file;
	file.open( info->outputFile.c_str() );
	if(!file.good()) return false;

	//--- start file
	const string guard = MakeIncludeGuard(info->outputFile);
	file << "#ifndef " << guard << "\n";
	file << "#define " << guard << "\n";
	file << "\n";

	//--- put includes
	loop(i, info->includes.size()) {
		file << "#include " << info->includes[i] << "\n";
	}
	file << "\n";

	//--- write EnumClass 
	const string className = info->className;
	Indentation indent(0);

	file << indent << "class " << className << " {\n";
	indent.More();
	file << indent << "public:\n";
	indent.More();
	
	//--- write constructor
	file << indent << "/// Constructor, default to error\n";
	file << indent << className << "(" << info->storageType << " " << "x=" << info->errorValue << ") : " << memberName << "(x) { }\n";
	file << "\n";
	file << indent << "operator " << info->storageType << "() const { return " << memberName << "; }\n\n";
	file << indent << "operator string() const { return ToString(); }\n\n";

	//--- write list of constant objects
	loop(i, info->values.size()) {
		const EnumValue &v = info->values[i];
		//--- type prefix
		file << indent << "static const " << info->storageType << " ";
		//--- varname = value
		file << v.name << " = " << v.value << ";\n";
	}
	file << "\n";

	file << indent << "/// returns true if this enum value is Valid or not\n";
	file << indent << "bool Valid() const {\n";
	indent.More();
	file << indent << "return ";
	{
		//--- create a sorted set of allowable indices
		vector<int> set;
		loop(i, info->values.size()) {
			set.push_back(info->values[i].value);
		}
		const string expr = makeSetExpression(set, memberName);
		file << expr << ";\n";
	}
	indent.Less();
	file << indent << "}\n";


	const string constRef = ConstRef(className);

	//--- write operator==
	file << indent << "/// Comparison operator\n";
	file << indent << "bool operator==(" << constRef << " rhs) const { return " << memberName << " == rhs." << memberName << "; }\n";
	file << indent << "/// Comparison operator for storage type\n";
	file << indent << "bool operator==(" << info->storageType << " rhs) const { return " << memberName << " == rhs; }\n";
	file << indent << "/// Friend comparison operator for storage type\n";
	file << indent << "friend bool operator==(" << info->storageType << " lhs, " << constRef << " rhs) { return rhs." << memberName << " == lhs; }\n";
	file << indent << "/// Comparison operator with strings\n";
	file << indent << "bool operator==(const string& rhs) const { return this->operator==(FromString(rhs)); }\n";
	file << indent << "/// Friend comparison operator with strings\n";
	file << indent << "friend bool operator==(const string& lhs, " << constRef << " rhs) { return rhs == FromString(lhs); }\n";
	file << "\n\n";

	//--- write ToString method
	file << indent << "/// For turning a " << className << " into a string\n";
	file << indent << "string ToString() const {\n";
	indent.More();
	loop(i, info->values.size()) {
		const EnumValue &v = info->values[i];
		file << indent;
		if(i != 0) file << "else ";
		file << "if(" << memberName << " == " << v.value << ") return \"" << v.name << "\";\n";
	}
	file << indent << "else return util::stringf(\"<Unknown " << className << ":%d>\", " << memberName << ");\n"; 
	indent.Less();
	file << indent << "}\n\n";

	//--- write operator<<
	file << indent << "/// for using ostreams\n";
	file << indent << "friend std::ostream& operator<<(std::ostream &os, " << constRef << " obj) { os << string(obj.ToString()); return os; }\n\n";
	
	//--- write FromString method
	file << indent << "/// For constructing " << className << " values from strings\n";
	file << indent << "static " << className << " " << FromStringMethod << "(const string &input) {\n";
	indent.More();
	loop(i, info->values.size()) {
		const EnumValue &v = info->values[i];
		
		file << indent;
		if(i != 0) file << "else ";
		file << "if(input == \"" << v.name << "\") return " << v.name << ";\n";
	}
	file << indent << "else return " << className << "(" << info->errorValue << ");\n";
	indent.Less();
	file << indent << "} //--- end " << FromStringMethod << "\n";
	file << "\n";

	indent.Less();
	file << indent << "private:\n";
	indent.More();

	file << indent << info->storageType << " " << memberName << ";\n";
	file << "\n";
	indent.Less();
	indent.Less();
	file << indent << "}; //--- end " << className << "\n";
	file << "\n";

	//--- end file
	file << "#endif //" << guard << "\n\n";

	return true;
}

int main(int argc, char **argv) {
	ArgumentParser arg_parse;
	arg_parse.DefineParameter("enumFile", "The enumeration file to process");

	Option<Arguments> result = arg_parse.Parse(argc, argv);
	if(!result.Valid()) {
		printf("\n");
		arg_parse.PrintUsage(std::cout, argv[0]);
		return -1;
	}
	Arguments args(result);

	EnumToolInfo info;
	info.inputFile = args.GetParameter("enumFile");
	Assert(endsWith(info.inputFile, EnumExt));
	string baseName = before(info.inputFile, '.');
	info.outputFile = baseName + HeaderExt;
	info.className  = ifContainsAfterLast(baseName, PathSep);
	info.storageType = "int";
	
	//--- default includes
	info.includes.push_back("\"types.h\"");
	info.includes.push_back("\"util/strings.h\"");

	if(!parseInput(&info)) {
		printf("%s: *** file %s had no useful data.\n", argv[0], info.inputFile.c_str());
		return -2;
	}

	if(!writeOutput(&info)) {
		printf("%s: *** Couldn't write output file %s\n", argv[0], info.outputFile.c_str());
		return -3;
	}
}

